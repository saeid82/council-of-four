# README #


Sviluppo di software del gioco da tavolo: **The council of four**.
Un sistema distributivo composto da un lato gioco che puó gestire molti giochi simultanei e multipli lati giocatore che possono partecipare nel un solo gioco.
l’utilizzo del pattern MVC (Model-View-Controller) per progettare l’intero sistema

### Quick summary ###
# How to run #
The class **it.polimi.ingsw.cg44.cli.MainCLI** is the main class to running application.
there are 2 ways to playing this game that MainCLI class ask you to select: 

 1.  TCP connection
 2.  Local


Run the **it.polimi.ingsw.cg44.client.ClientApp** class for client side.