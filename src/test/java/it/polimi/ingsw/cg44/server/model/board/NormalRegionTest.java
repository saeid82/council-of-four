/**
 * 
 */
package it.polimi.ingsw.cg44.server.model.board;

import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.util.List;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.model.city.City;
import it.polimi.ingsw.cg44.server.model.city.FactoryCity;
import it.polimi.ingsw.cg44.server.model.map.Map;



/**
 * @author saeid
 *
 */
public class NormalRegionTest {
	private BufferedReader bufferedReader;
	private FactoryCity factoryCity;
	private NormalRegion normalRegion;
	
	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.board.NormalRegion#NormalRegion(it.polimi.ingsw.cg44.server.model.board.RegionType, java.util.List, int)}.
	 * @throws Exception 
	 */
	@Test
	public void testNormalRegion() throws Exception {
		bufferedReader = Map.readFile("src/main/resources/map.txt");
		// prima creo una lista della City
		BufferedReader bufferedReader = Map.readFile("src/main/resources/map.txt");
		factoryCity = new FactoryCity();
		List<City> cities = factoryCity.initializeCities(bufferedReader);
		normalRegion = new NormalRegion(RegionType.COAST, cities, 4);
		assertTrue(RegionType.COAST == normalRegion.getRegionType());
		
	}

	public BufferedReader getBufferedReader() {
		return bufferedReader;
	}


}
