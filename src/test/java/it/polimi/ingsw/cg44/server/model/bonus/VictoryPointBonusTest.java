/**
 * 
 */
package it.polimi.ingsw.cg44.server.model.bonus;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.model.Model;

/**
 * @author saeid
 *
 */
public class VictoryPointBonusTest {
	private VictoryPointBonus victoryPointBonus;
	private Model game;
	
	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.bonus.VictoryPointBonus#VictoryPointBonus()}.
	 */
	@Test
	public void testVictoryPointBonus() {
		victoryPointBonus = new VictoryPointBonus();
		System.out.println(victoryPointBonus);
	}

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.bonus.VictoryPointBonus#getQuantity()}.
	 */
	@Test
	public void testGetQuantity() {
		victoryPointBonus = new VictoryPointBonus();
		System.out.println(victoryPointBonus.getQuantity());
	}

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.bonus.VictoryPointBonus#useBonus(it.polimi.ingsw.cg44.server.model.game.Player, it.polimi.ingsw.cg44.server.model.game.Board)}.
	 * @throws Exception 
	 */
	@Test
	public void testUseBonus()  {

		try {
			game = new Model("src/main/resources/map.txt", 4);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		VictoryPointBonus victoryPointBonus = new VictoryPointBonus();
		
		// prima di usare il bonus
		int BeforUsingBonus = game.getPlayers().get(3).getVictoryTrack().getCurrentVictoryPoints();
		
		// usare il bonus
		victoryPointBonus.useBonus(game.getPlayers().get(3), game.getBoard());
		
		// dopo usare il bonus
		int afterUsingBonus = game.getPlayers().get(3).getVictoryTrack().getCurrentVictoryPoints();
		
		assertTrue(afterUsingBonus == BeforUsingBonus+victoryPointBonus.getQuantity());
	}

}
