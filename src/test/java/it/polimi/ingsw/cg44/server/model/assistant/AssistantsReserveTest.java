/**
 * 
 */
package it.polimi.ingsw.cg44.server.model.assistant;

import static org.junit.Assert.*;



import org.junit.Test;

/**
 * @author saeid
 *
 */
public class AssistantsReserveTest {
	private AssistantsReserve assistantsReserve;

	/**
	 * Test: con 4 giocatori abbiamo 30 assistanti, con 6 giocatori 31, con 7 giocatori 37
	 */
	@Test
	public void testAssistantsReserve() {
		
		assistantsReserve = new AssistantsReserve(4);
		assertTrue(30==assistantsReserve.getAssistants().size());
	}



}
