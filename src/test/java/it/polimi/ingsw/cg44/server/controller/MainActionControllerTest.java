/**
 * 
 */
package it.polimi.ingsw.cg44.server.controller;


import java.util.List;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.model.Model;
import it.polimi.ingsw.cg44.server.model.board.NormalRegion;
import it.polimi.ingsw.cg44.server.model.board.RegionType;
import it.polimi.ingsw.cg44.server.model.card.permitcard.PermitCard;
import it.polimi.ingsw.cg44.server.model.card.politiccard.PoliticCard;
import it.polimi.ingsw.cg44.server.model.city.City;
import it.polimi.ingsw.cg44.server.model.colors.Colors;
import it.polimi.ingsw.cg44.server.model.game.Board;
import it.polimi.ingsw.cg44.server.model.game.Player;
import it.polimi.ingsw.cg44.server.view.AcquirePermitCardView;
import it.polimi.ingsw.cg44.server.view.BuildEmporiumUsingPermitCardView;
import it.polimi.ingsw.cg44.server.view.BuildEmporiumWithKingHelpView;
import it.polimi.ingsw.cg44.server.view.ElectCouncillorView;

/**
 * @author saeid
 *
 */
public class MainActionControllerTest {
	private Model game;
	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.controller.MainActionController#MainActionController(java.util.List)}.
	 * @throws Exception 
	 */
	@Test
	public void testMainActionController() {
		try {
			game = new Model("src/main/resources/map.txt", 4);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Controller main = new MainActionController(game.getPlayers());
		System.out.println(main.toString());
		
		
		Player player = game.getPlayers().get(0);
		City nextCity = game.getBoard().getKing().getCity().getWays().get(0);
		Board board = game.getBoard();
		
		MainActionController mainActionController = new MainActionController(game.getPlayers());
		BuildEmporiumWithKingHelpView view = new BuildEmporiumWithKingHelpView(player, board, nextCity);
		
		view.createAction(mainActionController);
		mainActionController.update(view);
		
		PermitCard selectedPermitCard = game.getBoard().getRegions(RegionType.COAST).getDeckOfPermitCard().getFsceUpPermitCard().get(0);
		City city = selectedPermitCard.getCities().get(0);
		
		MainActionController mainActionController1 = new MainActionController(game.getPlayers());
		BuildEmporiumUsingPermitCardView view1 = new BuildEmporiumUsingPermitCardView(player, city, board, selectedPermitCard);
		
		view1.createAction(mainActionController1);
		mainActionController1.update(view1);
	}



	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.controller.MainActionController#update(it.polimi.ingsw.cg44.server.view.AcquirePermitCardView)}.
	 */
	@Test
	public void testUpdateAcquirePermitCardView() throws Exception{

		game = new Model("src/main/resources/map.txt", 4);
		Player player = game.getPlayers().get(0);
		List<PoliticCard> politicCards = player.getPoliticCards();
		Board board = game.getBoard();
		NormalRegion targetRegion = board.getRegions(RegionType.HILL);
		Colors colore = board.getCouncillorReserve().getCouncillorInReserve().get(0).getColors();

		MainActionController mainActionController = new MainActionController(game.getPlayers());
		AcquirePermitCardView view = new AcquirePermitCardView(player, targetRegion, board, politicCards, 0);
		
		view.createAction(mainActionController);
		mainActionController.update(view);
		
		MainActionController mainActionController2 = new MainActionController(game.getPlayers());
		ElectCouncillorView view2 = new ElectCouncillorView(player, targetRegion, board, colore);
		
		view2.createAction(mainActionController2);
		mainActionController2.update(view2);
		
	}


}
