/**
 * 
 */
package it.polimi.ingsw.cg44.server.model.king;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.model.Model;
import it.polimi.ingsw.cg44.server.model.board.RegionType;
import it.polimi.ingsw.cg44.server.model.city.City;
import it.polimi.ingsw.cg44.server.model.city.FactoryCity;
import it.polimi.ingsw.cg44.server.model.colors.Colors;

/**
 * @author saeid
 *
 */
public class KingTest {
	private Model game;
	private City hill;
	private City nextCity;
	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.king.King#getCity()}.
	 * @throws Exception 
	 */
	@Test
	public void testGetCity() throws Exception {
		game = new Model("src/main/resources/map.txt", 4);
		assertTrue(Colors.KING == game.getBoard().getKing().getCity().getColor());
	}

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.king.King#moving(it.polimi.ingsw.cg44.server.model.city.City)}.
	 * @throws Exception 
	 */
	@Test
	public void testMoving() throws Exception {
		game = new Model("src/main/resources/map.txt", 4);
		// vedere il nome della citta del Ré
		System.out.println(game.getBoard().getKing().getCity().getName());
		// vedere le cita collegate alla cita del ré.
		System.out.println(game.getBoard().getKing().getCity().getWays());
		// spostare il Re alla citta HILL.
		game.getBoard().getKing().moving(game.getBoard().getKing().getCity().getWays().get(0));

		hill = game.getBoard().getRegions(RegionType.HILL).getCities().get(3);
		nextCity = game.getBoard().getRegions(RegionType.HILL).getCities().get(3);
		assertTrue( hill == nextCity );

	}

}
