/**
 * 
 */
package it.polimi.ingsw.cg44.server.controller.action.quickaction;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.model.Model;
import it.polimi.ingsw.cg44.server.model.board.RegionType;
import it.polimi.ingsw.cg44.server.model.card.permitcard.PermitCard;

/**
 * @author saeid
 *
 */
public class ChangeFaceUpPermitCardTest {
	private Model game;
	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.controller.action.quickaction.ChangeFaceUpPermitCard#ChangeFaceUpPermitCard(it.polimi.ingsw.cg44.server.model.game.Player, it.polimi.ingsw.cg44.server.model.game.Board, it.polimi.ingsw.cg44.server.model.board.NormalRegion)}.
	 * @throws Exception 
	 */
	@Test
	public void testChangeFaceUpPermitCard() {
		try {
			game = new Model("src/main/resources/map.txt", 4);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ChangeFaceUpPermitCard changeFaceUpPermitCard = new ChangeFaceUpPermitCard(game.getPlayers().get(0), game.getBoard(),
				game.getBoard().getRegions(RegionType.COAST));
		
		// player paga un assistante per fare questo azione
		// il numero delle assistanti della giocarore prima della zione
		int PlyAssisbefor = game.getPlayers().get(0).getAssistants().size();
		
		// i due permitcard faceup prima della zione
		PermitCard firstBefor = game.getBoard().getRegions(RegionType.COAST).getDeckOfPermitCard().getFsceUpPermitCard().get(0);
		PermitCard secondBefor = game.getBoard().getRegions(RegionType.COAST).getDeckOfPermitCard().getFsceUpPermitCard().get(1);
		
		changeFaceUpPermitCard.run();
		
		// il numero delle assistanti della giocarore dopo della zione
		int PlyAssisAfter = game.getPlayers().get(0).getAssistants().size();
		
		// i due permitcard faceup dopo della zione
		PermitCard firstAfter = game.getBoard().getRegions(RegionType.COAST).getDeckOfPermitCard().getFsceUpPermitCard().get(0);
		PermitCard secondAfter = game.getBoard().getRegions(RegionType.COAST).getDeckOfPermitCard().getFsceUpPermitCard().get(1);
		
		assertTrue(firstBefor != firstAfter);
		assertTrue(secondBefor != secondAfter);
		
		assertTrue(PlyAssisbefor == PlyAssisAfter+1);
	}

}
