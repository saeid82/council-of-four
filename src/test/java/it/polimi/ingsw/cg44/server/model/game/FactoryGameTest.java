/**
 * 
 */
package it.polimi.ingsw.cg44.server.model.game;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.model.assistant.AssistantsReserve;
import it.polimi.ingsw.cg44.server.model.track.NobilityTrack;

/**
 * @author saeid
 *
 */
public class FactoryGameTest {
	
	private FactoryGame factory = new FactoryGame();
	private Board board;
	private NobilityTrack nobilityTrack;
	private AssistantsReserve assistantsReserve;
	private List<Player> players;
	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.game.FactoryGame#createBoard(java.lang.String, int)}.
	 * @throws Exception 
	 */
	@Test
	public void testCreateBoard() throws Exception {
		board = factory.createBoard("src/main/resources/map.txt", 4);
		assertTrue(15 == board.getCities().size());
		
	}

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.game.FactoryGame#createPlayers(it.polimi.ingsw.cg44.server.model.track.NobilityTrack, it.polimi.ingsw.cg44.server.model.assistant.AssistantsReserve, int)}.
	 * @throws Exception 
	 */
	@Test
	public void testCreatePlayers() throws Exception {
		nobilityTrack = new NobilityTrack();
		assistantsReserve = new AssistantsReserve(4);
		players = factory.createPlayers(nobilityTrack, assistantsReserve, 4);
		assertTrue(4 == players.size());
	}

}
