/**
 * 
 */
package it.polimi.ingsw.cg44.server.model.track;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.model.Model;

/**
 * @author saeid
 *
 */
public class NobilityTrackTest {
	private Model game;
	private NobilityTrack nobilityTrack;
	/**
	 * Test: dovrebbe esserci 20 spazio
	 * @throws Exception 
	 */
	@Test
	public void testNobilityTrack() throws Exception {
		game = new Model("src/main/resources/map.txt", 4);
		assertTrue(20 == game.getBoard().getNobilityTrack().getTrack().size());
	}

	/**
	 * Test: ci da elenco da 20 elemnti di  tutte le spazi.
	 * Prima casella non deve avere il bonus
	 * @throws Exception 
	 */
	@Test
	public void testGetTrack()  {
		nobilityTrack = new NobilityTrack();
		assertTrue(20 == nobilityTrack.getTrack().size());
		//Prima casella non deve avere il bonus
		assertTrue(true == nobilityTrack.getTrack().first().getBonus().isEmpty());
	}

}
