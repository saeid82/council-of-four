/**
 * 
 */
package it.polimi.ingsw.cg44.server.model.track;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.model.Model;

/**
 * @author saeid
 *
 */
public class VictoryTrackTest {

	private Model game;



	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.track.VictoryTrack#getCurrentVictoryPoints()}.
	 * @throws Exception 
	 */
	@Test
	public void testGetCurrentVictoryPoints() throws Exception{
		game = new Model("src/main/resources/map.txt", 4);
		assertTrue(0 == game.getPlayers().get(1).getVictoryTrack().getCurrentVictoryPoints());
		
		// portare in avanti per due caselle
		game.getPlayers().get(2).getVictoryTrack().setCurrentVictoryPoints(2);
		assertTrue(2 == game.getPlayers().get(2).getVictoryTrack().getCurrentVictoryPoints());
	}


}
