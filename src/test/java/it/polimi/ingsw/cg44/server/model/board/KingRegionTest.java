package it.polimi.ingsw.cg44.server.model.board;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.model.bonus.BonusKingCard;
import it.polimi.ingsw.cg44.server.model.colors.Colors;

public class KingRegionTest {
	private KingRegion kingRegion;
	private BonusKingCard bonusKingCard;
	@Test
	public void testKingRegion() {
		kingRegion = new KingRegion();
		bonusKingCard = new BonusKingCard();
		assertEquals(bonusKingCard.getClass(), kingRegion.drawKingCard().getClass());
		assertEquals(4, kingRegion.getDeckOfKingCard().size());
		kingRegion.getDeckOfCityColorBonus(Colors.IRON);
		assertEquals(null, kingRegion.getDeckOfCityColorBonus(Colors.IRON));
		
	}



}
