/**
 * 
 */
package it.polimi.ingsw.cg44.server.model.bonus;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.model.Model;

/**
 * @author saeid
 *
 */
public class PoliticCardBonusTest {
	private Model game;
	private PoliticCardBonus politicCardBonus;
	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.bonus.PoliticCardBonus#useBonus(it.polimi.ingsw.cg44.server.model.game.Player, it.polimi.ingsw.cg44.server.model.game.Board)}.
	 * @throws Exception 
	 */
	@Test
	public void testUseBonus() {

		try {
			game = new Model("src/main/resources/map.txt", 4);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		politicCardBonus = new PoliticCardBonus();
		
		// numero delle carte politiche prima di usare bonus
		assertTrue(6 == game.getPlayers().get(0).getPoliticCards().size());
		// usare bonus
		try {
			politicCardBonus.useBonus(game.getPlayers().get(0), game.getBoard());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// numero delle carte politiche dopo di usare bonus
		assertTrue(7 == game.getPlayers().get(0).getPoliticCards().size());
		
		// usare ancora bonus
		try {
			politicCardBonus.useBonus(game.getPlayers().get(0), game.getBoard());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// numero delle carte politiche dopo di usare bonus
		assertTrue(8 == game.getPlayers().get(0).getPoliticCards().size());
	}

}
