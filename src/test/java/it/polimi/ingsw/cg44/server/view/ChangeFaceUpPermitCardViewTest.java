/**
 * 
 */
package it.polimi.ingsw.cg44.server.view;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.controller.QuickActionConroller;
import it.polimi.ingsw.cg44.server.model.Model;
import it.polimi.ingsw.cg44.server.model.board.NormalRegion;
import it.polimi.ingsw.cg44.server.model.board.RegionType;
import it.polimi.ingsw.cg44.server.model.game.Board;
import it.polimi.ingsw.cg44.server.model.game.Player;

/**
 * @author saeid
 *
 */
public class ChangeFaceUpPermitCardViewTest {
	private Model game;
	private Player player;
	private NormalRegion normalRegion;
	private Board board;
	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.view.ChangeFaceUpPermitCardView#createAction(it.polimi.ingsw.cg44.server.controller.Controller)}.
	 * @throws Exception 
	 */
	@Test
	public void testCreateAction() throws Exception {
		game = new Model("src/main/resources/map.txt", 4);
		player = game.getPlayers().get(0);
		board = game.getBoard();
		normalRegion = board.getRegions(RegionType.HILL);
		
		QuickActionConroller quickActionConroller = new QuickActionConroller(game.getPlayers());
		ChangeFaceUpPermitCardView view = new ChangeFaceUpPermitCardView(player, normalRegion, board);
		
		view.createAction(quickActionConroller);
		quickActionConroller.update();
		
		view.registerObserver(quickActionConroller);
		view.unregisterObserver(quickActionConroller);

	}

}
