/**
 * 
 */
package it.polimi.ingsw.cg44.server.model.city;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.FileReader;

import org.junit.Test;

/**
 * @author saeid
 *
 */
public class FactoryCityTest {
	private FactoryCity factoryCity;
	private int numCities;
	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.city.FactoryCity#initializeCities(java.io.BufferedReader)}.
	 */
	@Test
	public void testInitializeCities() throws Exception{
		factoryCity = new FactoryCity();
		numCities = factoryCity.initializeCities(new BufferedReader(new FileReader("src/main/resources/map.txt"))).size();
		assertTrue(15 == numCities); 
		
	}

}
