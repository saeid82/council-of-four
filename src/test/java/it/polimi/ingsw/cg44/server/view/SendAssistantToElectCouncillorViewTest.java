/**
 * 
 */
package it.polimi.ingsw.cg44.server.view;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.controller.QuickActionConroller;
import it.polimi.ingsw.cg44.server.model.Model;
import it.polimi.ingsw.cg44.server.model.board.NormalRegion;
import it.polimi.ingsw.cg44.server.model.board.RegionType;
import it.polimi.ingsw.cg44.server.model.colors.Colors;
import it.polimi.ingsw.cg44.server.model.game.Board;
import it.polimi.ingsw.cg44.server.model.game.Player;

/**
 * @author saeid
 *
 */
public class SendAssistantToElectCouncillorViewTest {
	private Model game;
	private Player player;
	private NormalRegion targetRegion;
	private Board board;
	private Colors colore;
	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.view.SendAssistantToElectCouncillorView#createAction(it.polimi.ingsw.cg44.server.controller.Controller)}.
	 * @throws Exception 
	 */
	@Test
	public void testCreateAction() throws Exception {
		game = new Model("src/main/resources/map.txt", 4);
		player = game.getPlayers().get(0);
		board = game.getBoard();
		targetRegion = board.getRegions(RegionType.HILL);
		colore = board.getCouncillorReserve().getCouncillorInReserve().get(0).getColors();
		
		QuickActionConroller quickActionConroller = new QuickActionConroller(game.getPlayers());
		SendAssistantToElectCouncillorView view = new SendAssistantToElectCouncillorView(player, targetRegion, board, colore);
		
		view.createAction(quickActionConroller);
		quickActionConroller.update(view);
		
		view.registerObserver(quickActionConroller);
		view.unregisterObserver(quickActionConroller);
	}

}
