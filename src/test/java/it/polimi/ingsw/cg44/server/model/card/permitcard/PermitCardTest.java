/**
 * 
 */
package it.polimi.ingsw.cg44.server.model.card.permitcard;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.model.Model;
import it.polimi.ingsw.cg44.server.model.board.RegionType;

/**
 * @author saeid
 *
 */
public class PermitCardTest {
	private Model game;
	private PermitCard card , permitCard;

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.card.permitcard.PermitCard#PermitCard(java.util.List)}.
	 */
	@Test
	public void testPermitCard(){
		try {
			game = new Model("src/main/resources/map.txt", 4);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		card = game.getBoard().getRegions(RegionType.COAST).getDeckOfPermitCard().getDeck().poll();
		permitCard = new PermitCard(game.getBoard().getCities());
		System.out.println(permitCard);
		System.out.println(card);
		assertTrue(card.getClass() == permitCard.getClass());
	}



	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.card.permitcard.PermitCard#isUsed()}.
	 */
	@Test
	public void testIsUsed(){
		try {
			game = new Model("src/main/resources/map.txt", 4);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertTrue(false == game.getBoard().getRegions(RegionType.HILL).getDeckOfPermitCard().getFsceUpPermitCard().get(0).isUsed());
	}





}
