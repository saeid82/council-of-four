/**
 * 
 */
package it.polimi.ingsw.cg44.server.model.city;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.model.Model;

/**
 * @author saeid
 *
 */
public class EmporiumTest {
	private Model game;
	private Emporium emporium;
	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.city.Emporium#Emporium(it.polimi.ingsw.cg44.server.model.game.Player)}.
	 * @throws Exception 
	 */
	@Test
	public void testEmporium() {
		try {
			game = new Model("src/main/resources/map.txt", 4);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		emporium = new Emporium(game.getPlayers().get(3));
		assertTrue(game.getPlayers().get(3).getEmporiums().element().getClass() == emporium.getClass());
	}

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.city.Emporium#getPlayerId()}.
	 * @throws Exception 
	 */
	@Test
	public void testGetPlayerId(){
		try {
			game = new Model("src/main/resources/map.txt", 4);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		emporium = new Emporium(game.getPlayers().get(3));
		assertTrue(4 == emporium.getPlayerId());
	}

}
