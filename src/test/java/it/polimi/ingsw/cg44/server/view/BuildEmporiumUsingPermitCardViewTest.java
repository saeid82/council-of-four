/**
 * 
 */
package it.polimi.ingsw.cg44.server.view;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.controller.MainActionController;
import it.polimi.ingsw.cg44.server.controller.action.IAction;
import it.polimi.ingsw.cg44.server.model.Model;
import it.polimi.ingsw.cg44.server.model.board.RegionType;
import it.polimi.ingsw.cg44.server.model.card.permitcard.PermitCard;
import it.polimi.ingsw.cg44.server.model.city.City;
import it.polimi.ingsw.cg44.server.model.game.Board;
import it.polimi.ingsw.cg44.server.model.game.Player;

/**
 * @author saeid
 *
 */
public class BuildEmporiumUsingPermitCardViewTest {
	private Model game;
	private Player player;
	private Board board;
	private PermitCard permitcardToAdd;
	private PermitCard selectedPermitCard;
	private City city;
	private IAction buildEmporium;
	/**
	 * metto come commento il run() del azione pechè sonartest ci vuole inteazione con client
	 * 
	 * Test method for {@link it.polimi.ingsw.cg44.server.view.BuildEmporiumUsingPermitCardView#createAction(it.polimi.ingsw.cg44.server.controller.Controller)}.
	 * @throws Exception 
	 */
	@Test
	public void testCreateAction() throws Exception {
		game = new Model("src/main/resources/map.txt", 4);
		player = game.getPlayers().get(3);
		board = game.getBoard();
		permitcardToAdd = game.getBoard().getRegions(RegionType.COAST).getDeckOfPermitCard().getFsceUpPermitCard().get(0);
		
		// aggiungiamo a player una permicard per fare questo azione 
		player.getPermitCards().add(permitcardToAdd);
		selectedPermitCard = player.getPermitCards().get(0);
		
		 // la cita indicata nel permitcard per costruire emporium
		city = selectedPermitCard.getCities().get(0);
		 
		//mvc pattern
		MainActionController mainActionController = new MainActionController(game.getPlayers());
		BuildEmporiumUsingPermitCardView view = new BuildEmporiumUsingPermitCardView(player, city, board, selectedPermitCard);
		
		// numero di emporium nella cita prima della azione è 0
		System.out.println(city.getEmporium().size());
		assertTrue(0 == city.getEmporium().size());
		
		buildEmporium = view.createAction(mainActionController);
		
		view.registerObserver(mainActionController);
		view.unregisterObserver(mainActionController);

		// dopo la azione il permit card dovrebbe essere usata per non utilizzarlo più.
		System.out.println(player.getPermitCards().get(0).isUsed());
		//assertTrue(true == player.getPermitCards().get(0).isUsed());
		
		// numero di emporium nella cita dopo della azione è 1
		System.out.println(city.getEmporium().size());
		//assertTrue(1 == city.getEmporium().size());
		System.out.println(view.isValid());
	}

}
