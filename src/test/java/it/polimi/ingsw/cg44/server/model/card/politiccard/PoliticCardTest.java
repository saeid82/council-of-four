/**
 * 
 */
package it.polimi.ingsw.cg44.server.model.card.politiccard;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.model.colors.Colors;

/**
 * @author saeid
 *
 */
public class PoliticCardTest {
	private PoliticCard politicCard;
	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.card.politiccard.PoliticCard#PoliticCard(it.polimi.ingsw.cg44.server.model.colors.Colors)}.
	 */
	@Test
	public void testPoliticCardColors() {
		politicCard = new PoliticCard(Colors.BLACK);
		assertTrue(Colors.BLACK == politicCard.getCardColor());
	}



}
