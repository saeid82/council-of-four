/**
 * 
 */
package it.polimi.ingsw.cg44.server.controller.action.mainaction;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.controller.action.IAction;
import it.polimi.ingsw.cg44.server.model.Model;
import it.polimi.ingsw.cg44.server.model.board.RegionType;
import it.polimi.ingsw.cg44.server.model.bonus.BoughtPermitCardBonus;
import it.polimi.ingsw.cg44.server.model.bonus.MainActionBonus;
import it.polimi.ingsw.cg44.server.model.bonus.PermitCardBonus;
import it.polimi.ingsw.cg44.server.model.game.Player;

/**
 * @author saeid
 *
 */
public class AcquirePermitCardTest {
	private Model game;
	private IAction acquirePermitCard;
	private Player player1,player2;

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.controller.action.mainaction.AcquirePermitCard#AcquirePermitCard(it.polimi.ingsw.cg44.server.model.game.Board, it.polimi.ingsw.cg44.server.model.game.Player, it.polimi.ingsw.cg44.server.model.board.NormalRegion, java.util.List, int)}.
	 * @throws Exception 
	 */
	@Test
	public void testAcquirePermitCard(){
		
		try {
			game = new Model("src/main/resources/map.txt", 4);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		player2 = game.getPlayers().get(1);
		
		acquirePermitCard = new AcquirePermitCard(game.getBoard(), player2, 
				game.getBoard().getRegions(RegionType.COAST), game.getPlayers().get(1).getPoliticCards(), 0);
		

		
		System.out.println(acquirePermitCard.getClass());
		

	}

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.controller.action.mainaction.AcquirePermitCard#run()}.
	 */
	@Test
	public void testRun() throws Exception{
		game = new Model("src/main/resources/map.txt", 4);
		
		player1 = game.getPlayers().get(0);
		System.out.println(player1.getCoinsTrack().getCurrentCoinsNumber());
		//player1.getCoinsTrack().setCoinsNumber(5);
		
		
			if(!game.getBoard().getRegions(RegionType.HILL).getDeckOfPermitCard().getFsceUpPermitCard().get(0).getBonus().contains(BoughtPermitCardBonus.class) &&
					!game.getBoard().getRegions(RegionType.HILL).getDeckOfPermitCard().getFsceUpPermitCard().get(0).getBonus().contains(MainActionBonus.class) &&
					!game.getBoard().getRegions(RegionType.HILL).getDeckOfPermitCard().getFsceUpPermitCard().get(0).getBonus().contains(PermitCardBonus.class)){
				acquirePermitCard = new AcquirePermitCard(game.getBoard(), player1, game.getBoard().getRegions(RegionType.HILL), player1.getPoliticCards(), 0);
				System.out.println("The councillor in the balcony of the HILL region:\n"+game.getBoard().getRegions(RegionType.HILL).getBalcony().getBench());
				
				System.out.println("The politic card of the player1 befor action: "+player1.getPoliticCards());
				int moneyBeforActionPlyr1 = player1.getCoinsTrack().getCurrentCoinsNumber();
				System.out.println("The Coins number of player1 befor action= "+moneyBeforActionPlyr1);
				System.out.println("The nuber of permitcards of player1 before action: "+player1.getPermitCards().size());
				System.out.println("\nbefor\n"+player1);
		 
				//acquirePermitCard.run();
				
				System.out.println("\nAfter\n"+player1);
				System.out.println("The politic card of the player1 after action: "+game.getPlayers().get(0).getPoliticCards());
				int moneyAfterActionPlyr1 = player1.getCoinsTrack().getCurrentCoinsNumber();
				System.out.println("The Coins number of player1 after action= "+moneyAfterActionPlyr1);
				System.out.println("The number of permitcards of player1 after action:\n"+player1.getPermitCards().size());
			}

		

	}

}
