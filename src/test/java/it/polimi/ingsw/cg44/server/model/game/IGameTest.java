/**
 * 
 */
package it.polimi.ingsw.cg44.server.model.game;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.model.assistant.AssistantsReserve;
import it.polimi.ingsw.cg44.server.model.track.NobilityTrack;

/**
 * @author saeid
 *
 */
public class IGameTest {
	private Player player;
	
	private IGame game = new IGame() {
		
		
		@Override
		public List<Player> createPlayers(NobilityTrack nobilityTrack, AssistantsReserve assistantsReserve,
				int numberPlayers) throws Exception {
			player = new Player(nobilityTrack, assistantsReserve, 4);
			assertTrue(player.getAssistants().getClass() == 
					game.createPlayers(nobilityTrack, assistantsReserve, 4).get(0).getAssistants().getClass());
			return null;
		}
		
		@Override
		public Board createBoard(String file, int numberPlayers) throws Exception {
			int n= game.createBoard("src/main/resources/map.txt", 4).getAssistantsReserve().calcQtyAssis(4);
			assertTrue(30 == n);
			return null;
		}
	};
	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.game.IGame#createBoard(java.lang.String, int)}.
	 * @throws Exception 
	 */
	@Test
	public void testCreateBoard() throws Exception {
		
	
	}

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.game.IGame#createPlayers(it.polimi.ingsw.cg44.server.model.track.NobilityTrack, it.polimi.ingsw.cg44.server.model.assistant.AssistantsReserve, int)}.
	 * @throws Exception 
	 */
	@Test
	public void testCreatePlayers() throws Exception {

	}

}
