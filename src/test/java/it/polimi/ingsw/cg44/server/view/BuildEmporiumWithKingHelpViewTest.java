/**
 * 
 */
package it.polimi.ingsw.cg44.server.view;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.controller.MainActionController;
import it.polimi.ingsw.cg44.server.controller.action.IAction;
import it.polimi.ingsw.cg44.server.model.Model;
import it.polimi.ingsw.cg44.server.model.city.City;
import it.polimi.ingsw.cg44.server.model.game.Board;
import it.polimi.ingsw.cg44.server.model.game.Player;

/**
 * @author saeid
 *
 */
public class BuildEmporiumWithKingHelpViewTest {
	private Model game;
	private Player player;
	private City nextCity;
	private Board board;
	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.view.BuildEmporiumWithKingHelpView#createAction(it.polimi.ingsw.cg44.server.controller.Controller)}.
	 * @throws Exception 
	 */
	@Test
	public void testCreateAction() throws Exception {
		game = new Model("src/main/resources/map.txt", 4);
		player = game.getPlayers().get(3);
		//player.getCoinsTrack().setCoinsNumber(10);
		nextCity = game.getBoard().getKing().getCity().getWays().get(0);
		board = game.getBoard();
		
		MainActionController mainActionController = new MainActionController(game.getPlayers());
		BuildEmporiumWithKingHelpView view = new BuildEmporiumWithKingHelpView(player, board, nextCity);
		
		IAction buildEmporium = view.createAction(mainActionController);
		buildEmporium.run();
		
		view.registerObserver(mainActionController);
		view.unregisterObserver(mainActionController);
		assertTrue(false == view.isValid());
	}

}
