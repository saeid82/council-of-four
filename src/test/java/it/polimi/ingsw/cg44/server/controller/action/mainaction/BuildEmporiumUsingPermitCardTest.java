/**
 * 
 */
package it.polimi.ingsw.cg44.server.controller.action.mainaction;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.controller.action.IAction;
import it.polimi.ingsw.cg44.server.model.Model;
import it.polimi.ingsw.cg44.server.model.board.RegionType;

/**
 * ho messo il methodo run() come commento perche ci vuole interazione con client
 * @author saeid
 *
 */
public class BuildEmporiumUsingPermitCardTest {
	private Model game;
	private IAction action;
	
	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.controller.action.mainaction.BuildEmporiumUsingPermitCard#BuildEmporiumUsingPermitCard(it.polimi.ingsw.cg44.server.model.game.Player, it.polimi.ingsw.cg44.server.model.game.Board, it.polimi.ingsw.cg44.server.model.city.City, it.polimi.ingsw.cg44.server.model.card.permitcard.PermitCard)}.
	 * @throws Exception 
	 */
	@Test
	public void testBuildEmporiumUsingPermitCard() {
		try {
			game = new Model("src/main/resources/map.txt", 4);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// al inizio giocatore non ha nessun permitcard
		System.out.println(game.getPlayers().get(0).getPermitCards());
		
		// quindi ci da il numeri di permit card a possesso zero
		System.out.println(game.getPlayers().get(0).getPermitCards().size());
		assertTrue(0 == game.getPlayers().get(0).getPermitCards().size() );
		
		// aggiongiamo a giocatore un permitcard per fare azione
		game.getPlayers().get(0).getPermitCards().add(game.getBoard().getRegions(RegionType.HILL).getDeckOfPermitCard().getDeck().poll());
		
		// ora player ha 1 solo permitcard
		System.out.println(game.getPlayers().get(0).getPermitCards().size());
		assertTrue(1 == game.getPlayers().get(0).getPermitCards().size() );
		
		action = new BuildEmporiumUsingPermitCard(game.getPlayers().get(0), game.getBoard(), 
				game.getBoard().getCities().get(2), game.getPlayers().get(0).getPermitCards().get(0));
		
		//action.run();
	}


	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.controller.action.mainaction.BuildEmporiumUsingPermitCard#obtainBonusOfConnectedCities()}.
	 * @throws Exception 
	 */
	@Test
	public void testObtainBonusOfConnectedCities() {
		try {
			game = new Model("src/main/resources/map.txt", 4);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// al inizio giocatore non ha nessun permitcard
		System.out.println(game.getPlayers().get(0).getPermitCards());
		
		// quindi ci da il numeri di permit card a possesso zero
		System.out.println(game.getPlayers().get(0).getPermitCards().size());
		assertTrue(0 == game.getPlayers().get(0).getPermitCards().size() );
		
		// aggiongiamo a giocatore un permitcard per fare azione
		game.getPlayers().get(0).getPermitCards().add(game.getBoard().getRegions(RegionType.HILL).getDeckOfPermitCard().getDeck().poll());
		
		// ora player ha 1 solo permitcard
		System.out.println(game.getPlayers().get(0).getPermitCards().size());
		assertTrue(1 == game.getPlayers().get(0).getPermitCards().size() );
		
		// create action
		BuildEmporiumUsingPermitCard action = new BuildEmporiumUsingPermitCard(game.getPlayers().get(0), game.getBoard(), 
				game.getBoard().getCities().get(2), game.getPlayers().get(0).getPermitCards().get(0));
		//action.obtainBonusOfConnectedCities();
		
		try {
			action.checkBonusRegion();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			action.checkBonusCityColor();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public IAction getAction() {
		return action;
	}



}
