/**
 * 
 */
package it.polimi.ingsw.cg44.server.model.game;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.model.Model;
import it.polimi.ingsw.cg44.server.model.board.RegionType;
import it.polimi.ingsw.cg44.server.model.card.permitcard.PermitCard;
import it.polimi.ingsw.cg44.server.model.city.Emporium;
import it.polimi.ingsw.cg44.server.model.track.MarkerDisk;


/**
 * @author saeid
 *
 */
public class PlayerTest {
	private Model game;
	private Player player;
	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.game.Player#Player(it.polimi.ingsw.cg44.server.model.track.NobilityTrack, it.polimi.ingsw.cg44.server.model.assistant.AssistantsReserve, int)}.
	 * @throws Exception 
	 */
	@Test
	public void testPlayer(){
		try {
			game = new Model("src/main/resources/map.txt", 4);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			player = new Player(game.getBoard().getNobilityTrack(), game.getBoard().getAssistantsReserve(), 4);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertTrue(game.getPlayers().get(0).getClass() == player.getClass());
		
		System.out.println(game.getPlayers().get(0).getId());
		System.out.println(game.getPlayers().get(1).getId());
		
		 //Test: player at the begging non deve avere permitcard
		 //giocatore 2 pesca una carta di permesso tra i due con faccia in su.
		 //dopo aggionge la carta pescata nel suo mazzo
		 //il numero del suo permitcard pescata da 0 diventa 1.
		// player at the begging non deve avere permitcard
		assertTrue(true == game.getPlayers().get(2).getPermitCards().isEmpty() );
		// giocatore 2 pesca una carta di permesso tra i due con faccia in su
		PermitCard card = game.getBoard().getRegions(RegionType.COAST).getDeckOfPermitCard().drawFaceUpPermitCard(0);
		// dopo aggionge la carta pescata nel suo mazzo
		game.getPlayers().get(2).getPermitCards().add(card);

		// il numero del suo permitcard pescata da 0 diventa 1.
		assertTrue(1 == game.getPlayers().get(2).getPermitCards().size());
		
		
		
		 // player all'inizio non ha nessuna cita.
		 // ma ha 10 emporium in riserva
		 // toglie un emporium dalla sua riserva per costruire un emporium nella cita con indice 0 nella regione COAST.
		 // la cità ScheduledThreadPoolExecutor al InitializationError initializationError ha nessun emporium
		 // costruisce un emporium con quella prelevata:
		 // controlliamo il numero di emporium contenuto nella citta 
		 // ora il giocatore deve avere 9 emporium nella sua riserva
		
		// player all'inizio non ha nessuna cita
		assertTrue(true == game.getPlayers().get(0).getCities().isEmpty());
		// ma ha 10 emporium in riserva
		assertTrue(10 == game.getPlayers().get(0).getEmporiums().size());
		// toglie un emporium dalla sua riserva per costruire un emporium nella cita con indice 0 nella regione COAST.
		Emporium emporium1 = game.getPlayers().get(0).getEmporiums().poll();
		//la cità ScheduledThreadPoolExecutor al InitializationError initializationError ha nessun emporium
		//System.out.println(game.getBoard().getRegions(RegionType.COAST).getCities().get(0).getEmporium().size());
		assertTrue(0 == game.getBoard().getRegions(RegionType.COAST).getCities().get(0).getEmporium().size());
		// costruisce un emporium con quella prelevata:
		game.getBoard().getRegions(RegionType.COAST).getCities().get(0).addEmporium(emporium1);
		// controlliamo il numero di emporium contenuto nella citta 
		//System.out.println(game.getBoard().getRegions(RegionType.COAST).getCities().get(0).getEmporium().size());
		assertTrue(1 == game.getBoard().getRegions(RegionType.COAST).getCities().get(0).getEmporium().size());
		
		// ora il giocatore deve avere 9 emporium nella sua riserva
		assertTrue(9 == game.getPlayers().get(0).getEmporiums().size());
		
		
		// giocatore al inizio ha 6 politic card
		assertTrue(6 == game.getPlayers().get(0).getPoliticCards().size());
		
		
		// player ha 9 emporium in riserva
		System.out.println(game.getPlayers().get(0).getEmporiums().size());
		
		// toglie un emporium dalla sua riserva per costruire un emporium nella cita con indice 0 nella regione COAST.
		Emporium emporium2 = game.getPlayers().get(0).getEmporiums().poll();
		
		// costruisce un emporium con quella prelevata:
		game.getBoard().getRegions(RegionType.COAST).getCities().get(0).addEmporium(emporium2);
		
		
		// ovviamente al inzio è 0.
		assertTrue(0 == game.getPlayers().get(0).getVictoryTrack().getCurrentVictoryPoints() );
		
		
		MarkerDisk markerDisk = new MarkerDisk(game.getBoard().getNobilityTrack());
		assertTrue(markerDisk.getClass() == game.getPlayers().get(0).getMarkerDisk().getClass());
		
	}







}
