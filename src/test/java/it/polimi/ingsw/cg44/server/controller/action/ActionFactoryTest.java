/**
 * 
 */
package it.polimi.ingsw.cg44.server.controller.action;



import org.junit.Test;

import it.polimi.ingsw.cg44.server.model.Model;
import it.polimi.ingsw.cg44.server.model.board.RegionType;
import it.polimi.ingsw.cg44.server.model.colors.Colors;

/**
 * @author saeid
 *
 */
public class ActionFactoryTest {
	private Model game;
	private ActionFactory actionFactory = new ActionFactory();


	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.controller.action.ActionFactory#getBuildEmporiumUsingPermitCardAction(it.polimi.ingsw.cg44.server.model.game.Player, it.polimi.ingsw.cg44.server.model.game.Board, it.polimi.ingsw.cg44.server.model.city.City, it.polimi.ingsw.cg44.server.model.card.permitcard.PermitCard)}.
	 * @throws Exception 
	 */
	@Test
	public void testGetBuildEmporiumUsingPermitCardAction() {
		try {
			game = new Model("src/main/resources/map.txt", 4);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// aggiongiamo a giocatore un permitcard per fare azione
		game.getPlayers().get(0).getPermitCards().add(game.getBoard().getRegions(RegionType.HILL).getDeckOfPermitCard().getDeck().poll());
		
		IAction action = actionFactory.getBuildEmporiumUsingPermitCardAction(game.getPlayers().get(0), game.getBoard(), 
				game.getBoard().getCities().get(2), game.getPlayers().get(0).getPermitCards().get(0));
		System.out.println(action.getClass());
		
		
		
		IAction electCouncillorAction = actionFactory.getElectCouncillorAction(game.getPlayers().get(0), game.getBoard(), 
				game.getBoard().getRegions(RegionType.HILL), Colors.PINK);
		
		System.out.println(electCouncillorAction.getClass());
		
		
		
		
		IAction changeFaceUpPermitCardAction = actionFactory.getChangeFaceUpPermitCardAction(game.getPlayers().get(0), game.getBoard(), 
				game.getBoard().getRegions(RegionType.HILL));
		System.out.println(changeFaceUpPermitCardAction.getClass());
	}





}
