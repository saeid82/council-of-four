/**
 * 
 */
package it.polimi.ingsw.cg44.server.model.card.politiccard;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.model.colors.Colors;

/**
 * @author saeid
 *
 */
public class PoliticCardCacheTest {
	private PoliticCardCache politicCardCache;
	private PoliticCard politicCard ;
	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.card.politiccard.PoliticCardCache#getPoliticCard()}.
	 */
	@Test
	public void testGetPoliticCard() {
		politicCardCache = new PoliticCardCache();
		politicCard =  new PoliticCard(Colors.WHITE);
		System.out.println(politicCardCache.getPoliticCard().getClass());
		assertTrue(politicCard.getClass() == politicCardCache.getPoliticCard().getClass());
	}

}
