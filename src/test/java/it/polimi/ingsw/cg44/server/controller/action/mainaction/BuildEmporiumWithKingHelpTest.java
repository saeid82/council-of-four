/**
 * 
 */
package it.polimi.ingsw.cg44.server.controller.action.mainaction;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.controller.action.IAction;
import it.polimi.ingsw.cg44.server.model.Model;
import it.polimi.ingsw.cg44.server.model.city.City;
import it.polimi.ingsw.cg44.server.model.game.Player;

/**
 * @author saeid
 *
 */
public class BuildEmporiumWithKingHelpTest {
	
	private Model game;
	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.controller.action.mainaction.BuildEmporiumWithKingHelp#BuildEmporiumWithKingHelp(it.polimi.ingsw.cg44.server.model.game.Player, it.polimi.ingsw.cg44.server.model.game.Board, it.polimi.ingsw.cg44.server.model.city.City)}.
	 * @throws Exception 
	 */
	@Test
	public void testBuildEmporiumWithKingHelp(){
		try {
			game = new Model("src/main/resources/map.txt", 4);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Player player = game.getPlayers().get(0);
		City nextCity = game.getBoard().getKing().getCity().getWays().get(0);
		IAction action = new BuildEmporiumWithKingHelp(player, game.getBoard(), nextCity);
		System.out.println(action.getClass());
		
		Player player1 = game.getPlayers().get(1);
		

		
		City nextCity1 = game.getBoard().getKing().getCity().getWays().get(1);
		IAction action1 = new BuildEmporiumWithKingHelp(player1, game.getBoard(), nextCity1);
		
		//numero di emporium dentro next city prima del azione è 0
		assertTrue(0 == nextCity1.getEmporium().size());
		
		action1.run();
		
		//numero di emporium dentro nextcity dopo del azione è 1
		assertTrue(1 == nextCity1.getEmporium().size());
	}


}
