/**
 * 
 */
package it.polimi.ingsw.cg44.server.model.map;

import static org.junit.Assert.*;

import java.io.BufferedReader;

import java.io.IOException;


import org.junit.Test;

/**
 * @author saeid
 *
 */
public class MapTest {
	private BufferedReader map , noMap;

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.map.Map#readFile(java.lang.String)}.
	 * @throws IOException 
	 */
	@Test
	public void testReadFile() throws IOException {
			
		map = Map.readFile("src/main/resources/map.txt");
		assertTrue(0 == map.readLine().compareTo("START_CITY_INITIALIZATION"));
		
		noMap = Map.readFile("src/main/resources/ap.txt");
		assertTrue(null == noMap);
		
	
	}


}
