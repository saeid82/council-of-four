/**
 * 
 */
package it.polimi.ingsw.cg44.server.view;


import java.util.List;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.controller.MainActionController;
import it.polimi.ingsw.cg44.server.controller.action.IAction;
import it.polimi.ingsw.cg44.server.model.Model;
import it.polimi.ingsw.cg44.server.model.board.NormalRegion;
import it.polimi.ingsw.cg44.server.model.board.RegionType;
import it.polimi.ingsw.cg44.server.model.bonus.BoughtPermitCardBonus;
import it.polimi.ingsw.cg44.server.model.bonus.MainActionBonus;
import it.polimi.ingsw.cg44.server.model.card.politiccard.PoliticCard;
import it.polimi.ingsw.cg44.server.model.game.Board;
import it.polimi.ingsw.cg44.server.model.game.Player;

/**
 * @author saeid
 *
 */
public class AcquirePermitCardViewTest {
	private Model game;
	private Player player;
	private List<PoliticCard> politicCards;
	private Board board;
	private NormalRegion targetRegion;
	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.view.AcquirePermitCardView#createAction(it.polimi.ingsw.cg44.server.controller.Controller)}.
	 * @throws Exception 
	 */
	@Test
	public void testCreateAction() throws Exception {
		game = new Model("src/main/resources/map.txt", 4);
		player = game.getPlayers().get(0);
		player.getCoinsTrack().setCoinsNumber(10);
		int coinsbefor = player.getCoinsTrack().getCurrentCoinsNumber();
		System.out.println("The coins before action: "+coinsbefor);
		System.out.println("the size of permitcard prima del azione: "+player.getPermitCards().size());
		
		
		politicCards = player.getPoliticCards();
		System.out.println("\nLe carte politice del player prima di azione: "+politicCards);
		board = game.getBoard();
		targetRegion = board.getRegions(RegionType.HILL);

		MainActionController mainActionController = new MainActionController(game.getPlayers());
		AcquirePermitCardView view = new AcquirePermitCardView(player, targetRegion, board, politicCards, 1);
		
		System.out.println("\n i consiglieri Del region HILL:\n"+targetRegion.getBalcony().getBench());
		
		IAction acquirePermitcard = view.createAction(mainActionController);
		
		//acquirePermitcard.run();
		
		view.registerObserver(mainActionController);
		
		view.unregisterObserver(mainActionController);
		
		//assertTrue(false == view.isValid());
		

		System.out.println("the coins after action: "+player.getCoinsTrack().getCurrentCoinsNumber());
		
		System.out.println("the size of permitcard dopo del azione: "+player.getPermitCards().size());
		
		System.out.println("\nLe carte politice rimaste del player dopo azione: "+politicCards);
		
	}


}
