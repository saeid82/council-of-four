/**
 * 
 */
package it.polimi.ingsw.cg44.server.controller.action.quickaction;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.model.Model;
import it.polimi.ingsw.cg44.server.model.assistant.Assistant;
import it.polimi.ingsw.cg44.server.model.game.Player;

/**
 * The player return 3 assistants to the pool and perform 2 main action instead of one in this
 * turn(it is also possible to perform the same action twice.)
 * @author saeid
 *
 */
public class PerformAdditionalMainActionTest {
	private Model game;
	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.controller.action.quickaction.PerformAdditionalMainAction#PerformAdditionalMainAction(it.polimi.ingsw.cg44.server.model.game.Player, it.polimi.ingsw.cg44.server.model.game.Board)}.
	 * @throws Exception 
	 */
	@Test
	public void testPerformAdditionalMainAction() {

		try {
			game = new Model("src/main/resources/map.txt", 4);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Player player = game.getPlayers().get(0);
		PerformAdditionalMainAction performAdditionalMainAction = new PerformAdditionalMainAction(player, game.getBoard());
		
		// aggiungiamo al giocatore un pò assistanti per fare test
		for(int i=0;i<=5;i++)
			player.getAssistants().add(new Assistant());
		
		
		
		// numero di assistanti del giocarore prima della azione
		int assBefor = player.getAssistants().size();
		// numero di assistanti della riserva prima della zione
		int assReserveBefore = game.getBoard().getAssistantsReserve().getAssistants().size();
		
		performAdditionalMainAction.run();
		
		// numero di assistanti del giocatore dopo della azione
		int assAfter = player.getAssistants().size();
		// numero di assistanti della riserva dopo della zione
		int assReserveAfter = game.getBoard().getAssistantsReserve().getAssistants().size();
		
		assertTrue(assBefor == assAfter+3);
		assertTrue(assReserveBefore == assReserveAfter-3);
	}

}
