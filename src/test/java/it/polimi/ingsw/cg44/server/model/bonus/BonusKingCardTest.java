/**
 * 
 */
package it.polimi.ingsw.cg44.server.model.bonus;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.model.Model;

/**
 * @author saeid
 *
 */
public class BonusKingCardTest {

	private BonusKingCard bonusKingCard = new BonusKingCard();
	private VictoryPointBonus victoryPointBonus = new VictoryPointBonus();
	private Model game;;

	/**
	 * se il bonus non è usata ancora, dovebbe tornare false.
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.bonus.BonusKingCard#isUsed()}.
	 */
	@Test
	public void testIsUsed() {
		assertTrue(false == bonusKingCard.isUsed());
	}

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.bonus.BonusKingCard#useCard(it.polimi.ingsw.cg44.server.model.game.Player, it.polimi.ingsw.cg44.server.model.game.Board)}.
	 */
	@Test
	public void testUseBonus() throws Exception{

		game = new Model("src/main/resources/map.txt", 4);
		
		// il bonus non è ancora usata
		assertTrue(false == bonusKingCard.isUsed());
		
		// punto vittoria prima del usare il bonus 
		int beforUsingBonus = game.getPlayers().get(0).getVictoryTrack().getCurrentVictoryPoints();
		assertTrue(0 == beforUsingBonus);
		
		//usare il bonus
		try {
			bonusKingCard.useBonus(game.getPlayers().get(0), game.getBoard());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// il bonus è gia usata
		assertTrue(true == bonusKingCard.isUsed());
		
		int afterUsingBonus = game.getPlayers().get(0).getVictoryTrack().getCurrentVictoryPoints();
		
		assertTrue(afterUsingBonus != beforUsingBonus);
	}

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.bonus.BonusKingCard#getBonus()}.
	 */
	@Test
	public void testGetBonus() {
		assertTrue(bonusKingCard.getBonus().getClass() == victoryPointBonus.getClass());
	}

}
