/**
 * 
 */
package it.polimi.ingsw.cg44.server.model.balcony;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.model.colors.Colors;

/**
 * @author saeid
 *
 */
public class CouncillorTest {
	private Councillor councillor;
	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.balcony.Councillor#getColors()}.
	 */
	@Test
	public void testGetColors() {
		councillor=new Councillor(Colors.BLACK);
		assertTrue(Colors.BLACK==councillor.getColors());
	}

}
