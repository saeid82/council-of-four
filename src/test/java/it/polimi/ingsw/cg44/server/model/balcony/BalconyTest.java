/**
 * 
 */
package it.polimi.ingsw.cg44.server.model.balcony;

import static org.junit.Assert.*;

import java.util.Deque;
import java.util.LinkedList;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.model.colors.Colors;


/**
 * @author saeid
 *
 */
public class BalconyTest {
	private Deque<Councillor> bench = new LinkedList<>();
	private Balcony balcony;
	private Councillor councillor;
	private CouncillorReserve councillorReserve;
	/**
	 * Test: Controllo che Balcony sia un arrayList di Councillor
	 */


	@Test
	public void testGetBench() {
		balcony = new Balcony();
		assertEquals(bench, balcony.getBench());
	}



	/**
	 * Test: councillor scelto dalla riserva e messo in balcony sia uguale con l'ultimo entrata.
	 */
	@Test
	public void testElectCouncillor() {
		balcony = new Balcony();
		councillor = bench.pollLast();
		councillorReserve = new CouncillorReserve();
		councillorReserve.getCouncillorInReserve().add(councillor);
		bench.push(new Councillor(Colors.ORANGE));
		assertTrue(Colors.ORANGE==bench.getLast().getColors());
		balcony.electCouncillor(councillorReserve, councillor);
	}

}
