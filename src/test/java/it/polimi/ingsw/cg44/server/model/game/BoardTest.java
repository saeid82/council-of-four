/**
 * 
 */
package it.polimi.ingsw.cg44.server.model.game;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.model.Model;
import it.polimi.ingsw.cg44.server.model.board.KingRegion;
import it.polimi.ingsw.cg44.server.model.board.RegionType;
import it.polimi.ingsw.cg44.server.model.colors.Colors;
import it.polimi.ingsw.cg44.server.model.king.King;
import it.polimi.ingsw.cg44.server.model.track.NobilityTrack;

/**
 * @author saeid
 *
 */
public class BoardTest {

	private Model game;
	private KingRegion kingRegion;
	private King king;
	private NobilityTrack nobilityTrack;





	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.game.Board#getNobilityTrack()}.
	 * @throws Exception 
	 */
	@Test
	public void testGetNobilityTrack(){
		try {
			game = new Model("src/main/resources/map.txt", 4);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		nobilityTrack = new NobilityTrack();
		assertTrue(nobilityTrack.getClass() == game.getBoard().getNobilityTrack().getClass());
		assertTrue(20 == game.getBoard().getNobilityTrack().getTrack().size());
		
		
		assertTrue(30 == game.getBoard().getAssistantsReserve().calcQtyAssis(4));
		
		
		king = new King();
		assertTrue(king.getClass() == game.getBoard().getKing().getClass());
		assertTrue(Colors.KING == game.getBoard().getKing().getCity().getColor());
		
		int size = game.getBoard().getCouncillorReserve().getCouncillorInReserve().size();
		assertTrue(8 == size);
		
		assertTrue(15 == game.getBoard().getCities().size());
		
		assertTrue(KingRegion.class == game.getBoard().getKingRegion().getClass());
		
		assertTrue(RegionType.MOUNTAIN == game.getBoard().getRegions(RegionType.MOUNTAIN).getRegionType());
	}


}
