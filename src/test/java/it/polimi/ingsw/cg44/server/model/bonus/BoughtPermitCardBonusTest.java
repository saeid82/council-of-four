/**
 * 
 */
package it.polimi.ingsw.cg44.server.model.bonus;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.model.Model;
import it.polimi.ingsw.cg44.server.model.board.RegionType;
import it.polimi.ingsw.cg44.server.model.card.permitcard.PermitCard;

/**
 * @author saeid
 *
 */
public class BoughtPermitCardBonusTest {
	private Model game;
	private PermitCard permitCard;
	/**
	 * testing che la carta face up primo sia uguale con la carta pescata 
	 * testing che la carta utilitizzata sia segnalata USATA per non utilizzarelo più.
	 * @throws Exception 
	 */
	@Test
	public void testUseBonus(){
		try {
			game = new Model("src/main/resources/map.txt", 4);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		permitCard = game.getBoard().getRegions(RegionType.COAST).getDeckOfPermitCard().getFsceUpPermitCard().get(0);
		System.out.print(permitCard);
		System.out.print(permitCard.isUsed());
		try {
			permitCard.useCard();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertFalse(false== permitCard.isUsed());
		assertEquals(permitCard, game.getBoard().getRegions(RegionType.COAST).getDeckOfPermitCard().drawFaceUpPermitCard(0));
		
		
	}

}
