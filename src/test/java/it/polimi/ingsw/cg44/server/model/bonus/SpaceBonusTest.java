/**
 * 
 */
package it.polimi.ingsw.cg44.server.model.bonus;

import static org.junit.Assert.*;

import org.junit.Test;


/**
 * @author saeid
 *
 */
public class SpaceBonusTest {
	private SpaceBonus spaceBonus;
	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.bonus.SpaceBonus#SpaceBonus(int)}.
	 */
	@Test
	public void testSpaceBonusInt() {
		spaceBonus = new SpaceBonus(2);
		System.out.println(spaceBonus.getSpaceNumber());
	}

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.bonus.SpaceBonus#SpaceBonus()}.
	 */
	@Test
	public void testSpaceBonus() {
		spaceBonus = new SpaceBonus();
		assertTrue(0 == spaceBonus.getSpaceNumber());
		
	}

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.bonus.SpaceBonus#getBonus()}.
	 */
	@Test
	public void testGetBonus() {
		spaceBonus = new SpaceBonus();
		System.out.println(spaceBonus.getBonus().size());
	}


}
