/**
 * 
 */
package it.polimi.ingsw.cg44.server.controller.action.quickaction;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.model.Model;
import it.polimi.ingsw.cg44.server.model.game.Player;

/**
 * The player pays three coins, take an assistant from the pool,
 * @author saeid
 *
 */
public class EngageAssistantTest {
	private Model game;
	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.controller.action.quickaction.EngageAssistant#EngageAssistant(it.polimi.ingsw.cg44.server.model.game.Player, it.polimi.ingsw.cg44.server.model.game.Board)}.
	 * @throws Exception 
	 */
	@Test
	public void testEngageAssistant() throws Exception {

		game = new Model("src/main/resources/map.txt", 4);
		Player player = game.getPlayers().get(0);
		EngageAssistant engageAssistant = new EngageAssistant(player, game.getBoard());
		//The player pays three coins, take an assistant from the pool
		
		//il numero dei assistanti nella riserva prima della azione:
		int assReserveBefor = game.getBoard().getAssistantsReserve().getAssistants().size();
		//current coins number of player before action
		int coinsBefore = player.getCoinsTrack().getCurrentCoinsNumber();
		
		engageAssistant.run();
		
		//il numero dei assistanti nella riserva dopo della azione:
		int assReserveAfter = game.getBoard().getAssistantsReserve().getAssistants().size();
		//current coins number of player after action
		int coinsAfter = player.getCoinsTrack().getCurrentCoinsNumber();
		
		assertTrue(coinsBefore == coinsAfter+3);
		assertTrue(assReserveBefor == assReserveAfter+1);
	}

}
