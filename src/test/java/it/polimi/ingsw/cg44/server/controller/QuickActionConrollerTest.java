/**
 * 
 */
package it.polimi.ingsw.cg44.server.controller;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.model.Model;
import it.polimi.ingsw.cg44.server.model.board.NormalRegion;
import it.polimi.ingsw.cg44.server.model.board.RegionType;
import it.polimi.ingsw.cg44.server.model.colors.Colors;
import it.polimi.ingsw.cg44.server.model.game.Board;
import it.polimi.ingsw.cg44.server.model.game.Player;
import it.polimi.ingsw.cg44.server.view.ChangeFaceUpPermitCardView;
import it.polimi.ingsw.cg44.server.view.EngageAssistantView;
import it.polimi.ingsw.cg44.server.view.PerformAnotherMainActionView;
import it.polimi.ingsw.cg44.server.view.SendAssistantToElectCouncillorView;

/**
 * @author saeid
 *
 */
public class QuickActionConrollerTest {
	private Model game;

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.controller.QuickActionConroller#update(it.polimi.ingsw.cg44.server.view.EngageAssistantView)}.
	 * @throws Exception 
	 */
	@Test
	public void testUpdateEngageAssistantView(){

		try {
			game = new Model("src/main/resources/map.txt", 4);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Player player = game.getPlayers().get(0);
		Board board = game.getBoard();
		NormalRegion normalRegion = board.getRegions(RegionType.HILL);
		
		QuickActionConroller quickActionConroller = new QuickActionConroller(game.getPlayers());
		EngageAssistantView view = new EngageAssistantView(player, board);
		
		view.createAction(quickActionConroller);
		quickActionConroller.update(view);
	}

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.controller.QuickActionConroller#update(it.polimi.ingsw.cg44.server.view.ChangeFaceUpPermitCardView)}.
	 * @throws Exception 
	 */
	@Test
	public void testUpdateChangeFaceUpPermitCardView() throws Exception {

		game = new Model("src/main/resources/map.txt", 4);
		Player player = game.getPlayers().get(0);
		Board board = game.getBoard();
		NormalRegion normalRegion = board.getRegions(RegionType.HILL);
		
		QuickActionConroller quickActionConroller1 = new QuickActionConroller(game.getPlayers());
		ChangeFaceUpPermitCardView view1 = new ChangeFaceUpPermitCardView(player, normalRegion, board);
		
		view1.createAction(quickActionConroller1);
		quickActionConroller1.update();
	}

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.controller.QuickActionConroller#update(it.polimi.ingsw.cg44.server.view.PerformAnotherMainActionView)}.
	 * @throws Exception 
	 */
	@Test
	public void testUpdatePerformAnotherMainActionView() {

		try {
			game = new Model("src/main/resources/map.txt", 4);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Player player = game.getPlayers().get(0);
		Board board = game.getBoard();
		
		QuickActionConroller quickActionConroller = new QuickActionConroller(game.getPlayers());
		PerformAnotherMainActionView view = new PerformAnotherMainActionView(player, board);
		
		try {
			view.createAction(quickActionConroller);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		quickActionConroller.update(view);
	}

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.controller.QuickActionConroller#update(it.polimi.ingsw.cg44.server.view.SendAssistantToElectCouncillorView)}.
	 * @throws Exception 
	 */
	@Test
	public void testUpdateSendAssistantToElectCouncillorView()  {

		try {
			game = new Model("src/main/resources/map.txt", 4);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Player player = game.getPlayers().get(0);
		Board board = game.getBoard();
		NormalRegion targetRegion = board.getRegions(RegionType.HILL);
		Colors colore = board.getCouncillorReserve().getCouncillorInReserve().get(0).getColors();
		
		QuickActionConroller quickActionConroller = new QuickActionConroller(game.getPlayers());
		SendAssistantToElectCouncillorView view = new SendAssistantToElectCouncillorView(player, targetRegion, board, colore);
		
		view.createAction(quickActionConroller);
		quickActionConroller.update(view);
	}

}
