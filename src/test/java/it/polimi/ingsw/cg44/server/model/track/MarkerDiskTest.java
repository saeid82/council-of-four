/**
 * 
 */
package it.polimi.ingsw.cg44.server.model.track;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.model.Model;

/**
 * @author saeid
 *
 */
public class MarkerDiskTest {
	private Model game;
	private MarkerDisk markerDisk;
	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.track.MarkerDisk#MarkerDisk(it.polimi.ingsw.cg44.server.model.track.NobilityTrack)}.
	 * @throws Exception 
	 */
	@Test
	public void testMarkerDisk() throws Exception {
		game = new Model("src/main/resources/map.txt", 4);
		markerDisk = new MarkerDisk(game.getBoard().getNobilityTrack());
		System.out.println(markerDisk.getCurrentSpace());
		
	}

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.track.MarkerDisk#getCurrentSpace()}.
	 * @throws Exception 
	 */
	@Test
	public void testGetCurrentSpace() throws Exception {
		game = new Model("src/main/resources/map.txt", 4);
		markerDisk = new MarkerDisk(game.getBoard().getNobilityTrack());
		assertTrue(0 == markerDisk.getCurrentSpace().getSpaceNumber());
	}

	/**
	 * Test: spazio corrente=0
	 * spostiamo uno
	 * spostiamo ancora uno
	 * @throws Exception 
	 */
	@Test
	public void testNextCurrentSpace() throws Exception {
		game = new Model("src/main/resources/map.txt", 4);
		markerDisk = new MarkerDisk(game.getBoard().getNobilityTrack());
		// Spazio corrente
		int currentSpace = markerDisk.getCurrentSpace().getSpaceNumber();
		assertTrue(0 == currentSpace);
		// spostiamolo una casella
		int nextSpace1 = markerDisk.nextCurrentSpace().getSpaceNumber();
		assertTrue(1 == nextSpace1);
		// ancora lo portiamo avanti una casella
		int nextSpace2 = markerDisk.nextCurrentSpace().getSpaceNumber();
		assertTrue(2 == nextSpace2);
	}

}
