/**
 * 
 */
package it.polimi.ingsw.cg44.server.controller.action.mainaction;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.model.Model;
import it.polimi.ingsw.cg44.server.model.balcony.Councillor;
import it.polimi.ingsw.cg44.server.model.board.RegionType;
import it.polimi.ingsw.cg44.server.model.game.Player;

/**
 * @author saeid
 *
 */
public class ElectCouncillorTest {
	
	private Model game;
	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.controller.action.mainaction.ElectCouncillor#ElectCouncillor(it.polimi.ingsw.cg44.server.model.game.Player, it.polimi.ingsw.cg44.server.model.game.Board, it.polimi.ingsw.cg44.server.model.board.Region, it.polimi.ingsw.cg44.server.model.colors.Colors)}.
	 * @throws Exception 
	 */
	@Test
	public void testElectCouncillor() throws Exception {
		
		game = new Model("src/main/resources/map.txt", 4);
		Player player = game.getPlayers().get(0);
		Councillor electedCouncillor = game.getBoard().getCouncillorReserve().getCouncillorInReserve().get(0);
		System.out.println("Elected councillor from reserve: "+electedCouncillor);
		System.out.println(game.getBoard().getRegions(RegionType.HILL).getBalcony().getBench());
		ElectCouncillor electCouncillor = new ElectCouncillor(player, game.getBoard(), 
				game.getBoard().getRegions(RegionType.HILL), electedCouncillor.getColors());
		
		electCouncillor.run();
		System.out.println(game.getBoard().getRegions(RegionType.HILL).getBalcony().getBench());
		// the councillor selected from reserve is the same councillor in the balcony at the first position
		assertTrue(electedCouncillor == game.getBoard().getRegions(RegionType.HILL).getBalcony().getBench().getFirst());
		
	}



}
