/**
 * 
 */
package it.polimi.ingsw.cg44.server.model.bonus;

import static org.junit.Assert.*;

import org.junit.Test;
import it.polimi.ingsw.cg44.server.model.Model;


/**
 * @author saeid
 *
 */
public class AssistantsBonusTest {
	private AssistantsBonus assistantsBonus = new AssistantsBonus();;
	private Model game;

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.bonus.AssistantsBonus#getQuantity()}.
	 */
	@Test
	public void testGetQuantity() { 
		System.out.println(assistantsBonus.getQuantity());
		assertTrue(assistantsBonus.getQuantity()<6 && assistantsBonus.getQuantity()>0);
	}

	/**
	 * testing qty del assistanti del plater prima e dopo della usare Assistnts bonus.
	 * @throws Exception 
	 */
	@Test
	public void testUseBonus() throws Exception {
		
		game = new Model("src/main/resources/map.txt", 4);
		System.out.println(game.getPlayers().get(1).getAssistants().size());
		assistantsBonus.useBonus(game.getPlayers().get(1), game.getBoard());
		System.out.println(assistantsBonus.getQuantity());
		System.out.println(game.getPlayers().get(1).getAssistants().size());
		int befor = game.getPlayers().get(1).getAssistants().size();
		int bonus = assistantsBonus.getQuantity();
		int after = game.getPlayers().get(1).getAssistants().size() + assistantsBonus.getQuantity();
		assertEquals(after , befor + bonus);
	}

}
