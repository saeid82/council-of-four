/**
 * 
 */
package it.polimi.ingsw.cg44.server.view;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.controller.QuickActionConroller;
import it.polimi.ingsw.cg44.server.model.Model;
import it.polimi.ingsw.cg44.server.model.game.Board;
import it.polimi.ingsw.cg44.server.model.game.Player;

/**
 * @author saeid
 *
 */
public class PerformAnotherMainActionViewTest {
	private Model game;
	private Player player;
	private Board board;
	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.view.PerformAnotherMainActionView#createAction(it.polimi.ingsw.cg44.server.controller.Controller)}.
	 * @throws Exception 
	 */
	@Test
	public void testCreateAction() throws Exception {
		game = new Model("src/main/resources/map.txt", 4);
		player = game.getPlayers().get(0);
		board = game.getBoard();
		
		QuickActionConroller quickActionConroller = new QuickActionConroller(game.getPlayers());
		PerformAnotherMainActionView view = new PerformAnotherMainActionView(player, board);
		
		//view.createAction(quickActionConroller);
		quickActionConroller.update(view);
		
		view.registerObserver(quickActionConroller);
		view.unregisterObserver(quickActionConroller);
	}

}
