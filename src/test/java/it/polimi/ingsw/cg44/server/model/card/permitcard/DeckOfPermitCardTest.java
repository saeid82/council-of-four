/**
 * 
 */
package it.polimi.ingsw.cg44.server.model.card.permitcard;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.model.Model;
import it.polimi.ingsw.cg44.server.model.board.RegionType;

/**
 * @author saeid
 *
 */
public class DeckOfPermitCardTest {
	private Model game;
	
	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.card.permitcard.DeckOfPermitCard#getFsceUpPermitCard()}.
	 */
	@Test
	public void testGetFsceUpPermitCard(){
		try {
			game = new Model("src/main/resources/map.txt", 4);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertTrue(game.getBoard().getRegions(RegionType.HILL).getDeckOfPermitCard().getFsceUpPermitCard().size() == 2);
		System.out.println(game.getBoard().getCouncillorReserve());
		
		
		assertTrue(game.getBoard().getRegions(RegionType.HILL).getDeckOfPermitCard().getDeck().size() == 13);
		
		
		
		// Testing che la carta pescata sia uguale con la carta a faccia in su 
		PermitCard deckOfPermitCard = game.getBoard().getRegions(RegionType.HILL).getDeckOfPermitCard().getFsceUpPermitCard().get(0);
		assertTrue(game.getBoard().getRegions(RegionType.HILL).getDeckOfPermitCard().drawFaceUpPermitCard(0) == deckOfPermitCard);
	}




}
