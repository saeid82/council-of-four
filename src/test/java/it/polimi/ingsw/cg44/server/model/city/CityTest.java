/**
 * 
 */
package it.polimi.ingsw.cg44.server.model.city;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.model.Model;
import it.polimi.ingsw.cg44.server.model.board.RegionType;
import it.polimi.ingsw.cg44.server.model.colors.Colors;

/**
 * @author saeid
 *
 */
public class CityTest {
	private City city;
	private Model game;

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.city.City#addBorder(it.polimi.ingsw.cg44.server.model.city.City)}.
	 * @throws Exception 
	 */
	@Test
	public void testAddBorder(){

		try {
			city = new City("src/main/resources/map.txt", RegionType.HILL, Colors.BLUE);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			city.addBorder(new City("ARKON", RegionType.COAST, Colors.IRON));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertTrue("ARKON"==city.getWays().get(0).getName());
		assertTrue(RegionType.COAST == city.getWays().get(0).getRegion());
		assertTrue(Colors.IRON == city.getWays().get(0).getColor());
		
	}

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.city.City#addEmporium(it.polimi.ingsw.cg44.server.model.city.Emporium)}.
	 * @throws Exception 
	 */
	@Test
	public void testAddEmporium() {
		try {
			city = new City("src/main/resources/map.txt", RegionType.HILL, Colors.BLUE);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Model game = null;
		try {
			game = new Model("src/main/resources/map.txt", 4);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Emporium emporium = new Emporium(game.getPlayers().get(1));
		city.addEmporium(emporium);
		assertTrue(1 == city.getEmporium().size());
		
	}

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.city.City#getEmporium()}.
	 * @throws Exception 
	 */
	@Test
	public void testGetEmporium() {
		try {
			city = new City("src/main/resources/map.txt", RegionType.HILL, Colors.BLUE);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertTrue(0 == city.getEmporium().size());
	}

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.city.City#getName()}.
	 * @throws Exception 
	 */
	@Test
	public void testGetName() {
		try {
			city = new City("saeid", RegionType.HILL, Colors.BLUE);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertTrue("saeid" == city.getName());
	}

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.city.City#getWays()}.
	 * @throws Exception 
	 */
	@Test
	public void testGetWays() {
		try {
			city = new City("src/main/resources/map.txt", RegionType.HILL, Colors.BLUE);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(city.getWays().size());
		assertTrue(0 == city.getWays().size());
		try {
			city.addBorder(new City("ARKON", RegionType.COAST, Colors.IRON));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertTrue(1 == city.getWays().size());
	}

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.city.City#getRegion()}.
	 * @throws Exception 
	 */
	@Test
	public void testGetRegion() {
		try {
			city = new City("src/main/resources/map.txt", RegionType.HILL, Colors.BLUE);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertTrue(RegionType.HILL == city.getRegion());
	}

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.city.City#getBonus()}.
	 * @throws Exception 
	 */
	@Test
	public void testGetBonus() {
		try {
			game = new Model("src/main/resources/map.txt", 4);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(game.getBoard().getCities().get(0));
		System.out.println(game.getBoard().getCities().get(0).getBonus());
	}

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.city.City#getColor()}.
	 * @throws Exception 
	 */
	@Test
	public void testGetColor() {
		try {
			city = new City("src/main/resources/map.txt", RegionType.HILL, Colors.BLUE);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertTrue(Colors.BLUE == city.getColor());
	}

}
