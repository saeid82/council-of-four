/**
 * 
 */
package it.polimi.ingsw.cg44.server.controller.action.mainaction;



import org.junit.Test;

import it.polimi.ingsw.cg44.server.model.Model;

import it.polimi.ingsw.cg44.server.model.board.RegionType;

/**
 * @author saeid
 *
 */
public class SatisfyCouncillorTest {
	
	private Model game;

	@Test
	public void test() {
		try {
			game = new Model("src/main/resources/map.txt", 4);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SatisfyCouncillor satisfyCouncillor = new SatisfyCouncillor(game.getPlayers().get(0),
				game.getBoard().getRegions(RegionType.COAST), game.getPlayers().get(0).getPoliticCards());
		//aggiungiamo i coins di player per pagare
		game.getPlayers().get(0).getCoinsTrack().setCoinsNumber(10);
		
		System.out.println("players politicard before action: "+game.getPlayers().get(0).getPoliticCards());
		System.out.println("the councillors in the balcony of COAST region:\n"+game.getBoard().getRegions(RegionType.COAST).getBalcony().getBench());
		System.out.println("The coins number of player before action: "+game.getPlayers().get(0).getCoinsTrack().getCurrentCoinsNumber());
		
		satisfyCouncillor.satisfy();
		
		System.out.println("players politicard after action: "+game.getPlayers().get(0).getPoliticCards());
		System.out.println("The coins number of player after action: "+game.getPlayers().get(0).getCoinsTrack().getCurrentCoinsNumber());
	}

}
