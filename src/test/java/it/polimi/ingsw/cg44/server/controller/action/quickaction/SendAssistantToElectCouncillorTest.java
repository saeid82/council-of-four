/**
 * 
 */
package it.polimi.ingsw.cg44.server.controller.action.quickaction;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.model.Model;
import it.polimi.ingsw.cg44.server.model.balcony.Councillor;
import it.polimi.ingsw.cg44.server.model.board.RegionType;
import it.polimi.ingsw.cg44.server.model.colors.Colors;
import it.polimi.ingsw.cg44.server.model.game.Player;

/**
 * @author saeid
 *
 */
public class SendAssistantToElectCouncillorTest {
	private Model game;
	/**
	 * The player returns an assistant to the pool, takes a councillor and inserts them in
	 * a balcony, exactly as with the main action "elect a councillor". 
	 * but they do not earn any coins by performing this action.
	 * Test method for {@link it.polimi.ingsw.cg44.server.controller.action.quickaction.SendAssistantToElectCouncillor#SendAssistantToElectCouncillor(it.polimi.ingsw.cg44.server.model.game.Player, it.polimi.ingsw.cg44.server.model.game.Board, it.polimi.ingsw.cg44.server.model.board.Region, it.polimi.ingsw.cg44.server.model.colors.Colors)}.
	 * @throws Exception 
	 */
	@Test
	public void testSendAssistantToElectCouncillor() throws Exception{
		game = new Model("src/main/resources/map.txt", 4);
		Player player = game.getPlayers().get(0);
		Councillor councillorFromReserve = game.getBoard().getCouncillorReserve().getCouncillorInReserve().get(0);
		Colors colors = councillorFromReserve.getColors();
		SendAssistantToElectCouncillor sendAssistantToElectCouncillor = new SendAssistantToElectCouncillor(player, game.getBoard(),
				game.getBoard().getRegions(RegionType.HILL), colors);
		
		//numero di assistanti del player prima la azione
		int assPlyPrima = player.getAssistants().size();
		
		sendAssistantToElectCouncillor.run();
		
		//numero di assistanti del player dopo la azione
		int assPlyAfter = player.getAssistants().size();
		
		// il councillor scelta deve essere uguale con l'ultima inserita nel balcony
		assertTrue(councillorFromReserve == game.getBoard().getRegions(RegionType.HILL).getBalcony().getBench().getFirst());
		assertTrue(assPlyPrima == assPlyAfter+1);
	}

}
