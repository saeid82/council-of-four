/**
 * 
 */
package it.polimi.ingsw.cg44.server.model;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.controller.Controller;
import it.polimi.ingsw.cg44.server.controller.MainActionController;
import it.polimi.ingsw.cg44.server.controller.action.Action;
import it.polimi.ingsw.cg44.server.controller.action.mainaction.AcquirePermitCard;
import it.polimi.ingsw.cg44.server.model.board.NormalRegion;
import it.polimi.ingsw.cg44.server.model.board.RegionType;
import it.polimi.ingsw.cg44.server.model.bonus.VictoryPointBonus;
import it.polimi.ingsw.cg44.server.model.card.politiccard.PoliticCard;
import it.polimi.ingsw.cg44.server.model.game.Board;
import it.polimi.ingsw.cg44.server.model.game.Player;
import it.polimi.ingsw.cg44.server.view.AcquirePermitCardView;
import it.polimi.ingsw.cg44.server.view.View;

/**
 * @author saeid
 *
 */
public class ModelTest {
	private Model game;
	private NormalRegion hill;

	private VictoryPointBonus victoryPointBonus;
	private Controller mainActionController;
	private View acquirePermitCardView;
	private Action acquirePermitCard;
	private Board board;
	private Player player1;
	private List<PoliticCard> politicCards;
	 
//	MainActionController mainActionController = new MainActionController(game.getPlayers());
//	AcquirePermitCardView acquirePermitCardView = new AcquirePermitCardView(player1, game.getBoard().getRegions(RegionType.HILL)
//			, game.getBoard(), player1.getPoliticCards(), 1);
//	
//	IAction acquirePermitCard = acquirePermitCardView.createAction(mainActionController);


	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.Model#getBoard()}.
	 * @throws Exception 
	 */
	@Test
	public void testGetBoard() throws Exception {
		game = new Model("src/main/resources/map.txt", 4);
		assertTrue(15 == game.getBoard().getCities().size());
		game.getBoard().getKing().moving(game.getBoard().getKing().getCity().getWays().get(0));
		//System.out.print(game.getBoard().getKing().getCity().getColor());
		
		hill = game.getBoard().getRegions(RegionType.HILL);
		assertTrue(4 == hill.getBalcony().getBench().size());
		
		
		Player player1 = game.getPlayers().get(0);
		Player player2 = game.getPlayers().get(1);
		assertTrue(player1.getId() != player2.getId());
		
		
		
		assertTrue(false == game.endGame(game));
		player1.getEmporiums().remove();
		player1.getEmporiums().remove();
		player1.getEmporiums().remove();
		player1.getEmporiums().remove();
		player1.getEmporiums().remove();
		player1.getEmporiums().remove();
		player1.getEmporiums().remove();
		player1.getEmporiums().remove();
		player1.getEmporiums().remove();
		player1.getEmporiums().remove();
		assertTrue(true == game.endGame(game));
		
		
		
		board = game.getBoard();
		politicCards = game.getPlayers().get(0).getPoliticCards();
		acquirePermitCardView = new AcquirePermitCardView(player1, hill, board, politicCards, 0);
		game.registerObserver(acquirePermitCardView);
	}





	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.observer.Observable#notifyObserver()}.
	 * @throws Exception 
	 */
	@Test
	public void testNotifyObserver() throws Exception {
		game = new Model("src/main/resources/map.txt", 4);
		player1 = game.getPlayers().get(0);
		hill = game.getBoard().getRegions(RegionType.HILL);
		board = game.getBoard();
		politicCards = game.getPlayers().get(0).getPoliticCards();
		// player1 vuole fare main action (acquirePermiCard) dalla regione HILL

		
		System.out.println(politicCards.size());
		mainActionController = new MainActionController(game.getPlayers());
		
		acquirePermitCardView = new AcquirePermitCardView(player1, hill, board, politicCards, 0);
		
		acquirePermitCard = new AcquirePermitCard(board, player1, hill, politicCards, 0);
		
		//prima della lanciar azione controlliamo tutto per vedere cambiamenti.
		// vediamo le politic card e quanti sono
		System.out.println("The politic cards of player1:\n"+politicCards+"\nsize= "+politicCards.size());
		//vediamo i colori di councillor in balcone della regione HILL
		System.out.println("The councillor in balcony of HILL region:\n"+hill.getBalcony().getBench());
		
		System.out.println("the money befor: "+player1.getCoinsTrack().getCurrentCoinsNumber());

		//acquirePermitCard.run();

		// vediamo le politic card e quanti sono
		System.out.println("The politic cards of player1:\n"+politicCards+"\nsize= "+politicCards.size());
		// Money dopo la azione
		System.out.println("the money after: "+player1.getCoinsTrack().getCurrentCoinsNumber());
	}
	public Controller getMainActionController() {
		return mainActionController;
	}

	public Action getAcquirePermitCard() {
		return acquirePermitCard;
	}
}
