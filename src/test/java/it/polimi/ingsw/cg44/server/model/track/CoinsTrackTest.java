/**
 * 
 */
package it.polimi.ingsw.cg44.server.model.track;

import static org.junit.Assert.*;

import org.junit.Test;

import it.polimi.ingsw.cg44.server.model.Model;

/**
 * @author saeid
 *
 */
public class CoinsTrackTest {
	private Model game;
	private CoinsTrack coinsTrack;





	/**
	 * Test pagamento e guadagnare del player
	 * @throws Exception 
	 */
	@Test
	public void testSetCoinsNumber() throws Exception {
		Model game = new Model("src/main/resources/map.txt", 4);
		
		int currCoinsPlayer1 = game.getPlayers().get(0).getCoinsTrack().getCurrentCoinsNumber();
		int currCoinsPlayer2 = game.getPlayers().get(1).getCoinsTrack().getCurrentCoinsNumber();

		assertTrue(10 == currCoinsPlayer1);
		assertTrue(11 == currCoinsPlayer2);
		
		
		
		
		// spostare due posti più avanti (guadagnare).
		game.getPlayers().get(0).getCoinsTrack().setCoinsNumber(2);
		
		assertTrue(12 == game.getPlayers().get(0).getCoinsTrack().getCurrentCoinsNumber());
		
		// spostare 3 posti in dietro (pagamento)
		game.getPlayers().get(0).getCoinsTrack().setCoinsNumber(-3);

		assertTrue(9 == game.getPlayers().get(0).getCoinsTrack().getCurrentCoinsNumber());
		
	}

}
