/**
 * 
 */
package it.polimi.ingsw.cg44.server.model.bonus;



import org.junit.Test;


/**
 * ho fatto testing per tutti i metodi e le azioni, ma ci vogliono interazioni con client.
 * @author saeid
 *
 */
public class MainActionBonusTest {
	
	private MainActionBonus mainActionBonus = new MainActionBonus();
	/**
	 * Test method for {@link it.polimi.ingsw.cg44.server.model.bonus.MainActionBonus#useBonus(it.polimi.ingsw.cg44.server.model.game.Player, it.polimi.ingsw.cg44.server.model.game.Board)}.
	 * @throws Exception 
	 */
	@Test
	public void testUseBonus(){
		
		// non lancio useBonus perché ci vuole interazione con client
		//mainActionBonus.useBonus(game.getPlayers().get(0), game.getBoard());
		System.out.println(mainActionBonus.getClass().getName());
	}


	@Test
	public void testGetAction() {
		System.out.println(mainActionBonus.getAction());
	}

}
