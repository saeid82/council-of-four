/**
 * 
 */
package it.polimi.ingsw.cg44;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author saeid
 *
 */
public class AppTest {

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.cli.LocalApp#acquirePermitCard(it.polimi.ingsw.cg44.server.model.Model, int)}.
	 */
	@Test
	public void testAcquirePermitCard() {
		
	}

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.cli.LocalApp#hillAcquirePermirCard(it.polimi.ingsw.cg44.server.model.Model, int)}.
	 */
	@Test
	public void testHillAcquirePermirCard() {
		
	}

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.cli.LocalApp#coastAcquirePermitCard(it.polimi.ingsw.cg44.server.model.Model, int)}.
	 */
	@Test
	public void testCoastAcquirePermitCard() {
		
	}

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.cli.LocalApp#mountainAcquirePermitCard(it.polimi.ingsw.cg44.server.model.Model, int)}.
	 */
	@Test
	public void testMountainAcquirePermitCard() {
		
	}

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.cli.LocalApp#buildEmporiumUsingPermitCard(it.polimi.ingsw.cg44.server.model.Model, int)}.
	 */
	@Test
	public void testBuildEmporiumUsingPermitCard() {
		
	}

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.cli.LocalApp#buildEmporiumWithKingHelp(it.polimi.ingsw.cg44.server.model.Model, int)}.
	 */
	@Test
	public void testBuildEmporiumWithKingHelp() {
		
	}

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.cli.LocalApp#electCouncillor(it.polimi.ingsw.cg44.server.model.Model, int)}.
	 */
	@Test
	public void testElectCouncillor() {
		
	}

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.cli.LocalApp#changeFaceUpPermitCard(it.polimi.ingsw.cg44.server.model.Model, int)}.
	 */
	@Test
	public void testChangeFaceUpPermitCard() {
		
	}

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.cli.LocalApp#engageAssistant(it.polimi.ingsw.cg44.server.model.Model, int)}.
	 */
	@Test
	public void testEngageAssistant() {
		
	}

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.cli.LocalApp#performAdditionalMainAction(it.polimi.ingsw.cg44.server.model.Model, int)}.
	 */
	@Test
	public void testPerformAdditionalMainAction() {
		
	}

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.cli.LocalApp#sendAssistantToElectCouncillor(it.polimi.ingsw.cg44.server.model.Model, int)}.
	 */
	@Test
	public void testSendAssistantToElectCouncillor() {
		
	}

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.cli.LocalApp#selectRegion()}.
	 */
	@Test
	public void testSelectRegion() {

		
		
	}

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.cli.LocalApp#selectNameRegion()}.
	 */
	@Test
	public void testSelectNameRegion() {
		
	}

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.cli.LocalApp#selectColor()}.
	 */
	@Test
	public void testSelectColor() {
		
	}

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.cli.LocalApp#selectPermitcard(it.polimi.ingsw.cg44.server.model.Model, it.polimi.ingsw.cg44.server.model.board.RegionType)}.
	 */
	@Test
	public void testSelectPermitcard() {
		
	}

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.cli.LocalApp#selectActionNumber()}.
	 */
	@Test
	public void testSelectMainAction() {
		
	}

	/**
	 * Test method for {@link it.polimi.ingsw.cg44.cli.LocalApp#selectPermitcardFromPly(it.polimi.ingsw.cg44.server.model.Model, int)}.
	 */
	@Test
	public void testSelectPermitcardFromPly() {
		
	}

}
