package it.polimi.ingsw.cg44.server.model.colors;

/**
 * @author saeid
 *
 */
public enum Colors {
	
	
	/**
	 * Councillors and politic card colors
	 */
	BLACK(true),
	WHITE(true),
	BLUE(true),
	PINK(true),
	PURPLE(true),
	ORANGE(true),
	JOKER(true),
	
	
	/**
	 * Cites colors
	 */
	IRON(false), 
	SILVER(false), 
	BRONZE(false), 
	GOLD(false), 
	KING(false)
	;
	
	boolean politicCardColor;

	/**
	 * @param politicCardColor
	 */
	private Colors(boolean politicCardColor) {
		this.politicCardColor = politicCardColor;
	}

	public boolean isPoliticCardColor() {
		return politicCardColor;
	}
	
	

}
