package it.polimi.ingsw.cg44.server.model.bonus.factory;

import it.polimi.ingsw.cg44.server.model.game.Board;
import it.polimi.ingsw.cg44.server.model.game.Player;

public interface IBonus {
	
	public abstract void useBonus(Player player, Board board) throws Exception;

}
