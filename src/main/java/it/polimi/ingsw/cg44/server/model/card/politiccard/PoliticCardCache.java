package it.polimi.ingsw.cg44.server.model.card.politiccard;


import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import it.polimi.ingsw.cg44.server.model.colors.Colors;


/**
 * Prototype pattern to creating duplicate object
 * @author saeid
 *
 */
public class PoliticCardCache {

	private Map<Integer, PoliticCard> politicCards = new HashMap<>();
	private static final int BOUND_COLOR = 7;


	public PoliticCard getPoliticCard(){
		if(politicCards.isEmpty()) 
			loadCache();
		Random random = new Random();
		PoliticCard cachedPermitCard = politicCards.get(random.nextInt(BOUND_COLOR));
		return new PoliticCard(cachedPermitCard);
	}
	

	/**
	 * Metodo per inizializzare la mappa, lo chiamo solo la prima volta che chiedo una carta politica 
	 */
	private void loadCache() {
		politicCards.put(0, new PoliticCard(Colors.BLACK));
		politicCards.put(1, new PoliticCard(Colors.BLUE));
		politicCards.put(2, new PoliticCard(Colors.ORANGE));
		politicCards.put(3, new PoliticCard(Colors.PINK));
		politicCards.put(4, new PoliticCard(Colors.PURPLE));
		politicCards.put(5, new PoliticCard(Colors.WHITE));
		politicCards.put(6, new PoliticCard(Colors.JOKER));
	}
}