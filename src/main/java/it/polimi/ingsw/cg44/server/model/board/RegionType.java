package it.polimi.ingsw.cg44.server.model.board;

/**
 * Regions types are three
 * @author saeid
 *
 */
public enum RegionType {
	
	HILL, COAST, MOUNTAIN;
}
