package it.polimi.ingsw.cg44.server.view;

import it.polimi.ingsw.cg44.server.controller.Controller;
import it.polimi.ingsw.cg44.server.controller.MainActionController;
import it.polimi.ingsw.cg44.server.controller.action.IAction;
import it.polimi.ingsw.cg44.server.model.card.permitcard.PermitCard;
import it.polimi.ingsw.cg44.server.model.city.City;
import it.polimi.ingsw.cg44.server.model.city.Emporium;
import it.polimi.ingsw.cg44.server.model.game.Board;
import it.polimi.ingsw.cg44.server.model.game.Player;

public class BuildEmporiumUsingPermitCardView extends View {
	
	private City city;
	private PermitCard selectedPermitCard;
	
	public BuildEmporiumUsingPermitCardView(Player player, City city, Board board, PermitCard selectedPermitCard) {
	super(player, board);
	this.city = city;
	this.selectedPermitCard = selectedPermitCard;
}


	@Override
	public IAction createAction(Controller mainActionController) {
		if(isValid())
			return ((MainActionController) mainActionController).update(this);
		else
		return null;
	}
	
	public City getCity() {
		return city;
	}
	
	public PermitCard getSelectedPermitCard() {
		return selectedPermitCard;
	}
	
	@Override
	protected boolean isValid() {
		
		boolean control = false;
		
		//Controllo che la tessera permesso sia del giocatore
		for(PermitCard permitCard : getPlayer().getPermitCards()) {
			if(permitCard.equals(this.selectedPermitCard))
				control = true;
		}	
		
		if(!control) {
			System.out.println("The selected permit card is not your.");
			return false;
		}
			
		//Controllo che la tessera permesso non sia stata usata
		if(selectedPermitCard.isUsed())
			return false;
		
		control = false;
		
		//Controllo se nella carta permesso c'è la città richiesta
		for(City city : selectedPermitCard.getCities()) {
			if(city.equals(this.city))
				control = true;
		}
		
		if(!control) {
			System.out.println("The selected city is not present in the selected permit card.");
			return false;
		}
		
		//Controllo se nella città scelta ho già un emporio del giocatore
		for(City city : getPlayer().getCities()) {
			if(city.equals(this.city)) {
				System.out.println("Hai già costruito un emporio in questa città, non puoi costruirne due.");
				return false;
			}
		}
		
		//Controllo se ha gli assistenti per pagare
		int assistantsToPay = 0;
		for(Emporium emporium : city.getEmporium()) {
			if(emporium.getPlayerId() != getPlayer().getId()) 
				assistantsToPay++;
		}
		if(getPlayer().getAssistants().size() < assistantsToPay) {
			System.out.println("You don't have enough assistants to build an emporium in the selected city.");
			return false;
		}
			
		
		return true;
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}
}
