package it.polimi.ingsw.cg44.server.model.balcony;

import it.polimi.ingsw.cg44.server.model.colors.Colors;

/**
 * Councillor class with just color attribute.
 * @author saeid
 *
 */
public class Councillor {
	
	private Colors colors;

	public Councillor(Colors colors) {
		super();
		this.colors = colors;
	}

	public Colors getColors() {
		return colors;
	}
	
	@Override
	public String toString() {
		return "Councillor --> " + this.getColors() + "\n";
	}

}
