package it.polimi.ingsw.cg44.server.controller;

import java.util.List;

import it.polimi.ingsw.cg44.server.controller.action.IAction;
import it.polimi.ingsw.cg44.server.controller.action.mainaction.AcquirePermitCard;
import it.polimi.ingsw.cg44.server.controller.action.mainaction.BuildEmporiumUsingPermitCard;
import it.polimi.ingsw.cg44.server.controller.action.mainaction.BuildEmporiumWithKingHelp;
import it.polimi.ingsw.cg44.server.controller.action.mainaction.ElectCouncillor;
import it.polimi.ingsw.cg44.server.model.game.Player;
import it.polimi.ingsw.cg44.server.view.AcquirePermitCardView;
import it.polimi.ingsw.cg44.server.view.BuildEmporiumUsingPermitCardView;
import it.polimi.ingsw.cg44.server.view.BuildEmporiumWithKingHelpView;
import it.polimi.ingsw.cg44.server.view.ElectCouncillorView;

/**
 * MVC pattern
 * @author saeid
 *
 */
public class MainActionController extends Controller {

	public MainActionController(List<Player> players) {
		super(players);
		
	}
	
	public IAction update(BuildEmporiumWithKingHelpView b) {
		return new BuildEmporiumWithKingHelp(b.getPlayer(), b.getBoard(), b.getCity());
	}
	
	public IAction update(BuildEmporiumUsingPermitCardView b) {
		return new BuildEmporiumUsingPermitCard(b.getPlayer(), b.getBoard(), 
				b.getCity(), b.getSelectedPermitCard());
	}
	
	public IAction update(AcquirePermitCardView a) {
		return new AcquirePermitCard(a.getBoard(), a.getPlayer(), 
				a.getTargetRegion(), a.getPoliticCards(), a.getCardToDrawPosition());
	}	
	
	public IAction update(ElectCouncillorView e) {
		return new ElectCouncillor(e.getPlayer(), e.getBoard(), e.getTargetRegion(), e.getColore());
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}


}
