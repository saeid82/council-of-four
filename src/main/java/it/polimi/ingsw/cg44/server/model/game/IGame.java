package it.polimi.ingsw.cg44.server.model.game;


import java.util.List;

import it.polimi.ingsw.cg44.server.model.assistant.AssistantsReserve;
import it.polimi.ingsw.cg44.server.model.track.NobilityTrack;



/**
 * Factory pattern
 * @author saeid
 *
 */
public interface IGame {

	public Board createBoard(String file, int numberPlayers) throws Exception;
	
	public List<Player> createPlayers(NobilityTrack nobilityTrack, AssistantsReserve assistantsReserve, 
			int numberPlayers) throws Exception;
}

