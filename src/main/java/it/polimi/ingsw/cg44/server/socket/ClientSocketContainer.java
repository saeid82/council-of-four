package it.polimi.ingsw.cg44.server.socket;

import java.io.Closeable;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;

/**
 * contains the socket of client and socket communicator
 * @author saeid
 *
 */
public class ClientSocketContainer implements Closeable{

	Socket socket;
	
	
	SocketCommunicator socketCommunicator;
    /**
     * Client insertion command timeout in milliseconds (30 s)
     */
//    public static final int CLIENT_ACTION_TIMEOUT = 30*1000;
	
	/**
	 * @param socket
	 */
	public ClientSocketContainer(Socket socket) {
		super();
		this.socket = socket;
//------------------------------------------------------------------------------
//		try {
//			this.socket.setSoTimeout(CLIENT_ACTION_TIMEOUT);
//		} catch (SocketException e) {
//			e.printStackTrace();
//		}
//-------------------------------------------------------------------------------
		socketCommunicator = new SocketCommunicator(socket);
	}
	
	public Socket getSocket() {
		return socket;
	}

	public SocketCommunicator getSocketCommunicator() {
		return socketCommunicator;
	}

	@Override
	public void close() throws IOException {
		if(socket != null)
			socket.close();
	}

}
