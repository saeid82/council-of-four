package it.polimi.ingsw.cg44.server.view;

import it.polimi.ingsw.cg44.server.controller.Controller;
import it.polimi.ingsw.cg44.server.controller.QuickActionConroller;
import it.polimi.ingsw.cg44.server.controller.action.IAction;
import it.polimi.ingsw.cg44.server.model.board.Region;
import it.polimi.ingsw.cg44.server.model.colors.Colors;
import it.polimi.ingsw.cg44.server.model.game.Board;
import it.polimi.ingsw.cg44.server.model.game.Player;

/**
 * @author saeid
 *
 */
public class SendAssistantToElectCouncillorView extends View {

	private Region targetRegion;
	private Colors colore;
	
	public SendAssistantToElectCouncillorView(Player player, Region targetRegion, Board board, Colors colore) {
		super(player,board);
		this.targetRegion = targetRegion;
		this.colore = colore;
	}
	
	public Region getTargetRegion() {
		return targetRegion;
	}

	public Colors getColore() {
		return colore;
	}

	@Override
	public IAction createAction(Controller quickActionController) {
		if(isValid())
			return ((QuickActionConroller) quickActionController).update(this);
		else
			return null;
	}
	
	@Override
	protected boolean isValid() {
		
		//Caso in cui la riserva è vuota
		if(getBoard().getCouncillorReserve().getCouncillorInReserve().isEmpty()) {
			System.out.println("Councillor reserve is empty, you can't elect a councillor.");
			return false;
		}
		
		if(getPlayer().getAssistants().isEmpty()) {
			System.out.println("You don't have any assistant to doing this action!");
			return false;
		}
		
		if(!getBoard().getCouncillorReserve().isContainsCouncillor(colore)) {
			System.out.println("Non c'è il consigliere di quel colore nella riserva. Cambia colore");
			return false;
		}
		else
			return true;
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}


}
