package it.polimi.ingsw.cg44.server.controller.action.quickaction;

import it.polimi.ingsw.cg44.server.controller.action.Action;
import it.polimi.ingsw.cg44.server.model.board.NormalRegion;
import it.polimi.ingsw.cg44.server.model.game.Board;
import it.polimi.ingsw.cg44.server.model.game.Player;

/**
 * Quick Action 2:
 * the player return an assistant to the pool, then take the 2 permit card face up in a 
 * region, return them to the bottom of the deck and draws 2 new ones from the top of the deck.
 * @author saeid
 *
 */
public class ChangeFaceUpPermitCard extends Action {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7623495846606942975L;
	private transient Player player;
	private Board board;
	private transient NormalRegion region;
	
	public ChangeFaceUpPermitCard(Player player, Board board, NormalRegion region) {
		this.player = player;
		this.board = board;
		this.region = region;
	}

	@Override
	public void run() {
		region.getDeckOfPermitCard().changeFaceUpCard();
		board.getAssistantsReserve().getAssistants().add(player.getAssistants().poll());
		
	}


}
