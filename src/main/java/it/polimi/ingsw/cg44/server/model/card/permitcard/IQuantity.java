package it.polimi.ingsw.cg44.server.model.card.permitcard;

import java.util.Random;

/**
 * Interface for select the quantity of objects
 * @author saeid
 *
 */
public interface IQuantity {
	//serve per limitare la generazione di numeri casuali nel metodo nextInt della classe random
	public static final int MAX_NUMBER = 100;
	
	public default int chooseQuantity() {
		Random random = new Random();
		int bonusQuantity = random.nextInt(MAX_NUMBER);
	
		if(bonusQuantity<40)
			return 1;
		if(bonusQuantity>=40 && bonusQuantity<75)
			return 2;
		else
			return 3;
	}

}
