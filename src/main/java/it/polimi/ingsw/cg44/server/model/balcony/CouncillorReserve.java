package it.polimi.ingsw.cg44.server.model.balcony;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import it.polimi.ingsw.cg44.server.model.colors.Colors;

public class CouncillorReserve {
	
	/**
	 * Numbers of Councillors for each color are four.
	 */
	public static final int NUM_OF_COUNIL_FOREACH_COLOR = 4;

	private final List<Councillor> councInReserve;

	/**
	 * Constructor to create 24 councillor (4x6 colors) and mix them.
	 */
	public CouncillorReserve() {
		super();
		this.councInReserve = new LinkedList<>();
		
		for(int i=0 ; i < NUM_OF_COUNIL_FOREACH_COLOR ; i++){
			
			councInReserve.add(new Councillor(Colors.BLACK));
			councInReserve.add(new Councillor(Colors.WHITE));
			councInReserve.add(new Councillor(Colors.BLUE));
			councInReserve.add(new Councillor(Colors.ORANGE));
			councInReserve.add(new Councillor(Colors.PINK));
			councInReserve.add(new Councillor(Colors.PURPLE));
			
			Collections.shuffle(councInReserve);
			
		}
	}

	public List<Councillor> getCouncillorInReserve() {
		return councInReserve;
	}
	
	
	
	/**
	 * The method that checks if there is a counselor in the reserves
	 * @param colore
	 * @return boolean
	 */
	public boolean isContainsCouncillor(Colors colore) {
		for(Councillor councillor : councInReserve)
			if(councillor.getColors() == colore)
				return true;
		throw new NullPointerException();
	}

	/**
	 * @param colors
	 * @return councillor with color chosen.
	 */
	public Councillor getCouncillorByColor(Colors colors){
		for (Councillor councillor : councInReserve) {
			if(councillor.getColors() == colors){
				int idx = councInReserve.indexOf(councillor);
				councInReserve.remove(idx);
				return councillor;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return " CouncillorReserve{\n" + councInReserve + "\t\n}";
	}
	
	
	

}
