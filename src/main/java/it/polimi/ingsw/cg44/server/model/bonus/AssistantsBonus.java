package it.polimi.ingsw.cg44.server.model.bonus;

import it.polimi.ingsw.cg44.server.model.game.Board;
import it.polimi.ingsw.cg44.server.model.game.Player;

/**
 * @author saeid
 *
 */
public class AssistantsBonus extends Bonus {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6619368480429693298L;
	private final int quantity;
	private static final int MAX_QUANTITY_ASSISTANTS = 5;
	
	public AssistantsBonus() {
		this.quantity=(int) (Math.random()*MAX_QUANTITY_ASSISTANTS + 1);
		//ho aggiunto + 1 nella funzione randomica per evitare che si generino bonus con quantità nulla
	}

	public int getQuantity() {
		return quantity;
	}
	
	@Override
	public void useBonus(Player player, Board board){
		if(board.getAssistantsReserve().getAssistants().isEmpty()) {
			throw new NullPointerException("The reserve is empty!");
		}
		for(int i = 0; i< quantity; i++)
			player.getAssistants().add(board.getAssistantsReserve().getAssistants().poll());
		
	}

	@Override
	public String toString() {
		return "AssistantsBonus(quantity= " + quantity + ")";
	}

}
