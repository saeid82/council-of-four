package it.polimi.ingsw.cg44.server.model.bonus;

import it.polimi.ingsw.cg44.server.model.card.politiccard.PoliticCardCache;
import it.polimi.ingsw.cg44.server.model.game.Board;
import it.polimi.ingsw.cg44.server.model.game.Player;

/**
 * Bonus to draw a politic card from the deck. 
 * there is just in the nobility track.
 * @author saeid
 *
 */
public class PoliticCardBonus extends Bonus {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -8410782771806668601L;

	@Override
	public void useBonus(Player player, Board board) throws Exception {
		PoliticCardCache politicCardPrototype = new PoliticCardCache();
		player.getPoliticCards().add(politicCardPrototype.getPoliticCard());	
	}

	@Override
	public String toString() {
		return "(PoliticCardBonus)";
	}

}
