package it.polimi.ingsw.cg44.server.view;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.cg44.server.controller.Controller;
import it.polimi.ingsw.cg44.server.controller.MainActionController;
import it.polimi.ingsw.cg44.server.controller.action.IAction;
import it.polimi.ingsw.cg44.server.model.balcony.Councillor;
import it.polimi.ingsw.cg44.server.model.board.NormalRegion;
import it.polimi.ingsw.cg44.server.model.card.politiccard.PoliticCard;
import it.polimi.ingsw.cg44.server.model.colors.Colors;
import it.polimi.ingsw.cg44.server.model.game.Board;
import it.polimi.ingsw.cg44.server.model.game.Player;


/**
 * MVC pattern
 * @author saeid
 *
 */
public class AcquirePermitCardView extends View{
	
	private NormalRegion targetRegion;
	private List<PoliticCard> politicCards;
	private int positionOfCardToDraw;
	
	public AcquirePermitCardView(Player player, NormalRegion targetRegion, Board board, List<PoliticCard> politicCards, int positionOfCardToDraw) {
		super(player, board);
		this.targetRegion = targetRegion;
		this.politicCards = politicCards;
		this.positionOfCardToDraw = positionOfCardToDraw;	
	}



	@Override
	public IAction createAction(Controller mainActionController) {
		try {
			if(isValid())
				try {
					return ((MainActionController)mainActionController).update(this);
				} catch (Exception e) {
					System.out.print(e.getMessage());
					return null;
				}
				
			else
				return null;
		} catch (Exception e) {
			System.out.print(e.getMessage());
		}
		return null;
	}
	
	
	public NormalRegion getTargetRegion() {
		return targetRegion;
	}
	
	public List<PoliticCard> getPoliticCards() {
		return politicCards;
	}

	public int getCardToDrawPosition() {
		return positionOfCardToDraw;
	}


	@Override
	protected boolean isValid() throws Exception {
		int jokerCounter = 0;
		List<PoliticCard> politicCardsMatched = new ArrayList<>();
		// creat a copy of balcony to doing the operation
		
		ArrayDeque<Councillor> copyBench = new ArrayDeque<>();
		int benchSize = targetRegion.getBalcony().getBench().size();
		for(int i=0 ; i < benchSize ; i++)
			copyBench.add(targetRegion.getBalcony().getBench().poll());
		
		//a questp punto targetRegion.getBalcony().getBench() è vuoto
		
		// creo un bench temporaneo che è una copia di targetRegion.getBalcony().getBench()
		ArrayDeque<Councillor> tmpBench = new ArrayDeque<Councillor>();
		tmpBench = copyBench.clone();
		
		// riempo targetRegion.getBalcony().getBench() ancora con gli stessi consiglieri
		int copySize = copyBench.size();
		for(int i=0 ; i<copySize ; i++)
			targetRegion.getBalcony().getBench().push(tmpBench.poll());
		
		
		

		//Contiamo i JOKER e verifichiamo che le carte politiche giocate abbiano una corrispondenza con i consiglieri del balcone scelto
		for(PoliticCard politicCard : politicCards) {
			if(politicCard.getCardColor() == Colors.JOKER){
				jokerCounter++;
			}
			for(Councillor councillor : copyBench) {
				if(politicCard.getCardColor().equals(councillor.getColors()) || politicCard.getCardColor().equals(Colors.JOKER)) {
					politicCardsMatched.add(new PoliticCard(politicCard));
					copyBench.remove(councillor);
					break;
				}
			}
		}
		//Controlliamo se il numero di carte politiche è giusto e se in base al numero il giocatore ha abbastanza soldi
		try {
			if(politicCardsMatched.isEmpty()){
				System.out.println("You don't have enough politic cards matched with councillor colors to buy this permitCard.");
				return false;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		if(politicCardsMatched.size()==1 && getPlayer().getCoinsTrack().getCurrentCoinsNumber() < (10 + jokerCounter)) {				
				throw new Exception("You don't have enough money!\n");		
		}
		
		if(politicCardsMatched.size()==2 && getPlayer().getCoinsTrack().getCurrentCoinsNumber() < (7 + jokerCounter)) {
			throw new Exception("You don't have enough money!\n");
		}
		
		if(politicCardsMatched.size()==3 && getPlayer().getCoinsTrack().getCurrentCoinsNumber() < (4 + jokerCounter)) {
			throw new Exception("You don't have enough money!\n");
		}
		
		if(politicCardsMatched.size()==4 && getPlayer().getCoinsTrack().getCurrentCoinsNumber() < jokerCounter) {
			throw new Exception("You don't have enough money!\n");
		}
		
		if(politicCardsMatched.size()>4) {
			System.out.println("\nYou can choose at max 4 politc cards.!\n");
			System.out.println("\nIn this case is considered only 4 politicCard!\n\n");
			return true;
		}
		
		//Verifico che la carta permesso scelta sia una delle due scoperte (posizione 0 e 1 di faceupCard)
		if(positionOfCardToDraw != 0 && positionOfCardToDraw != 1)
			return false;
		
		return true;
		
	}



	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}

}
