package it.polimi.ingsw.cg44.server.controller.action.mainaction;

import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.cg44.server.controller.action.Action;
import it.polimi.ingsw.cg44.server.model.board.NormalRegion;
import it.polimi.ingsw.cg44.server.model.bonus.BonusKingCard;
import it.polimi.ingsw.cg44.server.model.card.permitcard.PermitCard;
import it.polimi.ingsw.cg44.server.model.city.City;
import it.polimi.ingsw.cg44.server.model.city.Emporium;
import it.polimi.ingsw.cg44.server.model.colors.Colors;
import it.polimi.ingsw.cg44.server.model.game.Board;
import it.polimi.ingsw.cg44.server.model.game.Player;

/**
 * Action 3:
 * The player chooses on of the permit card face up in front of them,they place an emporium on the 
 * corresponding space in the city whose initial is indicated on the card. then turn the used card
 * face down and can't use it anymore to build another emporium.
 * @author saeid
 *
 */

public class BuildEmporiumUsingPermitCard extends Action {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4668387875297987747L;
	private transient Player player;
	private Board board;
	private transient City city;
	private transient PermitCard selectedPermitCard;
	

	/**
	 * @param player
	 * @param board
	 * @param city
	 * @param selectedPermitCard
	 */
	public BuildEmporiumUsingPermitCard(Player player, Board board, City city, PermitCard selectedPermitCard) {
		this.player = player;
		this.board = board;
		this.city = city;
		this.selectedPermitCard = selectedPermitCard;
	}
	


	/* 
	 * Rules for building an emporium:
	 * 1- Each player can build one emporium only in each city.
	 * 2- Each time they build an emporium in a city must return 1 assistant to the pool for each
	 * 	  emporium already built by the other players in the same city,
	 * 	  if the city is empty, they do not need to return anything.
	 * 3- Each time a player build an emporium in a city, they obtain the bonus indicated by that city's token
	 *    in additional if the city is connected directly to another city ....
	 * @see it.polimi.ingsw.cg44.model.action.Action#execute()
	 */

	@Override
	public void run() {
		// Calculate the emporiums of the others players and pay 1 assistant for each emporium built
		int assistantsToPay = 0;
		for(Emporium emporium : city.getEmporium()) {
			if(emporium.getPlayerId() != player.getId()) 
				assistantsToPay++;
		}
		for(int i=0; i<assistantsToPay; i++)
			player.getAssistants().poll();
		
		//turn the used permit card face down and can't use it anymore to build another emporium.
		try {
			selectedPermitCard.useCard();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//place the emporium on the corresponding space in the city
		city.getEmporium().add(player.getEmporiums().poll());
		
		// Add the city to the list of the player's cities
		player.getCities().add(city);
		
		// Obtain the bonus of all the connected cities if ...
		try {
			obtainBonusOfConnectedCities();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		try {
			checkBonusRegion();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			checkBonusCityColor();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	/**
	 * Obtain the bonus of all the connected cities if the city in witch a player builds an emporium
	 * is connected directly to another city or to other cities or to a group of already connected
	 * citeies, in witch one of their emporiums is already present.
	 * @throws Exception 
	 */
	public void obtainBonusOfConnectedCities() throws Exception{
		List<City> controlledCities = new ArrayList<>();
		controlledCities.add(this.city);
		
		//Creat the list of the connected cities
		for(int i=0; i<controlledCities.size(); i++) {
			City currentCity = controlledCities.get(i);
			for(City borderCity : currentCity.getWays()){
				for(Emporium emporium : borderCity.getEmporium())
					if(emporium.getPlayerId() == player.getId() && !controlledCities.contains(borderCity))
						controlledCities.add(borderCity);
			}
		}
		//Obtain the bonus of all the connected cities(controlledCities)
		for(City currentCity : controlledCities)
			currentCity.getBonus().stream().forEach(
					b -> {
						try{
							try {
								b.useBonus(player, board);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} catch(IllegalArgumentException e){
							throw new RuntimeException(e);
						}
					}
			
			);

	}


	/**
	 * If the player have built an emporium in each city of the specific region,
	 * can obtain the corresponding bonus card
	 * can obtain the king's card bonus from the deck.
	 * @throws Exception 
	 */
	public void checkBonusRegion() throws Exception  {
		NormalRegion selectedRegion = board.getRegions(this.city.getRegion());
		int citiesNumber = selectedRegion.getCities().size();
		for(City city : selectedRegion.getCities()) {
			for(Emporium emporium : city.getEmporium()) {
				if(emporium.getPlayerId() == this.player.getId())
					citiesNumber--;
			}
		}

		if(citiesNumber == 0) {
			System.out.println("You have built an emporium in each city of the region:  " + selectedRegion.getRegionType());
			
			try{
				BonusKingCard cardToDraw = selectedRegion.getBonusRegionCard();
				System.out.println("You have drawn bonus card " + cardToDraw);
				cardToDraw.useBonus(player, board);
			}catch(NullPointerException ex){
				System.out.println("Other players have may already drawn of the Bonus Card of the region " + selectedRegion.getRegionType());
			}
			
			try{
				BonusKingCard cardToDraw = board.getKingRegion().drawKingCard();
				System.out.println("You have drawn the King's bonus card from the deck" + cardToDraw);
				cardToDraw.useBonus(player, board);
			}catch(NullPointerException ex){
				System.out.println("Sorry, the King's bonus cards are finished!!");
			}
		}
	}
	

	/**
	 * When a player possesses an emporium in all cities of one color they receive 
	 * the corresponding bonus card.
	 * @throws Exception 
	 */
	public void checkBonusCityColor() throws Exception {
		Colors color = this.city.getColor();
		int countCity = 0;
		for(City currentCity : board.getCities()) {
			if(currentCity.getColor() == color) {
				countCity++;
				for(Emporium emporium : currentCity.getEmporium()) {
					if(emporium.getPlayerId() == this.player.getId())
						countCity--;
				}
			}
		}
		if(countCity == 0) {
			System.out.println("You possesses an emporium in all cities of color " + color);
			try{
				BonusKingCard cardToDraw = board.getKingRegion().getDeckOfCityColorBonus(color);
				System.out.println("You have drawn bonus card: " + cardToDraw);
				cardToDraw.useBonus(player, board);
			}catch(NullPointerException e){
				System.out.println("Sorry! The bonus card correspondinf in all cities of one color " + color + " has already been drawn.");
			}
			try{
				BonusKingCard cardToDraw = board.getKingRegion().drawKingCard();
				System.out.println("You have drawn the King's bonus card from the deck " + cardToDraw);
				cardToDraw.useBonus(player, board);
			}catch(NullPointerException e){
				System.out.println("Sorry, the King's bonus cards are finished!!");
			}
		}
	}
}
