package it.polimi.ingsw.cg44.server.model.card.politiccard;

import it.polimi.ingsw.cg44.server.model.colors.Colors;

/**
 * Prototype pattern to creating duplicate object
 * @author saeid
 *
 */
public class PoliticCard  implements Cloneable{
	
	private Colors cardColor;
	
	protected PoliticCard(Colors color) {
		this.cardColor = color;
	}
	
	public PoliticCard(PoliticCard politicCard) {
		this(politicCard.cardColor);
	}

	public Colors getCardColor() {
		return cardColor;
	}

	@Override
	public String toString() {
		return "" + cardColor + "";
	}

}
