package it.polimi.ingsw.cg44.server.view;

import it.polimi.ingsw.cg44.server.controller.Controller;
import it.polimi.ingsw.cg44.server.controller.QuickActionConroller;
import it.polimi.ingsw.cg44.server.controller.action.IAction;
import it.polimi.ingsw.cg44.server.model.game.Board;
import it.polimi.ingsw.cg44.server.model.game.Player;


public class EngageAssistantView extends View {
	
	public EngageAssistantView(Player player, Board board) {
		super(player, board);
	}

	@Override
	public IAction createAction(Controller quickActionController) {
		if(isValid())
			return ((QuickActionConroller) quickActionController).update(this);
		else {
			return null;
		}
	}
	
	@Override
	protected boolean isValid(){
		if(getPlayer().getCoinsTrack().getCurrentCoinsNumber() < 3){
			System.out.println("You need at least 3 coins to execute this action!");
			return false;
		}
		if(getBoard().getAssistantsReserve().getAssistants().isEmpty()){
			System.out.println("there aren't enough assistant in reserve to execute the action!");
			return false;
		}
		else return true;
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}


}
