package it.polimi.ingsw.cg44.server.model.game;



import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import it.polimi.ingsw.cg44.server.model.assistant.Assistant;
import it.polimi.ingsw.cg44.server.model.assistant.AssistantsReserve;
import it.polimi.ingsw.cg44.server.model.card.permitcard.PermitCard;
import it.polimi.ingsw.cg44.server.model.card.politiccard.PoliticCard;
import it.polimi.ingsw.cg44.server.model.card.politiccard.PoliticCardCache;
import it.polimi.ingsw.cg44.server.model.city.City;
import it.polimi.ingsw.cg44.server.model.city.Emporium;
import it.polimi.ingsw.cg44.server.model.track.CoinsTrack;
import it.polimi.ingsw.cg44.server.model.track.MarkerDisk;
import it.polimi.ingsw.cg44.server.model.track.NobilityTrack;
import it.polimi.ingsw.cg44.server.model.track.VictoryTrack;



/**
 * @author saeid
 *
 */
public class Player {


	private static int counterPlayers = 0;
	private int id;

	private final List<PoliticCard> politicCards;
	private final MarkerDisk markerDisk;
	private final CoinsTrack coinsTrack;
	private final VictoryTrack victoryTrack;
	private final Queue<Emporium> emporiums;
	private final Queue<Assistant> assistants;
	private final List<PermitCard> boughtPermitCards; // permitcard bought player
	private final List<City> citiesBuiltEmporium; // Cities where the player built emporium
		public Player(NobilityTrack nobilityTrack, AssistantsReserve assistantsReserve, int numberPlayers) throws Exception {
		this.id = ++counterPlayers;
		this.politicCards = initializePolitcCard();
		this.markerDisk = new MarkerDisk(nobilityTrack);
		this.coinsTrack = new CoinsTrack(id, numberPlayers);
		this.victoryTrack = new VictoryTrack();
		this.emporiums = initializeEmporiums();
		this.assistants = initializeAssistants(assistantsReserve);
		this.boughtPermitCards = new ArrayList<>();
		this.citiesBuiltEmporium = new ArrayList<>();
	}
	
	public int getId() {
		return id;
	}

	public List<PermitCard> getPermitCards() {
		return boughtPermitCards;
	}

	public List<City> getCities() {
		return citiesBuiltEmporium;
	}

	public List<PoliticCard> getPoliticCards() {
		return politicCards;
	}

	public MarkerDisk getMarkerDisk() {
		return markerDisk;
	}

	public CoinsTrack getCoinsTrack() {
		return coinsTrack;
	}

	public VictoryTrack getVictoryTrack() {
		return victoryTrack;
	}

	public Queue<Emporium> getEmporiums() {
		return emporiums;
	}
	
	//Ritorna il numero di empori ancora in mano al giocatore
	public int getResidualEmporium(){
		return emporiums.size();
	}

	public Queue<Assistant> getAssistants() {
		return assistants;
	}
	
	

	
	/**
	 * Crea le carte politiche da assegnare all'inizio al giocatore
	 * @return
	 * @throws Exception
	 */
	private List<PoliticCard> initializePolitcCard() throws Exception {
		final int INITIAL_POLITIC_CARDS = 6;
		PoliticCardCache politicCardPrototype = new PoliticCardCache();
		List<PoliticCard> politicCardsPlayer = new ArrayList<>();
		for(int i=0; i<INITIAL_POLITIC_CARDS; i++) {
			politicCardsPlayer.add(politicCardPrototype.getPoliticCard());
		}
		return politicCardsPlayer;
	}
	
	
	/**
	 * Crea gli empori da assegnare al giocatore ad inizio partita
	 * @return
	 */
	private Queue<Emporium> initializeEmporiums() {
		final int INITIAL_EMPORIUM = 10;
		Queue<Emporium> emporiumsPlayer = new LinkedList<>();
		for(int i=0; i<INITIAL_EMPORIUM; i++) {
			emporiumsPlayer.add(new Emporium(this));
		}
		return emporiumsPlayer;
	}
	
	private Queue<Assistant> initializeAssistants(AssistantsReserve assistantsReserve){
		Queue<Assistant> playerAssistants = new LinkedList<>();
		
		for(int i = 0; i < this.id; i++){
			playerAssistants.add(assistantsReserve.getAssistants().poll());
		}
		
		return playerAssistants;
	}

	@Override
	public String toString() {
		return "Player{\n\tId = " + id + "\n\tPoliticCards = " + politicCards + "\n\tMarkerDisk at space n = " + markerDisk.getCurrentSpace().getSpaceNumber() + "\n\tCoinsTrack = "
				+ coinsTrack.getCurrentCoinsNumber() + "\n\tVictoryTrack = " + victoryTrack.getCurrentVictoryPoints() + "\n\tEmporiums = " + getResidualEmporium() + "\n\tAssistants = "
				+ assistants.size() + "\n}";
	}
	
	


}

