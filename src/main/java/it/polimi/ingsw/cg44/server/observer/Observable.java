package it.polimi.ingsw.cg44.server.observer;

import java.util.ArrayList;
import java.util.List;





public abstract class Observable{
	private List<Observer> observers;

	/**
	 * 
	 */
	public Observable() {
		this.observers = new ArrayList<Observer>();
	}
	
	public void registerObserver(Observer ob){
		this.observers.add(ob);
	}
	public void unregisterObserver(Observer ob){
		this.observers.remove(ob);
	}

	
	protected void notifyObserver() {
		for(Observer o: this.observers){
			o.update();
		}		
	}

}
