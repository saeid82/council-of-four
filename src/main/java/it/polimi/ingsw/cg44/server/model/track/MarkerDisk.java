package it.polimi.ingsw.cg44.server.model.track;


import java.util.Iterator;

import it.polimi.ingsw.cg44.server.model.bonus.SpaceBonus;

/*
 * Classe indicatore che ha un percorso e la casella corrente, che si può 
 * settare grazie al metodo setCurrentSpace che utilizza un ListIterator per 
 * scorrere il percorso. Viene usata solo per scorrere il percorso nobiltà
 * poichè è l'unico provvisto di Bonus.
 */


public class MarkerDisk{

	private SpaceBonus currentSpace;
	private Iterator<SpaceBonus> trackIterator;
	
	
	/**
	 * Non abbiamo bisognmo di passargli indice di partenza 
	 * perchè per il percorso nobiltà partirà sempre dalla casella 0.
	 * @param track
	 */
	public MarkerDisk(NobilityTrack track) {
		trackIterator = track.getTrack().iterator();
		//spstiamo iteratore dalla posizione -1 alla posizione 0 
		//(quando è prima del primo elemnto della lista) 
		this.currentSpace = trackIterator.next();
		
	}

	public SpaceBonus getCurrentSpace() {
		return currentSpace;
	}
	

	/**
	 * metodo che permette solamente di avanzare di una casella alla volta.
	 * @return
	 */
	public SpaceBonus nextCurrentSpace() {

		if(trackIterator.hasNext()) {
			this.currentSpace = trackIterator.next();
			return this.currentSpace;
		}	
		else {
			throw new NullPointerException("Percorso nobiltà terminato, impossibile muovere indicatore.");
		}
		
	}

	@Override
	public String toString() {
		return "MarkerDisk (currentSpace= " + currentSpace + ")";
	}
		
}
	

