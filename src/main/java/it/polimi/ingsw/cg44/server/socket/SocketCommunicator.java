package it.polimi.ingsw.cg44.server.socket;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.util.NoSuchElementException;
import java.util.logging.Logger;

public class SocketCommunicator implements Communicator<String> {

	private static final Logger LOGGER = Logger.getLogger(SocketCommunicator.class.getName());
	Socket socket;
	ObjectInputStream in;
	ObjectOutputStream out;
	PrintWriter pWriter;
    /**
     * Player has 30 seconds time to doing action, otherwise lose the turn
     */
    public static final int CLIENT_ACTION_TIMEOUT = 30*1000;

	public SocketCommunicator(Socket sc) {
		socket = sc;
		
		try {
			socket.setSoTimeout(CLIENT_ACTION_TIMEOUT);
			pWriter = new PrintWriter(sc.getOutputStream());
			out = new ObjectOutputStream(socket.getOutputStream());
			in = new ObjectInputStream(socket.getInputStream());
		} catch (IOException ex) {
			LOGGER.info(ex.getMessage());
		}
	}

	public String receive() throws SocketException {
		try {
			return ((String) in.readObject());
		} catch (NoSuchElementException | ClassNotFoundException | IOException e) {
			throw new SocketException("Socket failed");
		}
	}

	public void send(String message) {
//		 try {
		LOGGER.info("Sending a message to the socket: " + message);
		pWriter.println(message);
		pWriter.flush();
		
		//in.reset();
		// out.writeObject(message);
		// out.flush();
//		 } catch (IOException e) {
//		
//		 }
	}
	
	public void close() {
		try {
			socket.close();
		} catch (IOException e) {
			throw new AssertionError("error closing the socket", e);
		}
	}

}
