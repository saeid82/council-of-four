package it.polimi.ingsw.cg44.server.model.bonus;

import java.util.Random;

import it.polimi.ingsw.cg44.server.model.game.Board;
import it.polimi.ingsw.cg44.server.model.game.Player;

/**
 * @author saeid
 *
 */
public class CoinsBonus extends Bonus {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2014826484953104909L;
	private final int quantity;
	private static final int MAX_QUANTITY_COINS = 9;
	
	public CoinsBonus() {
		Random  random = new Random();
		this.quantity = random.nextInt(MAX_QUANTITY_COINS)+1; // + 1  per evitare che si generino bonus con quantità nulla
		//this.quantity=(int) (Math.random().*MAX_QUANTITY_COINS + 1);		
	}

	public int getQuantity() {
		return quantity;
	}
	
	@Override
	public void useBonus(Player player, Board board) {
		player.getCoinsTrack().setCoinsNumber(quantity);
	}

	@Override
	public String toString() {
		return "CoinsBonus(quantity= " + quantity + ")";
	}

}
