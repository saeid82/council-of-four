package it.polimi.ingsw.cg44.server.socket;
import java.io.Serializable;
import java.net.SocketException;

public interface Communicator<M extends Serializable> {
	
	public M receive() throws SocketException;
	public void send(M message);

}