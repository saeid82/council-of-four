package it.polimi.ingsw.cg44.server.controller.action.quickaction;

import it.polimi.ingsw.cg44.server.controller.action.Action;
import it.polimi.ingsw.cg44.server.model.game.Board;
import it.polimi.ingsw.cg44.server.model.game.Player;

/**
 * Quick Action 1:
 * The player pays three coins, take an assistant from the pool,
 * if they are unable to move back their marker on the coins track 3 spaces, 
 * they cannot perform an action. 
 * @author saeid
 *
 */
public class EngageAssistant extends Action {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8830259297498237956L;
	private static final int COINS_TO_PAY = -3;
	private transient Player player;
	private Board board;
	
	public EngageAssistant(Player player, Board board) {
		this.board = board;
		this.player = player;
	}
	

	@Override
	public void run() {
		try {
			if(player.getCoinsTrack().getCurrentCoinsNumber() > 3){
				player.getCoinsTrack().setCoinsNumber(COINS_TO_PAY);
				player.getAssistants().add(board.getAssistantsReserve().getAssistants().poll());
			}else {
				throw new Exception("You don't have enough money!");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
	}
}
