package it.polimi.ingsw.cg44.server.model.balcony;

import java.util.Deque;
import java.util.LinkedList;

/**
 * each region have a balcony with FIFO algorithm
 * @author saeid
 *
 */
public class Balcony {
	
	/**
	 * FIFO seats
	 */
	private final Deque<Councillor> bench;

	public Balcony() {
		bench = new LinkedList<>();
	}

	public Deque<Councillor> getBench() {
		return bench;
	}
	
	
	 /**
	  * for elect councillor needs to take a councillor from reserve and put at the end of the bench and removed the first in the queue
	 * @param councillorReserve
	 * @param councillor
	 */
	public void electCouncillor(CouncillorReserve councillorReserve , Councillor councillor){
		 Councillor councilRemoved = bench.pollLast();
		 councillorReserve.getCouncillorInReserve().add(councilRemoved);
		 bench.push(councillor);
	 }

	@Override
	public String toString() {
		return "Balcony [bench=" + bench + "]";
	}
	 
	 

}
