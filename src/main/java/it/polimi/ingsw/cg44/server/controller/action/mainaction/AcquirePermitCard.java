package it.polimi.ingsw.cg44.server.controller.action.mainaction;

import java.util.ArrayList;
import java.util.List;


import it.polimi.ingsw.cg44.server.controller.action.Action;
import it.polimi.ingsw.cg44.server.model.balcony.Councillor;
import it.polimi.ingsw.cg44.server.model.board.NormalRegion;
import it.polimi.ingsw.cg44.server.model.bonus.factory.IBonus;
import it.polimi.ingsw.cg44.server.model.card.permitcard.PermitCard;
import it.polimi.ingsw.cg44.server.model.card.politiccard.PoliticCard;
import it.polimi.ingsw.cg44.server.model.colors.Colors;
import it.polimi.ingsw.cg44.server.model.game.Board;
import it.polimi.ingsw.cg44.server.model.game.Player;

/**
 * Action 2:
 * The player chooses and satisfies the council of a region by discarding between 1 and 4 politics cards
 * corresponding to the colors of the councillors present in the council.
 * The Joker can be any color. The player pays a sum of money, depending on the number of councillors satisfied.
 * 1 card played = 10 coins
 * 2 cards played = 7 coins
 * 3 cards played = 4 coins
 * 4 cards played = 0 coins
 * Each Joker card played = 1 additional coin
 * The player chooses one of the two permit card face up in front of the council they have satisfied.
 * then takes it and place it face up in front of them.
 * They immediately obtain the bonuses indicated at the bottom of the card.
 * They replace the card with the first card of the corresponding deck.
 * @author saeid
 *
 */
public class AcquirePermitCard extends Action {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6625218465562386366L;
	private transient Player player;
	private transient NormalRegion region;
	private transient List<PoliticCard> politicCards;
	private int numCardToDraw; // should be 0 or 1
	private Board board;
	
	

	/**
	 * @param player
	 * @param region
	 * @param politicCards
	 * @param numCardToDraw
	 * @param board
	 */
	public AcquirePermitCard(Board board, Player player, NormalRegion region, List<PoliticCard> politicCards, int numCardToDraw) {
		this.region = region;
		this.player = player;
		this.politicCards = politicCards;
		this.numCardToDraw = numCardToDraw; // 0 or 1
		this.board = board;
	}

	@Override
	public void run() {
				SatisfyCouncillor satisfyCouncillor = new SatisfyCouncillor(player, region, politicCards);
				satisfyCouncillor.satisfy();
				
				// The player chooses one of the two permit card face up in front of the council they have satisfied.
				// then takes it and place it face up in front of them.
				PermitCard selectedPermitCard = region.getDeckOfPermitCard().drawFaceUpPermitCard(numCardToDraw);
				player.getPermitCards().add(selectedPermitCard);
				
				//The player immediately obtain the bonuses indicated at the bottom of the card.
				
				for(IBonus bonus : selectedPermitCard.getBonus())
					try {
						bonus.useBonus(player, board);
						System.out.println("\nYou obtained the bonuses indicated at the bottom of the card:\n\n"+selectedPermitCard.getBonus());
					} catch (Exception e) {
						System.out.println(e.getMessage());
					}
		
	}
}
