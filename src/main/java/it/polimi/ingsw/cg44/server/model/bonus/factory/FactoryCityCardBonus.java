package it.polimi.ingsw.cg44.server.model.bonus.factory;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
/**
 * @author saeid
 *
 */
public class FactoryCityCardBonus extends FactoryBonus {
	
	
	/**
	 * costante usata per garantire la generazione dei possibili bonus 
	 * (i primi 6 elementi della mappa BonusClasses in generateBonus)
	 */
	private static final int BOUND_CHOICE_BONUS = 8;
	
	/**
	 * 
	 */
	public FactoryCityCardBonus() {
		super();
	}
	

	@Override
	public List<IBonus> getBonus() {
		Random random = new Random();
		List<IBonus> listOfAddedBonus = new ArrayList<>();	
		int quantityBonus = chooseQuantity(); // 3 possibili valori: 1,2,3
		
		try{			
			for(int i=0; i<quantityBonus; i++){
				Class<?> bonusToAdd = Class.forName(bonusClasses.get(random.nextInt(BOUND_CHOICE_BONUS)));
				if(conditionToAddBonus(bonusToAdd, listOfAddedBonus)){	
					i--;	
				}					
				else {
					listOfAddedBonus.add((IBonus) bonusToAdd.newInstance());
				}
			}
			return listOfAddedBonus;
		}
		catch(ClassNotFoundException e){
			//System.out.println(e.getMessage());
		}
		catch (InstantiationException e) {
			//System.out.println(e.getMessage());
		}
		catch (IllegalAccessException e) {
			//System.out.println(e.getMessage());
		}
		return listOfAddedBonus;
	}



}
