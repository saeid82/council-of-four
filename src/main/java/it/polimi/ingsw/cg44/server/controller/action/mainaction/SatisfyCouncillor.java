package it.polimi.ingsw.cg44.server.controller.action.mainaction;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.cg44.server.model.balcony.Councillor;
import it.polimi.ingsw.cg44.server.model.board.Region;
import it.polimi.ingsw.cg44.server.model.card.politiccard.PoliticCard;
import it.polimi.ingsw.cg44.server.model.colors.Colors;
import it.polimi.ingsw.cg44.server.model.game.Player;

public class SatisfyCouncillor {
	
	private Player player;
	private Region region;
	private List<PoliticCard> playerPoliticCards;
	private List<PoliticCard> politicCardsMatched;
	/**
	 * @param player
	 * @param region
	 * @param playerPoliticCards
	 */
	public SatisfyCouncillor(Player player, Region region, List<PoliticCard> playerPoliticCards) {
		this.player = player;
		this.region = region;
		this.playerPoliticCards = playerPoliticCards;
		politicCardsMatched = new ArrayList<PoliticCard>();
	}

	public void satisfy(){
		int jokerCounter = 0;
		// creat a copy of balcony for doing the operation
		ArrayDeque<Councillor> copyBench = new ArrayDeque<>();
		
		// temporaneo solo per copy/paste
		ArrayDeque<Councillor> tmpBench = new ArrayDeque<>();	
		int benchSize = region.getBalcony().getBench().size();
		for(int i=0 ; i < benchSize ; i++){
			tmpBench.add(region.getBalcony().getBench().remove());	
		}
		// copy/paste
		copyBench = tmpBench.clone();
		
		//restituisco i councillor come orginale
		int copySize = copyBench.size();
		for(int i=0 ; i < copySize ; i++){
			region.getBalcony().getBench().push(tmpBench.poll());
		}

		for(PoliticCard politicCard : playerPoliticCards){ 
			if(politicCard.getCardColor() == Colors.JOKER){
				jokerCounter++;	
			}
			for(Councillor councillor : copyBench)
				if(politicCard.getCardColor().equals(councillor.getColors()) || politicCard.getCardColor().equals(Colors.JOKER)){
					politicCardsMatched.add(politicCard);
					copyBench.remove(councillor);
					break;
				}
			
		}
		System.out.println("\nPoliticards matched with councillor colors to discarding: "+politicCardsMatched);
		// discarding between 1 and 4 politics cards corresponding to the 
		// colors of the councillors present in the council.
		playerPoliticCards.removeAll(politicCardsMatched);

		//Control the player how much should pay.
		switch (politicCardsMatched.size()) {
		case 1: // 1 card played = 10 coins, Each Joker card played = 1 additional coin
			if(player.getCoinsTrack().getCurrentCoinsNumber() > 10+jokerCounter){
				player.getCoinsTrack().setCoinsNumber(-10 - jokerCounter);
			}					
			break;
		case 2: // 2 cards played = 7 coins, Each Joker card played = 1 additional coin
			if(player.getCoinsTrack().getCurrentCoinsNumber() > 7+jokerCounter){
				player.getCoinsTrack().setCoinsNumber(-7 - jokerCounter);
			}
			break;
		case 3: // 3 cards played = 4 coins, Each Joker card played = 1 additional coin
			if(player.getCoinsTrack().getCurrentCoinsNumber() > 4+jokerCounter){
				player.getCoinsTrack().setCoinsNumber(-4 - jokerCounter);
			}
			break;
		case 4: // 4 cards played = 0 coins, Each Joker card played = 1 additional coin
			if(player.getCoinsTrack().getCurrentCoinsNumber() > jokerCounter){
				player.getCoinsTrack().setCoinsNumber(-jokerCounter);
			}
			break;
		case 0:
			System.out.println("You don't have any politic card matched with councillor color.");
			break;
		
		default:
			if(player.getCoinsTrack().getCurrentCoinsNumber() > jokerCounter){
				player.getCoinsTrack().setCoinsNumber(-jokerCounter);
			}
			break;
		}
	}
}
