package it.polimi.ingsw.cg44.server.model.bonus;



import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.cg44.server.model.bonus.factory.IBonus;
import it.polimi.ingsw.cg44.server.model.bonus.factory.FactoryBonus;
import it.polimi.ingsw.cg44.server.model.bonus.factory.FactorySpaceBonus;



//  rapresents the space with bonus, it's used for the nobility track.


public class SpaceBonus implements Comparable<SpaceBonus>{

	private final List<IBonus> bonus;
	private int spaceNumber;
	
	
	public SpaceBonus(int number) throws NullPointerException {
		FactoryBonus factoryBonus = new FactorySpaceBonus();
		this.bonus = factoryBonus.getBonus();
		this.spaceNumber = number;
	}
	
	
	/**
	 * Costruttore per la prima casella, che non deve avere bonus
	 */
	public SpaceBonus() {
		this.bonus = new ArrayList<>();
		this.spaceNumber = 0;
	}
	
//	getters per SpaceNumbere e la lista di bonus (bonus)
	public List<IBonus> getBonus() {
		return bonus;
	}
	
	public int getSpaceNumber() {
		return spaceNumber;
	}
	
	//Ho dovuto aggiungere hasCOde e equals perché sonar diceva che il compareTo poteva fallire senza
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + spaceNumber;
		result = prime * result + ((bonus == null) ? 0 : bonus.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SpaceBonus other = (SpaceBonus) obj;
		if (spaceNumber != other.spaceNumber)
			return false;
		if (bonus == null) {
			if (other.bonus != null)
				return false;
		} else if (!bonus.equals(other.bonus))
			return false;
		return true;
	}

	//	Override metodo comparTo per garantire ordine nel SortedSet NobilityTrack.
	@Override
	public int compareTo(SpaceBonus o){
		return this.spaceNumber - o.spaceNumber;
	}
	
	@Override
	public String toString() {
		return "{ SpaceBonus--> SpaceNumber: (" + this.getSpaceNumber() + "), BonusQuantity: (" + this.bonus.size() + "),"
				+ " BonusType(" + this.bonus + ") }\n";
	}
	

}
