package it.polimi.ingsw.cg44.server.model.track;



/**
 * @author saeid
 *
 */
public class VictoryTrack {
	
	private int currentVictoryPoints;

	public VictoryTrack() {
		this.currentVictoryPoints = 0;
	}

	public int getCurrentVictoryPoints() {
		return currentVictoryPoints;
	}

	public void setCurrentVictoryPoints(int currentVictoryPoints) throws NullPointerException {
		this.currentVictoryPoints += currentVictoryPoints;
	}

	@Override
	public String toString() {
		return "VictoryTrack (currentVictoryPoints= " + currentVictoryPoints + ")";
	}
}

