package it.polimi.ingsw.cg44.server.controller.action.mainaction;

import it.polimi.ingsw.cg44.server.controller.action.Action;
import it.polimi.ingsw.cg44.server.model.balcony.Councillor;
import it.polimi.ingsw.cg44.server.model.board.Region;
import it.polimi.ingsw.cg44.server.model.colors.Colors;
import it.polimi.ingsw.cg44.server.model.game.Board;
import it.polimi.ingsw.cg44.server.model.game.Player;

/**
 * Action 1:
 * The player chooses one of their available councillors at the side of the board, inserts them in the 
 * council balcony of their choice on the side of the arrow, pushing the councillors present in that council until
 * the last one falls out, they remove the fallen councillor and place them together with the other councillors at 
 * the side of the board, the player receive 4 coins marking this moving their marker on the coins track.
 * @author saeid
 *
 */
public class ElectCouncillor extends Action {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5302645742386463068L;
	private static final int RECEIVED_COINS = 4;
	private transient Player player;
	private Board board;
	private transient Region region;
	private Colors colore;
	
	public ElectCouncillor(Player player, Board board, Region region, Colors colore) {
		this.board = board;
		this.player = player;
		this.region = region;
		this.colore = colore;
	}

	@Override
	public void run() {
		Councillor selectedCouncillor =  board.getCouncillorReserve().getCouncillorByColor(colore);
		region.getBalcony().electCouncillor(board.getCouncillorReserve(), selectedCouncillor);
		
		player.getCoinsTrack().setCoinsNumber(RECEIVED_COINS);
		
	}
}
