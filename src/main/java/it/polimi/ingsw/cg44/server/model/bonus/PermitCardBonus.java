package it.polimi.ingsw.cg44.server.model.bonus;

import java.util.InputMismatchException;
import java.util.Scanner;
import it.polimi.ingsw.cg44.server.model.board.RegionType;
import it.polimi.ingsw.cg44.server.model.game.Board;
import it.polimi.ingsw.cg44.server.model.game.Player;

/**
 * Bonus to draw a permit card from the deck. 
 * there is just in the nobility track.
 * @author saeid
 *
 */
public class PermitCardBonus extends Bonus {


	/**
	 * 
	 */
	private static final long serialVersionUID = 3825568499035887489L;
	private static Scanner input;

	@Override
	public void useBonus(Player player, Board board) {
		RegionType region = selectRegion(selectNameRegion());
		selectPermitcard(board, region);
	}
	/**
	 * the method for asking client to select one permitCard face up
	 * @param game
	 * @param regionType
	 * @return
	 */
	public static int selectPermitcard(Board board , RegionType regionType){
		int choose =0;
		boolean condition = true;
		System.out.println("\nSelect the one of the two faceUp permitcard:('0' or '1')");
		System.out.println("\n.............................................................................\n0 --> "
				+ board.getRegions(regionType).getDeckOfPermitCard().getFsceUpPermitCard().get(0));
		System.out.println("\n.............................................................................\n1 --> "
				+ board.getRegions(regionType).getDeckOfPermitCard().getFsceUpPermitCard().get(1)+"\n"
						+ "..............................................................................");
		while (condition) {
			try {
				choose = input.nextInt();
				if(choose==0 || choose==1){
					condition = false;
					return choose;
				}
				System.out.println("Try again! 'Incorrect input'\tinsert '0' or '1'\n");
			} catch (InputMismatchException e) {
				input.nextLine();
			}
		}
		return choose;
	}
	/**
	 * Metodo usato al interno del metodo selectRegion per prendere in ingresso dal utente 
	 * come String il nome della regione
	 * @return
	 */
	public static String selectNameRegion(){
		String regionTypeStr = null;
		boolean condition = true;
		System.out.println("\nSelect a region:\n\n\t1-HILL\n\t2-COAST\n\t3-MOUNTAIN\n");
		while (condition) {
			try {
				regionTypeStr = input.next().toUpperCase();
				if(regionTypeStr.equals("HILL") || regionTypeStr.equals("COAST") || regionTypeStr.equals("MOUNTAIN")){
					condition = false;
					return regionTypeStr;
				}
				System.out.println("Try again! 'Incorrect input'\n");
			} catch (InputMismatchException e) {
				input.nextLine().toUpperCase();
			}
		}
		return regionTypeStr;
	}
	/**
	 * the method for asking to client to select region
	 * @return RegionType
	 */
	public static RegionType selectRegion(String regionName){
		RegionType regionType = null;
		switch (regionName) {
		case "HILL":
			regionType = RegionType.HILL;
			break;
		case "COAST":
			regionType = RegionType.COAST;
			break;
		case "MOUNTAIN":
			regionType = RegionType.MOUNTAIN;
			break;
		default:
			//System.out.println("The region type is not correct!");
		}
		return regionType;
	}
	@Override
	public String toString() {
		return "(PermitCardBonus)";
	}

}
