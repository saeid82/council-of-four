package it.polimi.ingsw.cg44.server.model.game;


import java.io.Serializable;
import java.util.EnumMap;
import java.util.List;

import it.polimi.ingsw.cg44.server.model.assistant.AssistantsReserve;
import it.polimi.ingsw.cg44.server.model.balcony.CouncillorReserve;
import it.polimi.ingsw.cg44.server.model.board.KingRegion;
import it.polimi.ingsw.cg44.server.model.board.NormalRegion;
import it.polimi.ingsw.cg44.server.model.board.Region;
import it.polimi.ingsw.cg44.server.model.board.RegionType;
import it.polimi.ingsw.cg44.server.model.city.City;
import it.polimi.ingsw.cg44.server.model.colors.Colors;
import it.polimi.ingsw.cg44.server.model.king.King;
import it.polimi.ingsw.cg44.server.model.track.NobilityTrack;



/**
 * @author saeid
 *
 */
public class Board implements Serializable {


	private static final long serialVersionUID = -972564695287940227L;
	private transient CouncillorReserve councillorReserve;
	private transient AssistantsReserve assistantsReserve;
	private final EnumMap<RegionType, NormalRegion> regions;
	private transient KingRegion kingRegion;
	private transient List<City> cities;
	private final King king;
	private transient NobilityTrack nobilityTrack;
	

	protected Board(List<City> cities, int numberPlayers) throws Exception {
		
		this.councillorReserve = new CouncillorReserve();	
		this.regions = new EnumMap<>(RegionType.class);		
		this.regions.put(RegionType.COAST, new NormalRegion(RegionType.COAST, cities, numberPlayers));		
		this.regions.put(RegionType.MOUNTAIN, new NormalRegion(RegionType.MOUNTAIN, cities, numberPlayers));
		this.regions.put(RegionType.HILL, new NormalRegion(RegionType.HILL, cities, numberPlayers));
		this.kingRegion = new KingRegion();
		

		setBalconies(councillorReserve, kingRegion);
		for(NormalRegion region : regions.values())
			setBalconies(councillorReserve, region);
		
		this.cities = cities;		
		//Il metodo setKing si occupa di andare a selezionare la città del re e passare il suo riferimento
		//al re. Questo metodo ritorna direttamente l'istanza del re così settata.
		this.king = setKing(cities);
		this.nobilityTrack = new NobilityTrack();
		this.assistantsReserve = new AssistantsReserve(numberPlayers);
	
	}
	
	public NormalRegion getRegions(RegionType regionType) {
		return regions.get(regionType);
	}

	public KingRegion getKingRegion() {
		return kingRegion;
	}

	public List<City> getCities() {
		return cities;
	}

	public CouncillorReserve getCouncillorReserve() {
		return councillorReserve;
	}

	public King getKing() {
		return king;
	}

	public NobilityTrack getNobilityTrack() {
		return nobilityTrack;
	}
	

	public AssistantsReserve getAssistantsReserve() {
		return assistantsReserve;
	}

	/**
	 * 	sceglie tra le città qual è quella del re e setta la città scelta nella variabile king. 
	 * Infine restituisce la variabile così settata.
	 * @param cities
	 * @return
	 */
	private King setKing(List<City> cities) {
		King king = new King();
		for(City city : cities) 
			if(city.getColor()==Colors.KING) 
				king.moving(city);
		return king;
	}
	
	
	/**
	 * metodo per inizializzare i balcony riempiendoli dalla riserva
	 * @param reserve
	 * @param region
	 */
	private void setBalconies(CouncillorReserve reserve, Region region){
		
		final int MAX_NUMBER_OF_SEATS = 4;
		
		for(int i = 0; i < MAX_NUMBER_OF_SEATS; i++){
			region.getBalcony().getBench().push(reserve.getCouncillorInReserve().remove(0));
		}	
	}

	@Override
	public String toString() {
		return "Map( CouncillorReserve= " + councillorReserve + ", AssistantsReserve= " + assistantsReserve + ", Regions= "
				+ regions + ", KingRegion= " + kingRegion + ", Cities= " + cities + ", King= " + king + ", NobilityTrack= "
				+ nobilityTrack + " )";
	}

	
}
