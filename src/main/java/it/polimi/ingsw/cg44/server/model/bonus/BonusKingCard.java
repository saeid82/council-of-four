package it.polimi.ingsw.cg44.server.model.bonus;



import it.polimi.ingsw.cg44.server.model.bonus.factory.IBonus;
import it.polimi.ingsw.cg44.server.model.game.Board;
import it.polimi.ingsw.cg44.server.model.game.Player;

/**
 * The class of KingCard bonus chat contain just Victory point bonus.
 * @author saeid
 *
 */
public class BonusKingCard extends Bonus{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1270309671045166056L;
	private final IBonus bonus;
	private boolean isUsed;
	/**
	 * @param bonus
	 * @param isUsed
	 */
	public BonusKingCard() {
		super();
		this.bonus = new VictoryPointBonus();
		this.isUsed = false;
	}
	
	public boolean isUsed(){
		return isUsed;
	}
	
	@Override
	public void useBonus(Player player, Board board) throws Exception {
		try {
			if(!isUsed) {
				bonus.useBonus(player, board);
				this.isUsed = true;
			}
		} catch (Exception e) {
			System.out.println("This card is already used!");
			System.out.println(e.getMessage());
			}
	}
	

	public IBonus getBonus() {
		return bonus;
	}

	@Override
	public String toString() {
		return "BonusCard (bonus= " + bonus + ")";
	}



	
	
	
}

