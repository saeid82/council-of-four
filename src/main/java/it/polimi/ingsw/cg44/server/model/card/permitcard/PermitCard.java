package it.polimi.ingsw.cg44.server.model.card.permitcard;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import it.polimi.ingsw.cg44.server.model.bonus.factory.IBonus;
import it.polimi.ingsw.cg44.server.model.bonus.factory.FactoryBonus;
import it.polimi.ingsw.cg44.server.model.bonus.factory.FactoryCityCardBonus;
import it.polimi.ingsw.cg44.server.model.city.City;



/*
 * 
 * Questa è l'unica classe che ha dei metodi pubblici che non fanno parte dell'interfaccia Card
 * per questo motivo è l'unica delle card che non possiamo istanziare con tipo statico Card
 * 
 */

public class PermitCard implements IQuantity{

	private final List<IBonus> bonus;
	private final List<City> city;	
	private boolean used;	//Serve per verificare se è stata già usata o no.
	
	
	/**
	 * Costruttore a cui bisogna passargli la costante region appropriata
	 * @param cities
	 * @throws IllegalArgumentException
	 */
	protected PermitCard(List<City> cities) throws IllegalArgumentException {
		FactoryBonus factoryBonus = new FactoryCityCardBonus();
		this.bonus = factoryBonus.getBonus();
		this.city = chooseCity(cities);
		this.used = false;
	}
	
	
	/**
	 * Da chiamare quando il giocatore usa la carta
	 */
	public void turnOff(){
		this.used=true;
	}
	
	
	/**
	 * Verifica se la carta è stata già usata
	 * @return
	 */
	public boolean isUsed() {
		return used;
	}

	public void useCard() throws Exception{
		if(!used)
			this.used = true;
		else
			throw new Exception("This permit card has already been used!");
	}
	
	public List<IBonus> getBonus() {
		return bonus;
	}

	public List<City> getCities() {
		return city;
	}

	
	/**
	 * Sceglie le città per le carte permesso
	 * @param cities
	 * @return
	 */
	private List<City> chooseCity(List<City> cities) {
		//choseCity è la lista di città che andremo a restituire alla permitCard dopo averne inizializzato randomicamente
		//gli elementi facendo uso dei metodi chooseQuantity e containCity
		List<City> choseCities = new ArrayList<>();
		//Tramite invocazione metodo chooseQuantity scegliamo randomicamente la dimensione della lista di città associata
		//alla carta permesso in questione.
		for(int i=0; i<chooseQuantity() ; i++) {
			Random random = new Random();
			City cityToAdd = cities.get(random.nextInt(cities.size()));
			if(containCity(choseCities, cityToAdd)) 
				i--;
			else
				choseCities.add(cityToAdd);
		}
		return choseCities;
	}
	
	
	/**
	 * Controlla se abbiamo già aggiunto la stessa città alla carta permesso
	 * @param cities
	 * @param cityToAdd
	 * @return
	 */
	private boolean containCity(List<City> cities, City cityToAdd) {
		for(City cityadded : cities) {
			if(cityadded.equals(cityToAdd))
					return true;
		}
		return false;
	}

	@Override
	public String toString() {
		return "PermitCard:\n\n\tCity --> " + city + "\n\tBonus --> " + bonus + "\n\tUsed --> " + used + " \n\n";
	}
	
	
}	
	
	
	