package it.polimi.ingsw.cg44.server.view;

import it.polimi.ingsw.cg44.server.controller.Controller;
import it.polimi.ingsw.cg44.server.controller.MainActionController;
import it.polimi.ingsw.cg44.server.controller.action.IAction;
import it.polimi.ingsw.cg44.server.model.board.Region;
import it.polimi.ingsw.cg44.server.model.colors.Colors;
import it.polimi.ingsw.cg44.server.model.game.Board;
import it.polimi.ingsw.cg44.server.model.game.Player;

public class ElectCouncillorView extends View {

	private Region targetRegion;
	private Colors colore;
	
	public ElectCouncillorView(Player player, Region targetRegion, Board board, Colors colore) {
		super(player, board);
		this.targetRegion = targetRegion;
		this.colore = colore;
	}

	@Override
	public IAction createAction(Controller mainActionController) {
		if(isValid())
			return ((MainActionController) mainActionController).update(this);
		else
			return null;
	}

	public Region getTargetRegion() {
		return targetRegion;
	}

	public Colors getColore() {
		return colore;
	}
	
	@Override
	protected boolean isValid() {
		//Caso in cui la riserva è vuota
		if(getBoard().getCouncillorReserve().getCouncillorInReserve().isEmpty()) {
			System.out.println("Councillor reserve is empty, you can't elect a councillor.");
			return false;
		}
		
		//Caso in cui non c'è il consigliere del colore richiesto
		if(!getBoard().getCouncillorReserve().isContainsCouncillor(colore)) {
			System.out.println("Non c'è il consigliere di quel colore nella riserva. Cambia colore");
			return false;
		}
		else
			return true;
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}


}
