package it.polimi.ingsw.cg44.server.view;

import it.polimi.ingsw.cg44.server.controller.Controller;
import it.polimi.ingsw.cg44.server.controller.action.IAction;
import it.polimi.ingsw.cg44.server.model.game.Board;
import it.polimi.ingsw.cg44.server.model.game.Player;
import it.polimi.ingsw.cg44.server.observer.Observable;
import it.polimi.ingsw.cg44.server.observer.Observer;




/**
 * MVC pattern
 * @author saeid
 *
 */
public abstract class View extends Observable implements Observer{

	
	private Player player;
	private Board board;
	
	/**
	 * @param player
	 * @param board
	 */
	public View(Player player, Board board) {
		super();
		this.player = player;
		this.board = board;
	}

	public Player getPlayer() {
		return player;
	}

	public Board getBoard() {
		return board;
	}
	
	protected abstract IAction createAction(Controller controller) throws Exception;


	protected abstract boolean isValid() throws Exception;


	
}

