package it.polimi.ingsw.cg44.server.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.function.Predicate;
import java.util.logging.Logger;

import it.polimi.ingsw.cg44.server.controller.action.IAction;
import it.polimi.ingsw.cg44.server.model.Model;
import it.polimi.ingsw.cg44.server.model.board.RegionType;
import it.polimi.ingsw.cg44.server.model.colors.Colors;
import it.polimi.ingsw.cg44.server.socket.ClientSocketContainer;
import it.polimi.ingsw.cg44.server.view.AcquirePermitCardView;
import it.polimi.ingsw.cg44.server.view.BuildEmporiumUsingPermitCardView;
import it.polimi.ingsw.cg44.server.view.BuildEmporiumWithKingHelpView;
import it.polimi.ingsw.cg44.server.view.ChangeFaceUpPermitCardView;
import it.polimi.ingsw.cg44.server.view.ElectCouncillorView;
import it.polimi.ingsw.cg44.server.view.EngageAssistantView;
import it.polimi.ingsw.cg44.server.view.SendAssistantToElectCouncillorView;

public class CliGameControllerHelper {

	static Logger logger = Logger.getLogger(ClientSocketContainer.class.getName());

	private BufferedReader input;

	private Model game;
	private int playerNumber;
	private ClientSocketContainer clientSocketContainer;
	public static final String PLAYER_TURN_INDICATOR = "It's the turn of player %d: ";
	private static final String SELECT_QCK_ACTION = "Select a Quick actions number:\n\n\t1-Change face up permit card\n\t2-Engage assistant"
			+ "\n\t3-Perform additional main action\n\t4-Send assistant to elect councillor\n";
	private static final String SELECT_MAIN_ACTION = "\nYou can do one MAIN action from the four available action, Select an actions number:\n\n\t1-Acquire permit card\n\t2-Build emporium using permit card"
						+ "\n\t3-Build emporium with king help\n\t4-Elect councillors\n";


	/**
	 * @param game
	 * @param playerNumber
	 * @param clientSocketContainer
	 * @throws IOException
	 */
	public CliGameControllerHelper(Model game, int playerNumber, ClientSocketContainer clientSocketContainer)
			throws IOException {
		super();
		this.game = game;
		this.playerNumber = playerNumber;
		this.clientSocketContainer = clientSocketContainer;
		input = new BufferedReader(new InputStreamReader(clientSocketContainer.getSocket().getInputStream()));
	}

	public void play() throws Exception {
		String line = "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~";
		String space ="\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
		clientSocketContainer.getSocketCommunicator().send(line);
		clientSocketContainer.getSocketCommunicator()
				.send("\n\t\t\t\t\tPLAYER " + game.getPlayers().get(playerNumber).getId());
		clientSocketContainer.getSocketCommunicator().send(line);
		clientSocketContainer.getSocketCommunicator()
				.send("Your situaztion is:\n" + game.getPlayers().get(playerNumber));
		clientSocketContainer.getSocketCommunicator().send(line+space+"MAIN ACTION"+space+"\n"+line);
		clientSocketContainer.getSocketCommunicator().send(SELECT_MAIN_ACTION);


		
		int mainActionNumber = selectActionNumber();
		switch (mainActionNumber) {// switch per main action
		case 1:
			acquirePermitCard();
			break;
		case 2:
			buildEmporiumUsingPermitCard();
			break;
		case 3:
			buildEmporiumWithKingHelp();
			break;
		case 4:
			electCouncillor();
			break;
		default:
		}// swith finish --- main action

		clientSocketContainer.getSocketCommunicator()
				.send("Now your situation is:\n" + game.getPlayers().get(playerNumber));

		clientSocketContainer.getSocketCommunicator().send(line+space+"Quick ACTION"+space+"\n"+line);

		clientSocketContainer.getSocketCommunicator().send(
				"You can do one QUICK action from the four available action!\nDo you want to perform a Quick action? [y/n]");
		// controllo input del utente che sia giusto
		String answer = playerAnswer();

		if (answer.toUpperCase().equals("Y")) {

			clientSocketContainer.getSocketCommunicator().send(SELECT_QCK_ACTION);
			int quickActionNumber = selectActionNumber();
			switch (quickActionNumber) {
			case 1:
				changeFaceUpPermitCard();
				break;
			case 2:
				engageAssistant();
				break;
			case 3:
				performAdditionalMainAction();
				break;
			case 4:
				sendAssistantToElectCouncillor();
				break;
			default:
			}// switch quickaction finished
			clientSocketContainer.getSocketCommunicator()
			.send("The player number " + game.getPlayers().get(playerNumber).getId() + " has terminated!");
		} else {
			clientSocketContainer.getSocketCommunicator()
					.send("The player number " + game.getPlayers().get(playerNumber).getId() + " has terminated!");
		}


		clientSocketContainer.getSocketCommunicator().send(line);
	}

	/**
	 * the method for main action: acquirePermitCard
	 * 
	 * @param game
	 * @param playerNumber
	 */
	public void acquirePermitCard() {
		String line = "\n*********************************************************************************************\n";
		clientSocketContainer.getSocketCommunicator().send("You have to satisfay the councillor with your politic card:\n\n\t\t\t"
				+game.getPlayers().get(playerNumber).getPoliticCards()+ "\n");
		clientSocketContainer.getSocketCommunicator().send(line+"-The councillors in the balcony of HILL region are:\n\n"
				+ game.getBoard().getRegions(RegionType.HILL).getBalcony().getBench() +line);
		clientSocketContainer.getSocketCommunicator().send("-The councillors in the balcony of COAST region are:\n\n"
				+ game.getBoard().getRegions(RegionType.COAST).getBalcony().getBench()+ line);
		clientSocketContainer.getSocketCommunicator().send("-The councillors in the balcony of MOUNTAIN region are:\n\n"
				+ game.getBoard().getRegions(RegionType.MOUNTAIN).getBalcony().getBench()+ line);

		String nameRegion = selectNameRegion();
		RegionType regionType = selectRegion(nameRegion);
		switch (regionType) {
		case HILL:
			hillAcquirePermirCard();
			break;
		case COAST:
			coastAcquirePermitCard();
			break;
		case MOUNTAIN:
			mountainAcquirePermitCard();
			break;
		default:
		}

	}

	/**
	 * the method used for AcquirePErmiCard in case of HILL region selected by
	 * player
	 * 
	 * @param game
	 * @param playerNumber
	 */
	public void hillAcquirePermirCard() {
		int permitcardselected = selectPermitcard(game, RegionType.HILL);

		if (permitcardselected == 0 || permitcardselected == 1) {
			AcquirePermitCardView acquirePermitCardView = new AcquirePermitCardView(game.getPlayers().get(playerNumber),
					game.getBoard().getRegions(RegionType.HILL), game.getBoard(),
					game.getPlayers().get(playerNumber).getPoliticCards(), permitcardselected);
			MainActionController mainActionController = new MainActionController(game.getPlayers());
			IAction action = acquirePermitCardView.createAction(mainActionController);
			action.run();
		} else {
			throw new IllegalArgumentException();
		}
	}

	/**
	 * the method used for AcquirePErmiCard in case of COAST region selected by
	 * player
	 * 
	 * @param game
	 * @param playerNumber
	 */
	public void coastAcquirePermitCard() {

		int permitcardselected = selectPermitcard(game, RegionType.COAST);
		if (permitcardselected == 0 || permitcardselected == 1) {
			AcquirePermitCardView acquirePermitCardView = new AcquirePermitCardView(game.getPlayers().get(playerNumber),
					game.getBoard().getRegions(RegionType.COAST), game.getBoard(),
					game.getPlayers().get(playerNumber).getPoliticCards(), permitcardselected);
			MainActionController mainActionController = new MainActionController(game.getPlayers());
			IAction action = acquirePermitCardView.createAction(mainActionController);
			action.run();
		} else {
			throw new IllegalArgumentException();
		}
	}

	/**
	 * the method used for AcquirePErmiCard in case of MOUNTAIN region selected
	 * by player
	 * 
	 * @param game
	 * @param playerNumber
	 */
	public void mountainAcquirePermitCard() {
		int permitcardselected = selectPermitcard(game, RegionType.MOUNTAIN);
		if (permitcardselected == 0 || permitcardselected == 1) {
			AcquirePermitCardView acquirePermitCardView = new AcquirePermitCardView(game.getPlayers().get(playerNumber),
					game.getBoard().getRegions(RegionType.MOUNTAIN), game.getBoard(),
					game.getPlayers().get(playerNumber).getPoliticCards(), permitcardselected);
			MainActionController mainActionController = new MainActionController(game.getPlayers());
			IAction action = acquirePermitCardView.createAction(mainActionController);
			action.run();
		} else {
			throw new IllegalArgumentException();
		}
	}

	/**
	 * The method for main action BuildEmporiumUsingPermitCard
	 * 
	 * @param game
	 * @param playerNumber
	 * @throws Exception
	 */
	public void buildEmporiumUsingPermitCard() {
		int permCardNum = selectPermitcardFromPly(); // the permitcard selected
												
		// permitcard può avere qualche city e non solo uno, quindi....
		for (int i = 0; i < game.getPlayers().get(playerNumber).getPermitCards().get(permCardNum).getCities()
				.size(); i++) {
			MainActionController mainActionController = new MainActionController(game.getPlayers());
			BuildEmporiumUsingPermitCardView buildEmporiumUsingPermitCardView = new BuildEmporiumUsingPermitCardView(
					game.getPlayers().get(playerNumber),
					game.getPlayers().get(playerNumber).getPermitCards().get(permCardNum).getCities().get(i),
					game.getBoard(), game.getPlayers().get(playerNumber).getPermitCards().get(permCardNum));
			IAction action = buildEmporiumUsingPermitCardView.createAction(mainActionController);
			action.run();
		}

	}

	/**
	 * The method for main action BuildEmporiumWithKingHelp
	 * 
	 * @param game
	 * @param playerNumber
	 */
	public void buildEmporiumWithKingHelp() {
		String line = "*********************************************************************************************************************";
		clientSocketContainer.getSocketCommunicator().send("Yu must satisfy the King's council! and select one city to moving the King.");
		clientSocketContainer.getSocketCommunicator().send("Attention!! you must have enough coins to moving King, for each road you have to pay 2 coins."
						+ "\nThe King may also be left in the same city.");
		clientSocketContainer.getSocketCommunicator().send("Currently the KING is located within the city:\n\n"
				+ line
				+ "\nKING ---> "
				+ game.getBoard().getKing().getCity() + "\n\n"+line+"\nSelect the city index:\n");

		for (int i = 0; i < game.getBoard().getCities().size(); i++) {
			clientSocketContainer.getSocketCommunicator().send(i + "-" + game.getBoard().getCities().get(i));
		}
		clientSocketContainer.getSocketCommunicator().send(line);
		int selectedCity = insistAndGetAnswer(input, (t)-> t>0 && t <16, s -> Integer.valueOf(s));
		BuildEmporiumWithKingHelpView buildEmporiumWithKingHelpView = new BuildEmporiumWithKingHelpView(
				game.getPlayers().get(playerNumber), game.getBoard(), game.getBoard().getCities().get(selectedCity));
		MainActionController mainActionController = new MainActionController(game.getPlayers());
		IAction action = buildEmporiumWithKingHelpView.createAction(mainActionController);
		action.run();
	}

	static interface StringConvertor<T> {
		T convert(String s);
	}

	/**
	 * tho method for asking player to insert the selection 
	 * @param input
	 * @param predicate
	 * @param convertor
	 * @return
	 */
	public <T> T insistAndGetAnswer(BufferedReader input, Predicate<T> predicate, StringConvertor<T> convertor) {
		while (true) {
			try {
				clientSocketContainer.getSocketCommunicator()
						.send("\n" + String.format(PLAYER_TURN_INDICATOR, playerNumber+1) + "\n");
				String selectedInput = readStringValue(input);
				T res = convertor.convert(selectedInput);
				if (!predicate.test(res))
					throw new IllegalStateException();
				return res;
			} catch (Exception e) {
				logger.info(e.getMessage());
				clientSocketContainer.getSocketCommunicator().send("Input value is wrong! Please try again!");
			} 
		}

	}

	/**
	 * The method for main action ElectCouncillor
	 * 
	 * @param game
	 * @param playerNumber
	 */
	public void electCouncillor() {
		String line = "-----------------------------------------------------------------------------------------";
		String lineStar = "*********************************************************************************************************************";
		clientSocketContainer.getSocketCommunicator().send(
				"\nYou must choose one of their available councillors at the side of the board and inserts them in the council balcony.");
		clientSocketContainer.getSocketCommunicator().send("\n"+line
				+ "\nCheck which balcony is beter for you according to your politic cards:\n"+game.getPlayers().get(playerNumber).getPoliticCards()+ "\n"
						+ line);

		clientSocketContainer.getSocketCommunicator().send("\n"+lineStar+"\n"
				+ "-The councillors in the balcony of HILL region are:\n\n"
				+ game.getBoard().getRegions(RegionType.HILL).getBalcony().getBench()+ "\n" +lineStar);
		clientSocketContainer.getSocketCommunicator().send("-The councillors in the balcony of COAST region are:\n\n"
				+ game.getBoard().getRegions(RegionType.COAST).getBalcony().getBench()+ "\n" +lineStar);
		clientSocketContainer.getSocketCommunicator().send("-The councillors in the balcony of MOUNTAIN region are:\n\n"
				+ game.getBoard().getRegions(RegionType.MOUNTAIN).getBalcony().getBench()+ "\n"+lineStar);

		clientSocketContainer.getSocketCommunicator().send("Select one of the Councillor from reserve:\n\n" + game.getBoard().getCouncillorReserve());
		Colors color = selectColor();
		clientSocketContainer.getSocketCommunicator().send("Now put the councillor chosen int one of the region balcony:");
		clientSocketContainer.getSocketCommunicator().send("\n\n\t1-HILL\n\t2-COAST\n\t3-MOUNTAIN\n");
		String regionName = selectNameRegion();
		RegionType regionType = selectRegion(regionName);
		ElectCouncillorView electCouncillorView = new ElectCouncillorView(game.getPlayers().get(playerNumber),
				game.getBoard().getRegions(regionType), game.getBoard(), color);
		MainActionController mainActionController = new MainActionController(game.getPlayers());
		IAction action = electCouncillorView.createAction(mainActionController);
		action.run();
		clientSocketContainer.getSocketCommunicator().send("You have received 4 coins!!");
	}

	// ----------------------------------------------- the methods for quick actions ---------------------------------------------------

	/**
	 * The method for quick action changeFaceUpPermitCard
	 * 
	 * @param game
	 * @param playerNumber
	 */
	public void changeFaceUpPermitCard() {
		clientSocketContainer.getSocketCommunicator()
				.send("You will return an assistant to the pool, then you will take the 2 permit card face up in a region, "
						+ "return them to the bottom of the deck and draws 2 new ones from the top of the deck,So for do it:");
		String regionName = selectNameRegion();
		RegionType regionType = selectRegion(regionName);
		ChangeFaceUpPermitCardView changeFaceUpPermitCardView = new ChangeFaceUpPermitCardView(
				game.getPlayers().get(playerNumber), game.getBoard().getRegions(regionType), game.getBoard());
		QuickActionConroller quickActionController = new QuickActionConroller(game.getPlayers());
		IAction action = changeFaceUpPermitCardView.createAction(quickActionController);
		action.run();
	}

	/**
	 * The method for quick action engageAssistant
	 * 
	 * @param game
	 * @param playerNumber
	 * @throws Exception
	 */
	public void engageAssistant() throws Exception {
		clientSocketContainer.getSocketCommunicator()
				.send("You have to pays 3 coins to take an assistant from the pool");
		clientSocketContainer.getSocketCommunicator().send("Currently you have "
				+ game.getPlayers().get(playerNumber).getCoinsTrack().getCurrentCoinsNumber() + " coins.");
		if (game.getPlayers().get(playerNumber).getCoinsTrack().getCurrentCoinsNumber() > 3) {
			EngageAssistantView engageAssistantView = new EngageAssistantView(game.getPlayers().get(playerNumber),
					game.getBoard());
			QuickActionConroller quickActionConroller = new QuickActionConroller(game.getPlayers());
			IAction action = engageAssistantView.createAction(quickActionConroller);
			action.run();
			clientSocketContainer.getSocketCommunicator()
			.send("\n------------------------------------------------------------------------------------------------------------------------"
					+ "\n                                                          ACTION IS DONE                                                \n"
					+ "------------------------------------------------------------------------------------------------------------------------\n");

			clientSocketContainer.getSocketCommunicator().send("Now you have "
					+ game.getPlayers().get(playerNumber).getCoinsTrack().getCurrentCoinsNumber() + " coins.");
			clientSocketContainer.getSocketCommunicator()
					.send("Number of your assistants: " + game.getPlayers().get(playerNumber).getAssistants().size());
		} else {
			clientSocketContainer.getSocketCommunicator()
					.send("Select another QUICK action from the 3 available action!");

			clientSocketContainer.getSocketCommunicator()
					.send("Select a Quick actions number:\n\n\t1-Change face up permit card\n\t2-Perform Additional MainAction"
							+ "\n\t3-Send assistant to elect councillor\n");
			int quickActionNumber = selectActionNumber();
			switch (quickActionNumber) {
			case 1:
				changeFaceUpPermitCard();
				break;
			case 2:
				performAdditionalMainAction();
				break;
			case 3:
				sendAssistantToElectCouncillor();
				break;
			default:

			}
		}
	}

	/**
	 * The method for quick action performAdditionalMainAction
	 * 
	 * @param game
	 * @param playerNumber
	 * @throws Exception
	 */
	public void performAdditionalMainAction() throws Exception {
		clientSocketContainer.getSocketCommunicator().send("\nyou need at least 3 assistants to execute this action.");
		clientSocketContainer.getSocketCommunicator().send(
				"\nCurrently you have " + game.getPlayers().get(playerNumber).getAssistants().size() + " Assistants.");
		if (game.getPlayers().get(playerNumber).getAssistants().size() > 3) {

			clientSocketContainer.getSocketCommunicator()
					.send("\nYou can do one MAIN action from the four available action, Select an actions number:\n");
			clientSocketContainer.getSocketCommunicator()
					.send("\t1-Acquire permit card\n\t2-Build emporium using permit card"
							+ "\n\t3-Build emporium with king help\n\t4-Elect councillors\n");
			int mainActionNumber = selectActionNumber();
			switch (mainActionNumber) {
			case 1:
				acquirePermitCard();
				break;
			case 2:
				buildEmporiumUsingPermitCard();
				break;
			case 3:
				buildEmporiumWithKingHelp();
				break;
			case 4:
				electCouncillor();
				break;
			default:
			}

		} else {
			clientSocketContainer.getSocketCommunicator()
					.send("Select another QUICK action from the 3 available action!");

			clientSocketContainer.getSocketCommunicator()
					.send("Select a Quick actions number:\n\n\t1-Change face up permit card\n\t2-Engage assistant"
							+ "\n\t3-Send assistant to elect councillor\n");
			int quickActionNumber = selectActionNumber();
			switch (quickActionNumber) {
			case 1:
				changeFaceUpPermitCard();
				break;
			case 2:
				engageAssistant();
				break;
			case 3:
				sendAssistantToElectCouncillor();
				break;
			default:
			}
		}

	}

	/**
	 * The method for quick action sendAssistantToElectCouncillor
	 * 
	 * @param game
	 * @param playerNumber
	 * @throws Exception
	 */
	public void sendAssistantToElectCouncillor() throws Exception {
		clientSocketContainer.getSocketCommunicator()
				.send("You must return one assistant to the pool, takes a councillor and inserts them in a balcony.");
		clientSocketContainer.getSocketCommunicator().send(
				"\nCurrently you have " + game.getPlayers().get(playerNumber).getAssistants().size() + " Assistants.");
		if (game.getPlayers().get(playerNumber).getAssistants().size() > 1) {
			clientSocketContainer.getSocketCommunicator()
					.send("Select the Councillor from reserve:\n" + game.getBoard().getCouncillorReserve());
			Colors color = selectColor();
			String regioName = selectNameRegion();
			RegionType regionType = selectRegion(regioName);
			SendAssistantToElectCouncillorView sendAssistantToElectCouncillorView = new SendAssistantToElectCouncillorView(
					game.getPlayers().get(playerNumber), game.getBoard().getRegions(regionType), game.getBoard(),
					color);
			QuickActionConroller quickActionConroller = new QuickActionConroller(game.getPlayers());
			IAction action = sendAssistantToElectCouncillorView.createAction(quickActionConroller);
			action.run();
		} else {
			clientSocketContainer.getSocketCommunicator()
					.send("Select another QUICK action from the 3 available action!");
			clientSocketContainer.getSocketCommunicator()
					.send("Select a Quick actions number:\n\n\t1-Change face up permit card\n\t2-Engage assistant"
							+ "\n\t3-Perform additional MainAction\n");
			int quickActionNumber = selectActionNumber();
			switch (quickActionNumber) {
			case 1:
				changeFaceUpPermitCard();
				break;
			case 2:
				engageAssistant();
				break;
			case 3:
				performAdditionalMainAction();
				break;
			default:

			}
		}
	}


	/**
	 * Metodo usato al interno del metodo selectRegion per prendere in ingresso
	 * dal utente come String il nome della regione
	 * @return string
	 */
	public String selectNameRegion() {

		clientSocketContainer.getSocketCommunicator().send("\nSelect a region:\n\n\t1-HILL\n\t2-COAST\n\t3-MOUNTAIN\n");

		return insistAndGetAnswer(input, 
				(String t) -> RegionType.valueOf(t.toUpperCase()) != null, 
				s -> s.toUpperCase());
	}

	/**
	 * the method for asking to client to select region
	 * 
	 * @return RegionType
	 */
	public RegionType selectRegion(String regionName) {
		RegionType regionType = null;
		switch (regionName) {
		case "HILL":
			regionType = RegionType.HILL;
			break;
		case "COAST":
			regionType = RegionType.COAST;
			break;
		case "MOUNTAIN":
			regionType = RegionType.MOUNTAIN;
			break;
		default:	
		}
		return regionType;
	}


	/**
	 * the method for select the color
	 * @return Colors
	 */
	public Colors selectColor() {

		return insistAndGetAnswer(input, 
				(Colors t) -> t != null && t.isPoliticCardColor(), 
				(String s) -> Colors.valueOf(s.toUpperCase()));
	}


	/**
	 * the method for asking client to select one permitCard face up
	 * @param game
	 * @param regionType
	 * @return int
	 */
	public int selectPermitcard(Model game, RegionType regionType) {
		String line = "\n..........................................................................................................................................................\n";
		clientSocketContainer.getSocketCommunicator()
				.send("\nSelect the one of the two faceUp permitcard:('0' or '1')");
		clientSocketContainer.getSocketCommunicator()
				.send(line+"0 --> "
						+ game.getBoard().getRegions(regionType).getDeckOfPermitCard().getFsceUpPermitCard().get(0));
		clientSocketContainer.getSocketCommunicator()
				.send(line+"1 --> "
						+ game.getBoard().getRegions(regionType).getDeckOfPermitCard().getFsceUpPermitCard().get(1)
						+ line);

		return insistAndGetAnswer(input, (Integer t) -> t != null && (t == 0 || t == 1), s -> Integer.valueOf(s));
	}


	/**
	 * the method for asking client to select one Main Action
	 * @return int
	 */
	public int selectActionNumber() {

		return insistAndGetAnswer(input, 
				(Integer t) -> t != null && (t  >= 1 && t <= 4), 
				s -> Integer.valueOf(s));
	}

	/**
	 * the method for asking player to select one of the permit card face up in ront of them
	 * @return int
	 * @throws Exception
	 */
	public int selectPermitcardFromPly(){
		List<Integer> idx = new ArrayList<>();
		clientSocketContainer.getSocketCommunicator()
				.send("\nSelect one of the permit card face up in front of you:\n");
		if (!game.getPlayers().get(playerNumber).getPermitCards().isEmpty()) {
			// stampo le permitcard presente per player per decidere
			for (int i = 0; i < game.getPlayers().get(playerNumber).getPermitCards().size(); i++) {
				clientSocketContainer.getSocketCommunicator()
						.send(i + "-" + game.getPlayers().get(playerNumber).getPermitCards());
				idx.add(i);
			}
		} else {
			clientSocketContainer.getSocketCommunicator().send("error: You do not have any permitcard!\n");
			clientSocketContainer.getSocketCommunicator().send("\nYou can do one MAIN action from the 3 available action, Select an actions number:\n");
			clientSocketContainer.getSocketCommunicator().send(
					"\t1-Acquire permit card\n\t" + "\n\t2-Build emporium with king help\n\t3-Elect councillors\n");
			
			int mainActionNumber = selectActionNumber();
			switch (mainActionNumber) {
			case 1:
				acquirePermitCard();
				break;
			case 2:
				buildEmporiumWithKingHelp();
				break;
			case 3:
				electCouncillor();
				break;
			default:
			}
		}
		int chosenInt = 0;
		boolean condition = true;
		while (condition) {
			try {
				clientSocketContainer.getSocketCommunicator()
						.send("\n" + String.format(PLAYER_TURN_INDICATOR, playerNumber) + "\n");
				chosenInt = readIntValue(input);
				for (int j : idx) {
					if (chosenInt == idx.get(j)) {
						condition = false;
						return chosenInt;
					}
				}

			} catch (InputMismatchException e) {
				logger.info(e.getMessage());
				clientSocketContainer.getSocketCommunicator().send("Input value is wrong! Please try again!");
			}
		}
		return chosenInt;

	}

	/**
	 * the method for asking client to doing a quick action
	 * 
	 * @return String yes or no
	 */
	public String playerAnswer() {
	
		return insistAndGetAnswer(input, (String t) -> "Y".equalsIgnoreCase(t) || "N".equalsIgnoreCase(t), s->s);
	}

	/**
	 * @param sc
	 * @return int
	 * @throws InputMismatchException
	 */
	public int readIntValue(BufferedReader sc) {
		String string = readStringValue(sc); 

		try {
			return Integer.valueOf(string);
		} catch (NumberFormatException e) {
			logger.info(e.getMessage());
			throw new InputMismatchException(e.getMessage());
		}
	}

	/**
	 * @param sc
	 * @return string
	 * @throws InputMismatchException
	 */
	public String readStringValue(BufferedReader sc) {
		String string = null;
		try {
			string = sc.readLine();
		} catch (IOException e) {
			logger.info(e.getMessage());
			throw new InputMismatchException(e.getMessage());
		}
		logger.info("Read '" + string + "' from the player " + playerNumber);
		return string;
	}
	
	
	
	
	
	
	
	
	
	
	
}