package it.polimi.ingsw.cg44.server.controller.action;

import java.io.Serializable;

/**
 * @author saeid
 *
 */
public abstract class Action implements IAction, Runnable, Serializable{

	private static final long serialVersionUID = 6612713752071508045L;

}
