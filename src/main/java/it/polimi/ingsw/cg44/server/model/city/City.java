package it.polimi.ingsw.cg44.server.model.city;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import it.polimi.ingsw.cg44.server.model.board.RegionType;
import it.polimi.ingsw.cg44.server.model.bonus.factory.IBonus;
import it.polimi.ingsw.cg44.server.model.bonus.factory.FactoryBonus;
import it.polimi.ingsw.cg44.server.model.bonus.factory.FactoryCityCardBonus;
import it.polimi.ingsw.cg44.server.model.colors.Colors;

/**
 * Abbiamo creato una sola classe city perché tanto la city del re è una city normale senza bonus,
 * quindi nel costruttore verifichiamo se il tipo di city è del re e quando lo è lasciamo la lista di Bonus vuota.
 * Useremo un'eccezione per far sapere al client che la città del Re non può avere Bonus
 * L'emporio invece è un Set perché almeno sono sicuro che non potrà contenere valori duplicati (lo stesso emporio)
 * @author saeid
 *
 */
public class City {

	private final String name;
	private final List<City> ways;
	private final Set<Emporium> emporium;
	private final RegionType region;
	private final List<IBonus> bonus;
	private final Colors color;
	
	//Protected perché così l'unico modo per creare le città è tramite il factory delle città
	protected City(String name, RegionType region, Colors color) throws Exception {
		this.name=name;
		this.ways = new ArrayList<>();
		this.region = region;
		this.emporium = new HashSet<>();
		this.color = color;
		//Se la città è del re inizializza la lista di Bonus vuota
		if(color == Colors.KING)
			this.bonus = new ArrayList<>();
		else {
			FactoryBonus factoryBonus = new FactoryCityCardBonus();
			this.bonus = factoryBonus.getBonus();
		}
	}

	//Usato solo dal factoryCity perciò protected
	protected void addBorder(City city){
		ways.add(city);
	}
	
	public void addEmporium(Emporium emporium){
		this.emporium.add(emporium);
	}
		
	public Set<Emporium> getEmporium() {
		return emporium;
	}

	public String getName() {
		return name;
	}

	public List<City> getWays() {
		return ways;
	}

	public RegionType getRegion() {
		return region;
	}
	
	public List<IBonus> getBonus() {
		return bonus;
	}
	
	public Colors getColor() {
		return color;
	}

	@Override
	public String toString() {
		return "City [name=" + name + ", ways=" + toStringBorders()  + ", region=" + region
				+  ", color=" + color + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bonus == null) ? 0 : bonus.hashCode());
		result = prime * result + ((ways == null) ? 0 : ways.hashCode());
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		result = prime * result + ((emporium == null) ? 0 : emporium.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((region == null) ? 0 : region.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		City other = (City) obj;
		if (bonus == null) {
			if (other.bonus != null)
				return false;
		} else if (!bonus.equals(other.bonus))
			return false;
		if (ways == null) {
			if (other.ways != null)
				return false;
		} else if (!ways.equals(other.ways))
			return false;
		if (color != other.color)
			return false;
		if (emporium == null) {
			if (other.emporium != null)
				return false;
		} else if (!emporium.equals(other.emporium))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (region != other.region)
			return false;
		return true;
	}


	
	/**
	 * Metodo per stampare i ways, altrimenti ci dava l'eccezione stackErrorOverflow
	 * @return
	 */
	private StringBuilder toStringBorders() {
		StringBuilder stringBuilder = new StringBuilder();
		for(City city : this.ways) {
			stringBuilder.append(city.getName());
			stringBuilder.append("  ");
		}
		return stringBuilder;
	}

}
