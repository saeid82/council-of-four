package it.polimi.ingsw.cg44.server.controller.action.quickaction;

import it.polimi.ingsw.cg44.server.controller.action.Action;
import it.polimi.ingsw.cg44.server.model.balcony.Councillor;
import it.polimi.ingsw.cg44.server.model.board.Region;
import it.polimi.ingsw.cg44.server.model.colors.Colors;
import it.polimi.ingsw.cg44.server.model.game.Board;
import it.polimi.ingsw.cg44.server.model.game.Player;

/**
 * Quick Action 3:
 * The player returns an assistant to the pool, takes a councillor and inserts them in
 * a balcony, exactly as with the main action "elect a councillor". 
 * but they do not earn any coins by performing this action.
 * @author saeid
 *
 */
public class SendAssistantToElectCouncillor extends Action {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9076467434574619397L;
	private Player player;
	private Board board;
	private transient Region region;
	private Colors colore;
	
	public SendAssistantToElectCouncillor(Player player, Board board, Region region, Colors colore) {
		this.board = board;
		this.player = player;
		this.region = region;
		this.colore = colore;
	}


	@Override
	public void run() {
		board.getAssistantsReserve().getAssistants().add(player.getAssistants().poll());
		
		Councillor selectedCouncillor =  board.getCouncillorReserve().getCouncillorByColor(colore);
		region.getBalcony().electCouncillor(board.getCouncillorReserve(), selectedCouncillor);
		
	}

}
