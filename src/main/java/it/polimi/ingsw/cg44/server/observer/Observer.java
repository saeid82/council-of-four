package it.polimi.ingsw.cg44.server.observer;

public interface Observer{
	
	public void update();
}
