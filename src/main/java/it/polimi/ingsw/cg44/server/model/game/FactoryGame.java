package it.polimi.ingsw.cg44.server.model.game;



import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.cg44.server.model.assistant.AssistantsReserve;
import it.polimi.ingsw.cg44.server.model.city.City;
import it.polimi.ingsw.cg44.server.model.city.FactoryCity;
import it.polimi.ingsw.cg44.server.model.map.Map;
import it.polimi.ingsw.cg44.server.model.track.NobilityTrack;


/**
 * Factory pattern
 * @author saeid
 *
 */
public class FactoryGame implements IGame {

	@Override
	public Board createBoard(String file, int numberPlayers) throws Exception {
		
		BufferedReader bufferedReader = Map.readFile(file);
		FactoryCity factoryCity = new FactoryCity();
		List<City> cities = factoryCity.initializeCities(bufferedReader);

		return new Board(cities, numberPlayers);
	}

	@Override
	public List<Player> createPlayers(NobilityTrack nobilityTrack, AssistantsReserve assistantsReserve, int numberPlayers) throws Exception {
		
		List<Player> players = new ArrayList<>();
		for(int i=0; i<numberPlayers; i++)
			players.add(createPlayer(nobilityTrack, assistantsReserve, numberPlayers));
		
		return players;
	}

	public Player createPlayer(NobilityTrack nobilityTrack, AssistantsReserve assistantsReserve, int numberPlayers)
			throws Exception {
		return new Player(nobilityTrack, assistantsReserve, numberPlayers);
	}
	
	
}

