package it.polimi.ingsw.cg44.server.model.track;


import java.util.SortedSet;
import java.util.TreeSet;

import it.polimi.ingsw.cg44.server.model.bonus.SpaceBonus;

/**
 * @author saeid
 *
 */
public class NobilityTrack {
	
	private final SortedSet<SpaceBonus> track;
	private static final int NOBILITY_TRACK_LENGHT = 20;
	
	/**
	 * Il percorso è un SortedSet di SpaceBonus implementato come TreeSet.
	 * (usiamo un TreeSet perchè non c'è più bisogno di scorrere elementi in ordine inverso)
	 * @throws Exception
	 */
	public NobilityTrack() {
		track = new TreeSet<>();
		//Per la prima casella uso un costruttore senza parametro che mi da la certezza che non ci siano bonus in quella casella.
		track.add(new SpaceBonus());
		for(int i = 1; i < NOBILITY_TRACK_LENGHT; i++){
			track.add(new SpaceBonus(i));
		}
		
	}
	
	public SortedSet<SpaceBonus> getTrack() {
		return track;
	}

	@Override
	public String toString() {
		return "NobilityTrack --> track=\n " + track + "";
	}

}

