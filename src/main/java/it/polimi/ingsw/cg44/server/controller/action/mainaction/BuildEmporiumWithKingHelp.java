package it.polimi.ingsw.cg44.server.controller.action.mainaction;

import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.cg44.server.controller.action.Action;
import it.polimi.ingsw.cg44.server.model.balcony.Councillor;
import it.polimi.ingsw.cg44.server.model.board.RegionType;
import it.polimi.ingsw.cg44.server.model.card.politiccard.PoliticCard;
import it.polimi.ingsw.cg44.server.model.city.City;
import it.polimi.ingsw.cg44.server.model.colors.Colors;
import it.polimi.ingsw.cg44.server.model.game.Board;
import it.polimi.ingsw.cg44.server.model.game.Player;
import it.polimi.ingsw.cg44.server.model.king.King;

/**
 * Action 4:
 * The player must satisfy the King's council following the same rules of "AcquirePermitCard".
 * Move the King to the city of their choice, The King must use uninterrupted roads to make
 * the journey. The player pays 2 coins for each road traveled.
 * the King may also be left in the same city, in this case the player pays no money.
 * the player immediately build an emporium in the space where the King is located at
 * the end of his journey. 
 * @author saeid
 *
 */
public class BuildEmporiumWithKingHelp extends Action {

	/**
	 * 
	 */
	private static final long serialVersionUID = 519420213359224231L;
	private Player player;
	private Board board;
	private City nextCity;

	/**
	 * @param player
	 * @param board
	 * @param politicCards
	 * @param numCardToDraw
	 */
	public BuildEmporiumWithKingHelp(Player player, Board board, City nextCity) {
		this.player = player;
		this.board = board;
		this.nextCity = nextCity;
	}

	@Override
	public void run() {

		SatisfyCouncillor satisfyCouncillor = new SatisfyCouncillor(player, board.getKingRegion(), player.getPoliticCards());
		satisfyCouncillor.satisfy();
		
		// Move the King to the city of their choice.
		King king = new King();
		king.moving(nextCity);
		
		// pays 2 coins for each road traveled.
		player.getCoinsTrack().setCoinsNumber(-2);
		
		// build an emporium in the space where the King is located at the end of his journey.
		nextCity.getEmporium().add(player.getEmporiums().poll());
		
	}
	public Board getBoard() {
		return board;
	}
}
