package it.polimi.ingsw.cg44.server.model.track;



import java.util.Scanner;
import java.util.logging.Logger;

/**
 * @author saeid
 *
 */
public class CoinsTrack {

	private int currentCoinsNumber;

	public final int MAX_COINS_NUMBER;
	public static final int INFERIOR_COINS_START_THRESHOLD = 9;
	
	/**
	 * @param playerID
	 * @param numberPlayers
	 */
	public CoinsTrack(int playerID, int numberPlayers){
		this.currentCoinsNumber = INFERIOR_COINS_START_THRESHOLD + playerID;
		this.MAX_COINS_NUMBER = calculateMaxCoins(numberPlayers);
	}

	/**
	 * The method for get the current coins number
	 * @return
	 */
	public int getCurrentCoinsNumber() {
		return currentCoinsNumber;
	}
	

	/**
	 * @param coinsMove
	 * @return
	 */
	public boolean setCoinsNumber(int coinsMove){
		if(currentCoinsNumber + coinsMove > MAX_COINS_NUMBER){
			Scanner scanner = new Scanner(System.in);
			Logger logger = Logger.getLogger(CoinsTrack.class.getName());
			logger.info("You are exaggerating the max number of coins allowed, do you want to proced anyway? [y/n]");
			String scelta = scanner.nextLine();
			if(scelta.equals("y")){
				this.currentCoinsNumber = MAX_COINS_NUMBER;
				scanner.close();
				return true;
			}
			else{
				scanner.close();
				throw new IllegalArgumentException();
			}		
		}
		
		if(currentCoinsNumber - coinsMove < 0){
			throw new IndexOutOfBoundsException("Impossible execute the operation due to leakness of coins");
		}
		else{
			this.currentCoinsNumber += coinsMove;
			return true;
		}
	}
	
	
	/**
	 * Metodo per calcolare la lunghezza del CoinsTrack in base al numero dei giocatori 
	 * Implementato tenendo il rapporto che danno quelli della Cranio
	 * @param numberPlayers
	 * @return
	 */
	private int calculateMaxCoins(int numberPlayers) {
		int standardQuantityCoins = 20;
		if(numberPlayers<5)
			return standardQuantityCoins;
		else
			return standardQuantityCoins + (numberPlayers - 4);
	}

	@Override
	public String toString() {
		return "CoinsTrack (currentCoinsNumber= " + currentCoinsNumber + ", MAX_COINS_NUMBER= " + MAX_COINS_NUMBER + ")";
	}
	
}

