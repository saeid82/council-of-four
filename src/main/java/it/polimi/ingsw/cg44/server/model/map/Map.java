package it.polimi.ingsw.cg44.server.model.map;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author saeid
 *
 */
public class Map {
	
	/**
     * Suppress the default constructor for non instantiability (Effective Java
     * -
     * Item 4).
    */
	private Map(){
		throw new AssertionError();
	}
	
	public static BufferedReader readFile(String file) {
		try {
			return new BufferedReader(new FileReader(file));
			
		} catch (FileNotFoundException e) {
			System.out.println("File not find!");
			System.out.println(e.getMessage());
			return null;
		} 
	}
	
	public static List<String> readLines(BufferedReader bufferedReader, String startLine, String endLine) throws IOException {
		
		List<String> cached = new ArrayList<>();
		
		while(!bufferedReader.readLine().equals(startLine));

		for(String currentLine = bufferedReader.readLine(); !currentLine.equals(endLine); currentLine = bufferedReader.readLine()) {
			cached.add(currentLine);
		}
		
		return cached;
	}

}
