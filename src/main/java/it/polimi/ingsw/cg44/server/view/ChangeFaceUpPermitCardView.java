package it.polimi.ingsw.cg44.server.view;

import it.polimi.ingsw.cg44.server.controller.Controller;
import it.polimi.ingsw.cg44.server.controller.QuickActionConroller;
import it.polimi.ingsw.cg44.server.controller.action.IAction;
import it.polimi.ingsw.cg44.server.model.board.NormalRegion;
import it.polimi.ingsw.cg44.server.model.game.Board;
import it.polimi.ingsw.cg44.server.model.game.Player;

public class ChangeFaceUpPermitCardView extends View {

	private NormalRegion targetRegion;
	
	public ChangeFaceUpPermitCardView(Player player, NormalRegion normalRegion, Board board) {
		super(player, board);
		this.targetRegion = normalRegion;
	}

	public NormalRegion getTargetRegion() {
		return targetRegion;
	}

	@Override
	public IAction createAction(Controller quickActionController) {
		if(isValid())
			return ((QuickActionConroller) quickActionController).update(this);
		else
			return null;
	}
	
	@Override
	protected boolean isValid(){
		if(targetRegion.getDeckOfPermitCard().getDeck().size() < 2){
			System.out.println("There aren't enough card in the deck to execute the action");
			return false;
		}
		if(getPlayer().getAssistants().isEmpty()){
			System.out.println("you need at least 1 assistants to execute this action.");
			return false;
		}
		else return true;
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}

}
