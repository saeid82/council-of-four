package it.polimi.ingsw.cg44.server.model.city;


import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.cg44.server.model.board.RegionType;
import it.polimi.ingsw.cg44.server.model.colors.Colors;
import it.polimi.ingsw.cg44.server.model.map.Map;



/**
 * @author saeid
 *
 */
public class FactoryCity {

	public List<City> initializeCities(BufferedReader bufferedReader) throws Exception {
		
		List<City> cities = new ArrayList<>();

		try {
			List<String> infoCities = Map.readLines(bufferedReader, "START_CITY_INITIALIZATION", "END_CITY_INITIALIZATION");
			for(String string : infoCities) {
				String[] infoCity = string.split(",");
				cities.add(new City(infoCity[0], RegionType.valueOf(infoCity[1]), Colors.valueOf(infoCity[2])));
			}
			createConnection(cities, bufferedReader);
		} catch (IOException e) {
			System.out.println("Error reading cities");
			System.out.println(e.getMessage());
		}
		return cities;
	}
	/**
	 * Gestisce la creazione delle connessioni invocando altri metodi privati
	 * @param cities
	 * @param bufferedReader
	 * @throws Exception 
	 */
	private void createConnection(List<City> cities, BufferedReader bufferedReader) throws Exception {
		
		try {
			List<String> infoConnection = Map.readLines(bufferedReader, "START_CONNECTION", "END_CONNECTION");
			for(String string : infoConnection) {
				String[] connections = string.split(",");
				City city = findCity(cities, connections[0]);
				addConnections(cities, city, connections);
			}
		} catch (IOException e) {
			System.out.println("Error reading cities' connection");
			System.out.println(e.getMessage());
		}
	}
	/**
	 * Crea le connessioni chiamando il metodo che verifica se sono valide
	 * @param cities
	 * @param cityToBeConnected
	 * @param connections
	 */
	private void addConnections(List<City> cities, City cityToBeConnected, String[] connections) throws Exception {
		for(int i=1; i<connections.length; i++) {
			try{
				if(checkConnections(cityToBeConnected, connections[i], cities))  
					cityToBeConnected.addBorder(findCity(cities, connections[i]));
			}
			catch(Exception e){
				System.out.println("Not a valid connection between " + cityToBeConnected.getName() + " and " + connections[i]);
				System.out.println(e.getMessage());
				
			}
		}
	}
	/**
	 * Controlla se la connessione tra le due città è fattibile
	 * @param city
	 * @param name
	 * @param cities
	 * @return
	 */
	private boolean checkConnections(City city, String name, List<City> cities) {
		City otherCity = findCity(cities, name);
		if(city.getRegion()==RegionType.MOUNTAIN && otherCity.getRegion()==RegionType.COAST ||
				city.getRegion()==RegionType.COAST && otherCity.getRegion()==RegionType.MOUNTAIN)
			return false;
		return true;
	}
	/**
	 * @param cities
	 * @param name
	 * @return
	 */
	private City findCity(List<City> cities, String name) {
		for(City city : cities) {
			if(city.getName().equals(name))
				return city;
		}
		return null;
	}
}
