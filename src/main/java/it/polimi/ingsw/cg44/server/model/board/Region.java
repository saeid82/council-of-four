package it.polimi.ingsw.cg44.server.model.board;

import it.polimi.ingsw.cg44.server.model.balcony.Balcony;

/**
 * (Board(Region(Balcony(Councillor))))
 * We have 2 types region:
 * 	1- NormalREgion
 * 	2- KingRegion
 * @author saeid
 *
 */
public abstract class Region {
	
	private Balcony balcony;

	/**
	 * Each Region contain a balcony and it is within a board.
	 * @param balcony
	 */
	public Region() {
		balcony = new Balcony();
	}

	public Balcony getBalcony() {
		return balcony;
	}
	
	

}
