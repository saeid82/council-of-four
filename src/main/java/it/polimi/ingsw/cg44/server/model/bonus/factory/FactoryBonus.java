package it.polimi.ingsw.cg44.server.model.bonus.factory;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import it.polimi.ingsw.cg44.server.model.card.permitcard.IQuantity;


/**
 * @author saeid
 *
 */
public abstract class FactoryBonus implements IQuantity {
	private static final String PACKAGE = "it.polimi.ingsw.cg44.server.model.bonus.";
	protected final Map<Integer, String> bonusClasses;
	
	/**
	 * 
	 */
	public FactoryBonus(){
		this.bonusClasses = new TreeMap<>();
		bonusClasses.put(0, PACKAGE+"NobiltyBonus");
		bonusClasses.put(1, PACKAGE+"PoliticCardBonus");
		bonusClasses.put(2, PACKAGE+"ActionBonus");
		bonusClasses.put(3, PACKAGE+"AssistantsBonus");
		bonusClasses.put(4, PACKAGE+"VictoryPointBonus");
		bonusClasses.put(5, PACKAGE+"CoinsBonus");
		bonusClasses.put(6, PACKAGE+"RewardTokenOfCityBonus");
		bonusClasses.put(7, PACKAGE+"BoughtPermitCardBonus");
		bonusClasses.put(8, PACKAGE+"PermitCardBonus");
	}
	

	public abstract List<IBonus> getBonus();
	
	
	/**
	 * Controlla se il percorso del bonus che ho scelto randomicamente è uguale a uno di quelli già nella lista
	 * @param bonusToAdd
	 * @param addedBonus
	 * @return
	 */
	protected boolean conditionToAddBonus(Class<?> bonusToAdd, List<IBonus> addedBonus){
		for(IBonus bonus : addedBonus) {
			if(bonusToAdd == bonus.getClass()) {
				return true;
			}
		}
		return false;
	}

}
