package it.polimi.ingsw.cg44.server.controller.action;

import java.util.List;

import it.polimi.ingsw.cg44.server.controller.action.mainaction.AcquirePermitCard;
import it.polimi.ingsw.cg44.server.controller.action.mainaction.BuildEmporiumUsingPermitCard;
import it.polimi.ingsw.cg44.server.controller.action.mainaction.BuildEmporiumWithKingHelp;
import it.polimi.ingsw.cg44.server.controller.action.mainaction.ElectCouncillor;
import it.polimi.ingsw.cg44.server.controller.action.quickaction.ChangeFaceUpPermitCard;
import it.polimi.ingsw.cg44.server.controller.action.quickaction.EngageAssistant;
import it.polimi.ingsw.cg44.server.controller.action.quickaction.PerformAdditionalMainAction;
import it.polimi.ingsw.cg44.server.controller.action.quickaction.SendAssistantToElectCouncillor;
import it.polimi.ingsw.cg44.server.model.board.NormalRegion;
import it.polimi.ingsw.cg44.server.model.board.Region;
import it.polimi.ingsw.cg44.server.model.card.permitcard.PermitCard;
import it.polimi.ingsw.cg44.server.model.card.politiccard.PoliticCard;
import it.polimi.ingsw.cg44.server.model.city.City;
import it.polimi.ingsw.cg44.server.model.colors.Colors;
import it.polimi.ingsw.cg44.server.model.game.Board;
import it.polimi.ingsw.cg44.server.model.game.Player;

/**
 * Factory pattern
 * for each action there is one method.
 * @author saeid
 *
 */
public class ActionFactory {
	/**
	 * create AcquirePermiCard action
	 * @param board
	 * @param player
	 * @param normalRegion
	 * @param politicCards
	 * @param numCardToDraw
	 * @return new AcquirePermiCard
	 */
	public IAction getAcquirePermiCardAction(Board board, Player player, NormalRegion normalRegion, List<PoliticCard> politicCards,
			int numCardToDraw){
		return new AcquirePermitCard(board, player, normalRegion, politicCards, numCardToDraw);
	}

	/**
	 * Create BuildEmporiumUsingPermitCard action
	 * @param player
	 * @param board
	 * @param city
	 * @param selectedPermitCard
	 * @return new BuildEmporiumUsingPermitCard
	 */
	public IAction getBuildEmporiumUsingPermitCardAction(Player player, Board board, City city, PermitCard selectedPermitCard){
		return new BuildEmporiumUsingPermitCard(player, board, city, selectedPermitCard);
	}

	/**
	 * Create BuildEmporiumWithKingHelp action
	 * @param player
	 * @param board
	 * @param nextCity
	 * @return new BuildEmporiumWithKingHelp
	 */
	public IAction getBuildEmporiumWithKingHelpAction(Player player, Board board, City nextCity){
		return new BuildEmporiumWithKingHelp(player, board, nextCity);
	}
	
	/**
	 * create ElectCouncillor action
	 * @param player
	 * @param board
	 * @param region
	 * @param colore
	 * @return new ElectCouncillor
	 */
	public IAction getElectCouncillorAction(Player player, Board board, Region region, Colors colore){
		return new ElectCouncillor(player, board, region, colore);
	}
	
	/**
	 * Create ChangeFaceUpPermitCard action
	 * @param player
	 * @param board
	 * @param region
	 * @return new ChangeFaceUpPermitCard
	 */
	public IAction getChangeFaceUpPermitCardAction(Player player, Board board, NormalRegion region){
		return new ChangeFaceUpPermitCard(player, board, region);
	}

	/**
	 * create EngageAssistant action
	 * @param player
	 * @param board
	 * @return new EngageAssistant
	 */
	public IAction getEngageAssistantAction(Player player, Board board){
		return new EngageAssistant(player, board);
	}
	/**
	 * create PerformAdditionalMainAction action
	 * @param player
	 * @param board
	 * @return new PerformAdditionalMainAction
	 */
	public IAction getPerformAdditionalMainActionAction(Player player, Board board){
		return new PerformAdditionalMainAction(player, board);
	}
	/**
	 * Create SendAssistantToElectCouncillor action
	 * @param player
	 * @param board
	 * @param region
	 * @param colore
	 * @return new SendAssistantToElectCouncillor
	 */
	public IAction getSendAssistantToElectCouncillor(Player player, Board board, Region region, Colors colore){
		return new SendAssistantToElectCouncillor(player, board, region, colore);
	}


}
