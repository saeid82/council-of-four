package it.polimi.ingsw.cg44.server.model.bonus;

import it.polimi.ingsw.cg44.server.model.bonus.factory.IBonus;
import it.polimi.ingsw.cg44.server.model.game.Board;
import it.polimi.ingsw.cg44.server.model.game.Player;

/**
 * The bonus for move the marker disc one step along the nobility track 
 * @author saeid
 *
 */
public class NobiltyBonus extends Bonus {
	

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8185089485790810863L;

	@Override
	public void useBonus(Player player, Board board) throws Exception {
		SpaceBonus currentSpace = player.getMarkerDisk().nextCurrentSpace();
		for(IBonus bonus : currentSpace.getBonus()) {
			bonus.useBonus(player, board);
			System.out.println("You used the bonus: " + bonus);
		}
	}

	@Override
	public String toString() {
		return "(NobilityBonus)";
	}

}
