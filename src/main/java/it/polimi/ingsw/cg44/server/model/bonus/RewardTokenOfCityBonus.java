package it.polimi.ingsw.cg44.server.model.bonus;

import it.polimi.ingsw.cg44.server.model.game.Board;
import it.polimi.ingsw.cg44.server.model.game.Player;



/**
 * Reward token of city in which you have an emporium.
 * @author saeid
 *
 */
public class RewardTokenOfCityBonus extends Bonus {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3788591915722933439L;
	private final int quantity;
	private static final int MAX_QUANTITY_CITY = 5;
	
	public RewardTokenOfCityBonus() {
		this.quantity=(int) (Math.random()*MAX_QUANTITY_CITY + 1);
		//ho aggiunto + 1 nella funzione randomica per evitare che si generino bonus con quantità nulla
	}

	public int getQuantity() {
		return quantity;
	}

	@Override
	public void useBonus(Player player, Board board) {
	}

	@Override
	public String toString() {
		return "CityTokenBonus(quantity= " + quantity + ")";
	}

}
