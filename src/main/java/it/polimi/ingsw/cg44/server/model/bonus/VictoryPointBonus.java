package it.polimi.ingsw.cg44.server.model.bonus;

import it.polimi.ingsw.cg44.server.model.game.Board;
import it.polimi.ingsw.cg44.server.model.game.Player;

/**
 * @author saeid
 *
 */
public class VictoryPointBonus extends Bonus {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2749094533572971118L;
	private final int point;
	private static final int MAX_POINT_VICTORY = 5;
	
	public VictoryPointBonus() {
		this.point=(int) (Math.random()*MAX_POINT_VICTORY + 1);
		//ho aggiunto + 1 nella funzione randomica per evitare che si generino bonus con quantità nulla
	}

	public int getQuantity() {
		return point;
	}

	@Override
	public void useBonus(Player player, Board board) {
		player.getVictoryTrack().setCurrentVictoryPoints(point);
	}

	@Override
	public String toString() {
		return "VictoryBonus(quantity= " + point + ")";
	}
}
