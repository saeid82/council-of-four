package it.polimi.ingsw.cg44.server.model.assistant;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Assistants reserve, by default there are 30 assistants at the beginning of the game.
 * @author saeid
 *
 */
public class AssistantsReserve  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5498563309590074099L;
	private transient final Queue<Assistant> assistants;

	public AssistantsReserve(int numPlayers) {
		this.assistants = assisResAtBeginning(numPlayers);
	}
	
	/**
	 * The method to create assistant at the beginning of the game.
	 * @param numPlayers is the number of players
	 * @return assistants
	 */
	private Queue<Assistant> assisResAtBeginning(int numPlayers){
		Queue<Assistant> assistantsPlayer = new LinkedList<>();
		
		int assisQty = calcQtyAssis(numPlayers);
		
		for (int i = 0; i < assisQty; i++) {
			assistantsPlayer.add(new Assistant());
		}
		return assistantsPlayer;
		

			
	}
	
	/**
	 * Calculate assistants number at the beginning of the game.
	 * @param numPlayers is the number of players
	 * @return number of assistants in base a players number, by default is 30.
	 */
	public int calcQtyAssis(int numPlayers){
		int defaultQtyAssis = 0;
		
		if(numPlayers < 5)
			return defaultQtyAssis = 30;
		else
			for(int i=0; i <= numPlayers ; i++){
				defaultQtyAssis +=1;
			}
		return defaultQtyAssis + (numPlayers * 5);
	}

	
	
	public Queue<Assistant> getAssistants() {
		return assistants;
	}

	
	
	@Override
	public String toString() {
		return "AssistantReserve " + assistants.size();
	}
	
	
	

}
