package it.polimi.ingsw.cg44.server.model.board;

import java.util.EnumMap;
import java.util.LinkedList;
import java.util.Queue;

import it.polimi.ingsw.cg44.server.model.bonus.BonusKingCard;
import it.polimi.ingsw.cg44.server.model.colors.Colors;



/**
 * KingRegion contains:
 * 5 BonusKingCard within deckOfKingCard, and 
 * 4 BonusKingCard within deckOfCityColorBonus.
 * @author saeid
 *
 */
public class KingRegion extends Region {
	
	public static final int MAX_KINGCARD_NUMBER = 5;
	private final Queue<BonusKingCard> deckOfKingCard;
	private final EnumMap<Colors, BonusKingCard> deckOfCityColorBonus;
	
	
	public KingRegion(){
		super();
		
		deckOfKingCard = new LinkedList<>();
		for(int i = 0; i < MAX_KINGCARD_NUMBER; i++)
			deckOfKingCard.add(new BonusKingCard());
		
		deckOfCityColorBonus = new EnumMap<>(Colors.class);
		deckOfCityColorBonus.put(Colors.BRONZE, new BonusKingCard());
		deckOfCityColorBonus.put(Colors.SILVER, new BonusKingCard());
		deckOfCityColorBonus.put(Colors.GOLD, new BonusKingCard());
		deckOfCityColorBonus.put(Colors.IRON, new BonusKingCard());
	}
	

	public BonusKingCard drawKingCard() {
		if(!deckOfKingCard.isEmpty())
			return deckOfKingCard.poll();
		
		else {
			System.out.println("Mazzo di carte del re esaurito, impossibile pescare una carta!");
			return null;
		}
	}

	public Queue<BonusKingCard> getDeckOfKingCard() {
		return deckOfKingCard;
	}

	public BonusKingCard getDeckOfCityColorBonus(Colors color) {
		return deckOfCityColorBonus.remove(color);
	}

	@Override
	public String toString() {
		return "KingRegion (deckOfCityColorBonus= " + deckOfCityColorBonus + " la prima del mazzo " + getDeckOfKingCard().peek() + " )";
	}

}
