package it.polimi.ingsw.cg44.server.controller;

import java.util.List;

import it.polimi.ingsw.cg44.server.controller.action.IAction;
import it.polimi.ingsw.cg44.server.controller.action.quickaction.ChangeFaceUpPermitCard;
import it.polimi.ingsw.cg44.server.controller.action.quickaction.EngageAssistant;
import it.polimi.ingsw.cg44.server.controller.action.quickaction.PerformAdditionalMainAction;
import it.polimi.ingsw.cg44.server.controller.action.quickaction.SendAssistantToElectCouncillor;
import it.polimi.ingsw.cg44.server.model.game.Player;
import it.polimi.ingsw.cg44.server.view.ChangeFaceUpPermitCardView;
import it.polimi.ingsw.cg44.server.view.EngageAssistantView;
import it.polimi.ingsw.cg44.server.view.PerformAnotherMainActionView;
import it.polimi.ingsw.cg44.server.view.SendAssistantToElectCouncillorView;

public class QuickActionConroller extends Controller {
	
	public QuickActionConroller(List<Player> players) {
		super(players);
		// TODO Auto-generated constructor stub
	}
	
	public IAction update(EngageAssistantView messageRqst) {
		return new EngageAssistant(messageRqst.getPlayer(), messageRqst.getBoard());
	}
	
	public IAction update(ChangeFaceUpPermitCardView messageRqst) {
		return new ChangeFaceUpPermitCard(messageRqst.getPlayer(), messageRqst.getBoard(), messageRqst.getTargetRegion());
	}
	
	public IAction update(PerformAnotherMainActionView messageRqst) {
		return new PerformAdditionalMainAction(messageRqst.getPlayer(), messageRqst.getBoard());
	}
	
	public IAction update(SendAssistantToElectCouncillorView messageRqst) {
		return new SendAssistantToElectCouncillor(messageRqst.getPlayer(), messageRqst.getBoard(), 
				messageRqst.getTargetRegion(), messageRqst.getColore());
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}


}
