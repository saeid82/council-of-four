package it.polimi.ingsw.cg44.server.view;

import it.polimi.ingsw.cg44.server.controller.Controller;
import it.polimi.ingsw.cg44.server.controller.MainActionController;
import it.polimi.ingsw.cg44.server.controller.action.IAction;
import it.polimi.ingsw.cg44.server.model.city.City;
import it.polimi.ingsw.cg44.server.model.game.Board;
import it.polimi.ingsw.cg44.server.model.game.Player;

public class BuildEmporiumWithKingHelpView extends View {

	private City nextCity;

	public BuildEmporiumWithKingHelpView(Player player, Board board, City nextCity) {
		super(player, board);
		this.nextCity = nextCity;
	}

	@Override
	public IAction createAction(Controller mainActionController) {
		return ((MainActionController) mainActionController).update(this);
	}
	
	public City getCity() {
		return nextCity;
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected boolean isValid() {
		// TODO Auto-generated method stub
		return false;
	}
	

}
