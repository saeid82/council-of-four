package it.polimi.ingsw.cg44.server.model.city;

import it.polimi.ingsw.cg44.server.model.game.Player;

/**
 * @author saeid
 *
 */
public class Emporium {

	private final Player player;
	
	public Emporium(Player player) {
		this.player = player;
	}

	/**
	 * Invece di ritornare il giocatore, ritorniamo solo il suo id che è univoco nella partita
	 * @return ID
	 */
	public int getPlayerId() {
		return player.getId();
	}

	@Override
	public String toString() {
		return "Emporium ";
	}
	
}
