package it.polimi.ingsw.cg44.server.model.king;

import java.io.Serializable;

import it.polimi.ingsw.cg44.server.model.city.City;

/**
 * @author saeid
 *
 */
public class King implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4714029102930701635L;
	private City currentCity;

	public City getCity() {
		return currentCity;
	}

	public void moving(City nextCity) {
		if(checkMoving(nextCity))
			this.currentCity = nextCity;
		else
			throw new NullPointerException("King can't move to selected city");
	}
	
	
	/**
	 * Metodo che controlla se il re può muoversi nella città scelta dall'utente
	 * @param nextCity
	 * @return
	 */
	private boolean checkMoving(City nextCity) {
		if(this.currentCity==null)
			return true;
		for(City city : this.currentCity.getWays()) 
			if(city.equals(nextCity))
				return true;
		throw new NullPointerException("OoOps!!");
	}

	@Override
	public String toString() {
		return "King [city=" + currentCity + "]";
	}
	
}

