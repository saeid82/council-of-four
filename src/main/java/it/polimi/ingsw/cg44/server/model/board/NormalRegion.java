package it.polimi.ingsw.cg44.server.model.board;

import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.cg44.server.model.bonus.BonusKingCard;
import it.polimi.ingsw.cg44.server.model.card.permitcard.DeckOfPermitCard;
import it.polimi.ingsw.cg44.server.model.city.City;

/**
 * NormalRegion could be one of the Seaside cities , Hillside cities , Mountain cities.
 * There are 7 different bonus cards:
 * Silver cities , GOld cities , Bronze cities , Iron cities
 * Seaside cities , Hillside cities , Mountain cities
 * @author saeid
 *
 */
public class NormalRegion extends Region {
		
	/**
	 * Could be one of the Seaside cities , Hillside cities , Mountain cities.
	 */
	private BonusKingCard bonusRegionCard;
	private DeckOfPermitCard deckOfPermitCard;
	private RegionType regionType;
	private final List<City> cities;
	
	/**
	 * The constructor instants also bonus region card linked to the region.
	 * @param numberPlayers : to create enough cards, depending on the number of players.
	 * @param regionType
	 * @param cities
	 * @throws Exception 
	 */
	public NormalRegion(RegionType regionType, List<City> cities, int numberPlayers) throws Exception {
		
		super();
		this.regionType = regionType;
		this.deckOfPermitCard = new DeckOfPermitCard(cities, numberPlayers);
		this.cities = selectCity(cities, regionType);
		this.bonusRegionCard = new BonusKingCard();
	}

	public List<City> getCity() {
		return cities;
	}
	
	public BonusKingCard getBonusRegionCard() {
		if(!bonusRegionCard.isUsed())
			return bonusRegionCard;
		else
			return null;
	}

	public DeckOfPermitCard getDeckOfPermitCard() {
		return deckOfPermitCard;
	}

	public List<City> getCities() {
		return cities;
	}

	public RegionType getRegionType() {
		return regionType;
	}


	private List<City> selectCity(List<City> cities, RegionType regionType) {
		List<City> citiesRegion = new ArrayList<>();
		for(City city : cities) {
			if(city.getRegion()==regionType)
				citiesRegion.add(city);
		}
		return citiesRegion;
	}

	@Override
	public String toString() {
		return "NormalRegion [city=" + cities + ", regionType=" + regionType + "]";
	}

}
