package it.polimi.ingsw.cg44.server.controller.action.quickaction;

import it.polimi.ingsw.cg44.server.controller.action.Action;
import it.polimi.ingsw.cg44.server.model.game.Board;
import it.polimi.ingsw.cg44.server.model.game.Player;

/**
 * Quick Action 4:
 * The player return 3 assistants to the pool and perform 2 main action instead of one in this
 * turn(it is also possible to perform the same action twice.)
 * @author saeid
 *
 */
public class PerformAdditionalMainAction extends Action {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6193095051314093705L;
	private transient Player player;
	private Board board;
	
	public PerformAdditionalMainAction(Player player, Board board) {
		this.board = board;
		this.player = player;
	}
	

	@Override
	public void run() {

		//return 3 assistants
		for(int i = 0; i < 3; i++)
			board.getAssistantsReserve().getAssistants().add(player.getAssistants().poll());
		
	}

}
