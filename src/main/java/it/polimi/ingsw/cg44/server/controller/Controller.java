package it.polimi.ingsw.cg44.server.controller;

import java.util.List;

import it.polimi.ingsw.cg44.server.model.game.Player;
import it.polimi.ingsw.cg44.server.observer.Observer;


/**
 * MVC pattern
 * @author saeid
 *
 */
public abstract class Controller implements Observer {
	
	private final List<Player> players;

	/**
	 * @param players
	 */
	public Controller(List<Player> players) {
		super();
		this.players = players;
	}

	public List<Player> getPlayers() {
		return players;
	}




}
