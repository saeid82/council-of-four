package it.polimi.ingsw.cg44.server.model.bonus;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import it.polimi.ingsw.cg44.server.model.bonus.factory.IBonus;
import it.polimi.ingsw.cg44.server.model.card.permitcard.PermitCard;
import it.polimi.ingsw.cg44.server.model.game.Board;
import it.polimi.ingsw.cg44.server.model.game.Player;

/**
 * Receive the bonus of one of the permit card which you previously bought(also a face down card)
 * @author saeid
 *
 */
public class BoughtPermitCardBonus extends Bonus {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -7290294149103413490L;
	private static Scanner input;

	@Override
	public void useBonus(Player player, Board board) throws Exception {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Choose one of the permit card face up in front of you:");
		if(!player.getPermitCards().isEmpty()){
			for(PermitCard permitCard : player.getPermitCards())
				System.out.println(permitCard);
		}else{
			extracted();
		}
		int selectedPermitCard = scanner.nextInt();
		for(IBonus bonus : player.getPermitCards().get(selectedPermitCard).getBonus())
			bonus.useBonus(player, board);

		scanner.close();
		
	}
	
	public static int selectPermitcardFromPly(Player player) throws Exception{
		List<Integer> idx = new ArrayList<>();
		System.out.println("\nSelect one of the permit card face up in front of you:\n");
		if(!player.getPermitCards().isEmpty()){
			// stampo le permitcard presente per player per decidere
			for(int i=0; i<player.getPermitCards().size() ; i++){
				System.out.println(i+"-"+player.getPermitCards());
				idx.add(i);
			}
		}else {
			System.out.println("error: You do not have any permitcard!\n");
		}
		int choose = 0;
		boolean condition = true;
		while (condition) {
			try {
				choose = input.nextInt();
				for(int j:idx){
					if(choose == idx.get(j)){
						condition = false;
						return choose;
					}
				}

			} catch (InputMismatchException e) {
				input.nextLine();
			}
		}
		return choose;
	
	}

	private void extracted() throws Exception {
		throw new Exception("You don't have any permitCard!");
	}
	
	@Override
	public String toString() {
		return "BoughtPermitCardBonus";
	}
}
