package it.polimi.ingsw.cg44.server.model.bonus;

import java.util.InputMismatchException;
import java.util.Scanner;

import it.polimi.ingsw.cg44.server.controller.MainActionController;
import it.polimi.ingsw.cg44.server.controller.action.Action;
import it.polimi.ingsw.cg44.server.controller.action.IAction;
import it.polimi.ingsw.cg44.server.model.Model;
import it.polimi.ingsw.cg44.server.model.board.RegionType;
import it.polimi.ingsw.cg44.server.model.game.Board;
import it.polimi.ingsw.cg44.server.model.game.Player;
import it.polimi.ingsw.cg44.server.view.AcquirePermitCardView;

/**
 * Perform an additional main action.
 * @author saeid
 *
 */
public class MainActionBonus extends Bonus{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8212817256866799125L;
	private Action action;
	private transient Scanner input;
	private Model game;





	@Override
	public void useBonus(Player player, Board board) {
		System.out.println("Which action you want to do? select the MainAction number:\n");
		System.out.println("\t1-Acquire permit card\n\t2-Build emporium using permit card"
				+ "\n\t3-Build emporium with king help\n\t4-Elect councillors\n");
		
		int selectedAction = selectActionNumber();
		switch (selectedAction) {
		case 1:
			//action = new AcquirePermitCard(board, player, board.getRegions(selectRegion()) , player.getPoliticCards(), selectPermitcard());
			System.out.println("You have to satisfay the councillor.\n");
			System.out.println("The councillors in the balcony of HILL region are:\n"+ board.getRegions(RegionType.HILL).getBalcony().getBench());
			System.out.println("The councillors in the balcony of COAST region are:\n"+ board.getRegions(RegionType.COAST).getBalcony().getBench());
			System.out.println("The councillors in the balcony of MOUNTAIN region are:\n"+ board.getRegions(RegionType.MOUNTAIN).getBalcony().getBench());
			RegionType regionType = selectRegion();
			switch (regionType) {
			case HILL:
				int permitcardselectedFromHill = selectPermitcard(board, RegionType.HILL);

				if (permitcardselectedFromHill == 0 || permitcardselectedFromHill == 1) {
					AcquirePermitCardView acquirePermitCardView = new AcquirePermitCardView(
							player, board.getRegions(RegionType.HILL),
							board, player.getPoliticCards(), permitcardselectedFromHill);
					MainActionController mainActionController = new MainActionController(game.getPlayers());
					IAction action = acquirePermitCardView.createAction(mainActionController);
					action.run();
				} else {
					throw new IllegalArgumentException();
				}
				break;
			case COAST:
				int permitcardselectedFromCoast = selectPermitcard(board , RegionType.COAST);
				if (permitcardselectedFromCoast == 0 || permitcardselectedFromCoast == 1) {
					AcquirePermitCardView acquirePermitCardView = new AcquirePermitCardView(player,
							board.getRegions(RegionType.COAST), board,
							player.getPoliticCards(), permitcardselectedFromCoast);
					MainActionController mainActionController = new MainActionController(game.getPlayers());
					IAction action = acquirePermitCardView.createAction(mainActionController);
					action.run();
				} else {
					throw new IllegalArgumentException();
				}
				break;
			case MOUNTAIN:
				int permitcardselectedFromMaountain = selectPermitcard(board, RegionType.MOUNTAIN);
				if (permitcardselectedFromMaountain == 0 || permitcardselectedFromMaountain == 1) {
					AcquirePermitCardView acquirePermitCardView = new AcquirePermitCardView(player,
							game.getBoard().getRegions(RegionType.MOUNTAIN), game.getBoard(),
							player.getPoliticCards(), permitcardselectedFromMaountain);
					MainActionController mainActionController = new MainActionController(game.getPlayers());
					IAction action = acquirePermitCardView.createAction(mainActionController);
					action.run();
				} else {
					throw new IllegalArgumentException();
				}
				break;

			}
		}

			
	}
	/**
	 * the method for asking to client to select region
	 * @return RegionType
	 */
	public RegionType selectRegion(){
		String regionName = selectNameRegion();
		RegionType regionType = null;
		switch (regionName) {
		case "HILL":
			regionType = RegionType.HILL;
			break;
		case "COAST":
			regionType = RegionType.COAST;
			break;
		case "MOUNTAIN":
			regionType = RegionType.MOUNTAIN;
			break;
		default:
			//System.out.println("The region type is not correct!");
		}
		return regionType;
	}
	
	/**
	 * the method for asking client to select one permitCard face up
	 * @param game
	 * @param regionType
	 * @return
	 */
	public int selectPermitcard(Board board , RegionType regionType){
		int choose =0;
		boolean condition = true;
		System.out.println("\nSelect the one of the two faceUp permitcard:('0' or '1')");
		System.out.println("\n.............................................................................\n0 --> "
				+ board.getRegions(regionType).getDeckOfPermitCard().getFsceUpPermitCard().get(0));
		System.out.println("\n.............................................................................\n1 --> "
				+ board.getRegions(regionType).getDeckOfPermitCard().getFsceUpPermitCard().get(1)+"\n"
						+ "..............................................................................");
		while (condition) {
			try {
				choose = input.nextInt();
				if(choose==0 || choose==1){
					condition = false;
					return choose;
				}
				System.out.println("Try again! 'Incorrect input'\tinsert '0' or '1'\n");
			} catch (InputMismatchException e) {
				input.nextLine();
			}
		}
		return choose;
	}
	

	
	public Action getAction() {
		return action;
	}
	@Override
	public String toString() {
		return "MainActionBonus";
	}
	/**
	 * the method for asking client to select one Main Action
	 * @return int selectedAction
	 */
	public int selectActionNumber(){
		int mainActionNumber = 0;
		boolean condition = true;
		while (condition) {
			try {
				mainActionNumber= input.nextInt();
				if(0 < mainActionNumber && mainActionNumber < 5){
					condition = false;
					return mainActionNumber;
				}
				System.out.println("Try again. (" +"Incorrect input: an integer is required: 1 or 2 0r 3 or 4)\n");
			} catch (InputMismatchException e) {
				
	            input.nextLine();
			}
		}
		return mainActionNumber;
	}

	/**
	 * Metodo usato al interno del metodo selectRegion per prendere in ingresso dal utente 
	 * come String il nome della regione
	 * @return
	 */
	public String selectNameRegion(){
		String regionTypeStr = null;
		boolean condition = true;
		System.out.println("\nSelect a region frome which to take permit card::\n\n\t1-HILL\n\t2-COAST\n\t3-MOUNTAIN\n");
		while (condition) {
			try {
				regionTypeStr = input.next().toUpperCase();
				if(regionTypeStr.equals("HILL") || regionTypeStr.equals("COAST") || regionTypeStr.equals("MOUNTAIN")){
					condition = false;
					return regionTypeStr;
				}
				System.out.println("Try again! 'Incorrect input'\n");
			} catch (InputMismatchException e) {
				input.nextLine().toUpperCase();
			}
		}
		return regionTypeStr;
	}


}
