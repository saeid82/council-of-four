//MANCA LA GESTIONE DEL CASO CHE IL MAZZO SIA FINITO.
// MANCA ANCHE GESTIONE position che può valere solamente 0 o 1.

package it.polimi.ingsw.cg44.server.model.card.permitcard;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import it.polimi.ingsw.cg44.server.model.city.City;

public class DeckOfPermitCard {
	
	private final int MAX_DECK_SIZE;
	private final Queue<PermitCard> deck;
	private final List<PermitCard> faceUpCard;
	
	
	/**
	 * The constructor at the beginning fill deck of the permit cards with for loop.
	 * and create an array for draw 2 permit card (face up) from deck.
	 * @param cities
	 * @param numberPlayers
	 * @throws Exception 
	 */
	public DeckOfPermitCard(List<City> cities, int numberPlayers) throws Exception {
		this.MAX_DECK_SIZE = calculateMaxDeckSize(numberPlayers);
		this.deck = new LinkedList<>(); 
		try {
			for(int i = 0; i < MAX_DECK_SIZE; i++)
				deck.add(new PermitCard(cities));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		faceUpCard = Arrays.asList(new PermitCard[2]);
		faceUpCard.set(0, deck.poll());
		faceUpCard.set(1, deck.poll());
	}
	
	public List<PermitCard> getFsceUpPermitCard(){
		return faceUpCard;
	}
	
	public Queue<PermitCard> getDeck() {
		return deck;
	}
	

	/**
	 * method for draw one of the two permit cards with face up
	 * @param cardNumToDraw
	 * @return drawn card will pass to the player.
	 */
	public PermitCard drawFaceUpPermitCard(int cardNumToDraw){
	
		PermitCard drawedCard =	faceUpCard.get(cardNumToDraw);
		//replace the drawn card with the top card of the deck
		faceUpCard.set(cardNumToDraw, deck.poll());
		return drawedCard;
	}
	
	public void changeFaceUpCard(){

		deck.add(this.drawFaceUpPermitCard(0));
		deck.add(this.drawFaceUpPermitCard(1));

	}
	/**
	 * This method calculate the max number of the permit cards depending on the number of players.
	 * @param numberPlayers
	 * @return
	 */
	private int calculateMaxDeckSize(int numberPlayers) {
		int standardDeckSize = 15;
		if(numberPlayers<5)
			return standardDeckSize;
		else
			return standardDeckSize + (3 * (numberPlayers - 4)); 
	}
	
	
	@Override
	public String toString() {
		return "deck =\n " + deck + "]";
	}

}
