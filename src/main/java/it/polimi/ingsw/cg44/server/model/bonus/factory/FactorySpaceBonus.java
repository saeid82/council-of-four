package it.polimi.ingsw.cg44.server.model.bonus.factory;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;




/**
 * @author saeid
 *
 */
public class FactorySpaceBonus extends FactoryBonus {
	
	//costante usata per garantire la generazione di tutti bonus possibili (tutti gli elementi della
		//mappa BonusClasses in generateBonus) eccetto il NobilityBonus con la somma di un + 1 quando
		//generiamo randomicamente la chiave della mappa in chooseBonus.
		private static final int BOUND_CHOICE_BONUS = 8;
		
		/**
		 * 
		 */
		public FactorySpaceBonus() {
			super();
			// TODO Auto-generated constructor stub
		}
		

		/* (non-Javadoc)
		 * @see it.polimi.ingsw.cg44.server.model.bonus.factory.FactoryBonus#getBonus()
		 */
		@Override
		public List<IBonus> getBonus() {
			Random random = new Random();
			List<IBonus> listOfAddedBonus = new ArrayList<>();	
			int quantityBonus = chooseQuantity(); // 4 possibili valori: 0,1,2,3
			
			try{
				for(int i=0; i<quantityBonus; i++){
					Class<?> bonusToAdd = Class.forName(bonusClasses.get(random.nextInt(BOUND_CHOICE_BONUS)));

					if(conditionToAddBonus(bonusToAdd, listOfAddedBonus)){ 
						i--;
					}
					else {
						listOfAddedBonus.add((IBonus) bonusToAdd.newInstance());
					}
				}
				return listOfAddedBonus;
			}
			catch(ClassNotFoundException ex){
				//System.out.println(ex.getMessage());
				return null;
			}
			catch (InstantiationException e) {
				//System.out.println(e.getMessage());
				return null;
			}
			catch (IllegalAccessException e) {
				//System.out.println(e.getMessage());
				return null;
			}
		}

		/**
		 * Override del metodo chooseQuantity perchè le SpaceBonus possono anche non avere Bonus
		 * mentre non vale altrettanto per la carte permesso e le città (le quali utilizzano il metodo di default) 
		 * @see it.polimi.ingsw.cg44.server.model.card.permitcard.IQuantity#chooseQuantity()
		 */
		@Override
		public int chooseQuantity() {
		
			Random random = new Random();
			int bonusQuantity = random.nextInt(MAX_NUMBER);
			
			if(bonusQuantity<40)
				return 0;
			if(bonusQuantity>=40 && bonusQuantity<65)
				return 1;
			if(bonusQuantity>=65 && bonusQuantity<90)
				return 2;
			else
				return 3;
		}
}
