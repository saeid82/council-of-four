package it.polimi.ingsw.cg44.server.view;

import it.polimi.ingsw.cg44.server.controller.Controller;
import it.polimi.ingsw.cg44.server.controller.QuickActionConroller;
import it.polimi.ingsw.cg44.server.controller.action.IAction;
import it.polimi.ingsw.cg44.server.model.game.Board;
import it.polimi.ingsw.cg44.server.model.game.Player;

public class PerformAnotherMainActionView extends View {

	public PerformAnotherMainActionView(Player player, Board board) {
		super(player, board);
	}

	@Override
	public IAction createAction(Controller quickActionController) throws Exception {
		if(isValid())
			return ((QuickActionConroller) quickActionController).update(this);
		else
			return null;
	}
	
	@Override
	protected boolean isValid() throws Exception{
		if(getPlayer().getAssistants().size() <  3){
			throw new Exception("you need at least 3 assistants to execute this action.");
			//System.out.println("you need at least 3 assistants to execute this action.");
			//return false;
		}
		else 
			return true;	
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}

}
