package it.polimi.ingsw.cg44.server.controller.action;

/**
 * Factory pattern:
 * create a IActionFactory interface and use IAction interface.
 * @author saeid
 *
 */
public interface IAction {
	
	public abstract void run();
}
