package it.polimi.ingsw.cg44.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.cg44.cli.ServerApp;
import it.polimi.ingsw.cg44.server.controller.CliGameControllerHelper;

public class ClientApp {
	
	private static Logger logger = Logger.getLogger(ClientApp.class.getName());
	
	private static Scanner consoleScanner = new Scanner(System.in);
	
	private static String host = "127.0.0.1";
	private static volatile int OUR_PLAYER_NUMBER = -1;
	
	public static void main(String[] args) {
		int portNumber = ServerApp.getPortNumber();
		Socket socket = null;
		try {
			logger.info(String.format("Creating socket %s:%d", host, portNumber));
			socket = new Socket(host, portNumber);
			socket.setTcpNoDelay(true);
			socket.setReceiveBufferSize(30000);
//			socket.setSoTimeout(2000);
		} catch (IOException e) {
			logger.log(Level.SEVERE, "\nCouldn't create the client socket to connect to the host " + host + ":" + portNumber + ".", e);
			System.exit(0);
		}
		;
		try(PrintWriter printWriter = new PrintWriter(socket.getOutputStream());
//				Scanner socketScanner = new Scanner(socket.getInputStream());
			BufferedReader br = new BufferedReader(new InputStreamReader( socket.getInputStream()))
					) {
			printWriter.println("Hi!");
			printWriter.flush();
			String line = null;
			while( (line = br.readLine())!=null ){
				System.out.println(line);
				if(line.contains(ServerApp.USER_INTRODUCTION_STRING)){
					int playerNumberIndex = line.indexOf(ServerApp.USER_INTRODUCTION_STRING) + ServerApp.USER_INTRODUCTION_STRING.length();
					int playerNumberLength = 1;
					try{
						OUR_PLAYER_NUMBER = Integer.valueOf(line.substring(playerNumberIndex, playerNumberIndex + 1));
					} catch(NumberFormatException nfe){
						System.err.println("Couldn't extract player Number");
						System.exit(0);
					}
				}
			
			
				if(line.contains(String.format(CliGameControllerHelper.PLAYER_TURN_INDICATOR, OUR_PLAYER_NUMBER))){
					String  clientAnswer = consoleScanner.next();
					printWriter.println(clientAnswer);
					printWriter.flush();
				}
			
			}
			
//			while( (line))
			
//			InputStream inputStream = socket.getInputStream();
//			Buffere dataInputStream = new ObjectOutputStream(inputStream);
//			dataInputStream.read
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
