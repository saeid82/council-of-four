/**
 * 
 */
package it.polimi.ingsw.cg44.cli;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 * @author saeid
 *
 */
public class MainCLI {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter output = new BufferedWriter(new OutputStreamWriter(System.out));
		System.out.println("How you want to play?\n\t1- TCP connection\n\t2- Local\n> ");
		while (true) {
			String chosenStr = null;
			try {
				chosenStr = input.readLine();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			Integer chosenInt = Integer.parseInt(chosenStr);
			if(chosenStr == null){
				try {
					output.write("You have nothing inserted, select '1' or '2'\n");
				} catch (IOException e) {
					e.printStackTrace();
				}
				return;
			}
			if (chosenInt == 1) {
				it.polimi.ingsw.cg44.cli.ServerApp.main(args);
				System.out.println("\nRun the client/ClientApp.java class for client side.");
			} else if(chosenInt == 2){
				try {
					it.polimi.ingsw.cg44.cli.LocalApp.main(args);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

}
