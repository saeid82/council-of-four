package it.polimi.ingsw.cg44.cli;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;

import it.polimi.ingsw.cg44.server.controller.CliGameControllerHelper;
import it.polimi.ingsw.cg44.server.model.Model;
import it.polimi.ingsw.cg44.server.socket.ClientSocketContainer;

public class ServerApp {

	private static final Logger logger = Logger.getLogger(ServerApp.class.getName());
	private static Model game;
	private static Scanner serverInput;
	private static volatile int portNumber = 29999;
	/**
	 * 20 seconds timeout to connection the client, after timeout if the players number will be > 2 the game will start with at least 2 players
	 */
	private static final int TIME_OUT_CONNECTION = 20*1000;
	/**
	 * the minimum player number are 4 but after timeout we can playing with 2 players
	 */
	private static final int MIN_PLAYER_NUMBER = 2;
	
	
	public static int getPortNumber() {
		return portNumber;
	}

	public static void setPortNumber(int portNumber) {
		ServerApp.portNumber = portNumber;
	}

	public static final String USER_INTRODUCTION_STRING = "Hi, You are the player  "; 

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		serverInput = new Scanner(System.in);

		System.out.println("***************************************************************************************************************");
		System.out.println("***************************************************************************************************************");
		System.out.println("***  __________    __      __      _________                                                                ***");
		System.out.println("*** |          |  |  |    |  |    |         |                                                               ***");
		System.out.println("*** |___    ___|  |  |    |  |    |    _____|                                                               ***");
		System.out.println("***     |  |      |  |____|  |    |   |__                                                                   ***");
		System.out.println("***     |  |      |   ____|  |    |    __|                                                                  ***");
		System.out.println("***     |  |      |  |    |  |    |   |_____                                                                ***");
		System.out.println("***     |  |      |  |    |  |    |         |                                                               ***");
		System.out.println("***     |__|      |__|    |__|    |_________|                                                               ***");
		System.out.println("***                                                                                                         ***");
		System.out.println("***   ________      ___________    ___       ___    ___      ___    ________    ___________    ___          ***");
		System.out.println("***  |        |    |           |  |   |     |   |  |   \\    |   |  |        |  |           |  |   |         ***");
		System.out.println("***  |   _____|    |   _____   |  |   |     |   |  |    \\   |   |  |   _____|  |___     ___|  |   |         ***");
		System.out.println("***  |  |          |  |     |  |  |   |     |   |  |     \\  |   |  |  |            |   |      |   |         ***");
		System.out.println("***  |  |          |  |     |  |  |   |     |   |  |      \\ |   |  |  |            |   |      |   |         ***");
		System.out.println("***  |  |_____     |  |_____|  |  |   |_____|   |  |   |\\  \\|   |  |  |_____    ___|   |___   |   |______   ***");
		System.out.println("***  |        |    |           |  |             |  |   | \\      |  |        |  |           |  |          |  ***");
		System.out.println("***  |________|    |___________|  |_____________|  |___|  \\_____|  |________|  |___________|  |__________|  ***");
		System.out.println("***                                                                                                         ***");
		System.out.println("***                                                                                                         ***");
		System.out.println("***   ___________    ___________        __________    ____________    ___        ___    __________          ***");
		System.out.println("***  |           |  |           |      |          |  |            |  |   |      |   |  |    ___   |         ***");
		System.out.println("***  |   _____   |  |   ________|      |   _______|  |   ______   |  |   |      |   |  |   |   |  |         ***");
		System.out.println("***  |  |     |  |  |  |_____          |  |_____     |  |      |  |  |   |      |   |  |   |___|  |         ***");
		System.out.println("***  |  |     |  |  |   _____|         |   _____|    |  |      |  |  |   |      |   |  |        __|         ***");
		System.out.println("***  |  |_____|  |  |  |               |  |          |  |______|  |  |   |______|   |  |   |\\   \\           ***");
		System.out.println("***  |           |  |  |               |  |          |            |  |              |  |   | \\   \\          ***");
		System.out.println("***  |___________|  |__|               |__|          |____________|  |______________|  |___|  \\___\\         ***");
		System.out.println("***                                                                                                         ***");
		System.out.println("***************************************************************************************************************");
		System.out.println("***************************************************************************************************************");

		
		System.out.println("\n------------------------------------------------------------------------------------------------------------------------"
				+ "\n                                                          MAIN ACTION                                                \n"
				+ "------------------------------------------------------------------------------------------------------------------------\n");
		System.out.println(
				"\nMainAction 1: (Elect Councillor)\n\tChoose one available councillors at the side of the board,"
						+ "\n\tInserts them in the choosen council balcony. receive 4 coins");
		System.out.println(
				"\nMainAction 2:(Acquire permit card)\n\tChoose and satisfies the council of a region by discarding between 1 and 4 politics cards,"
						+ "\n\tcorresponding to the colors of the councillors present in the council."
						+ "\n\tPays a sum of money, depending on the number of councillors satisfied."
						+ "\n\t-1 card played = 10 coins\n\t-2 cards played = 7 coins\n\t-3 cards played = 4 coins\n\t-4 cards played = 0 coins\n\t-Each Joker card played = 1 additional coin"
						+ "\n\tChoose one of the two permit card face up in front of the council they have satisfied.then takes it and place it face up in front of them."
						+ "\n\tobtain the bonuses indicated at the bottom of the card.replace the card with the first card of the corresponding deck.");
		System.out.println(
				"\nMainAction 3:(Building emporium using permit card)\n\tChoose on of the permit card face up in front of them,they place an emporium on the, "
						+ "\n\tcorresponding space in the city whose initial is indicated on the card."
						+ "\n\tthen turn the used card face down and can't use it anymore to build another emporium.");
		System.out.println(
				"\nMainAction 4: (Building emporium with king help)\n\tSatisfy the King's council, Move the King to the city of their choice, The King must use uninterrupted roads to make"
						+ "\n\tthe journey. The player pays 2 coins for each road traveled. the King may also be left in the same city, in this case the player pays no money."
						+ "\n\tbuild an emporium in the space where the King is located at the end of his journey.");
		System.out.println("\n------------------------------------------------------------------------------------------------------------------------"
				+ "\n                                                          Quick ACTION                                                \n"
				+ "------------------------------------------------------------------------------------------------------------------------\n");
		System.out.println("\nQuickAction 1: (Engage assistant)\n\tPays three coins, take an assistant from the pool,");
		System.out.println(
				"\nQuickAction 2: (Change faceUp permit card)\n\tReturn an assistant to the pool, then take the 2 permit card face up in a region, "
						+ "\n\treturn them to the bottom of the deck and draws 2 new ones from the top of the deck.");
		System.out.println(
				"\nQuickAction 3: (Send assistant to elect councillor)\n\tReturns an assistant to the pool, takes a councillor and inserts them in a balcony.");
		System.out.println(
				"\nQuickAction 4: (Perform additional Main action)\n\tReturn 3 assistants to the pool and perform another main action.");

		System.out.println("\n. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . ."
				+ "\n\t\t\t\t\t\t\t\t\tInsert the number of players: \n. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . "
				+ ". . . . . . .. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .\n");
		boolean ifInputWasInteger = true;
		int playerCount = 0;
		while (ifInputWasInteger) {
			try {
				playerCount = serverInput.nextInt();
				ifInputWasInteger = false;
			} catch (InputMismatchException e) {
				System.out.println("Try again! the inserted number is not correct! the minimum player number are 4.");
				serverInput.nextLine();
			}
		}
		
		try {
			game = new Model("src/main/resources/map.txt", playerCount);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
		System.out.println("\n\nThere are " + playerCount + " players, The game will start with first player!\n");

		ServerSocket serverSocket = null;
		try {
			logger.info("Starting server on the port: " + getPortNumber() + "\n");
			serverSocket = new ServerSocket(getPortNumber());
			serverSocket.setReceiveBufferSize(1000);
			serverSocket.setSoTimeout(TIME_OUT_CONNECTION);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		}
		
		List<ClientSocketContainer> clientSocketContainers = new ArrayList<>();
		List<CliGameControllerHelper> cliGameControllerHelpers = new ArrayList<>();
		try {
			for(int i = 0; i < playerCount; i++){
				logger.info("Waiting to connect the player number: " + i+1 + "\n");
				Socket socket = serverSocket.accept();
				ClientSocketContainer clientHandler = new ClientSocketContainer(socket);
				clientSocketContainers.add(clientHandler);
				cliGameControllerHelpers.add(new CliGameControllerHelper(game, i, clientHandler));
				clientHandler.getSocketCommunicator().send("\n" + String.format(USER_INTRODUCTION_STRING + "%d", i+1) + "\n");
			}
			
		// when the time will be over (20 seconds) and don't get any connection with other players the game will start
		// with at least 2 players, in otherwise the game will crash	
		} catch (SocketTimeoutException e) {
			if (clientSocketContainers.size() >= MIN_PLAYER_NUMBER) {
				logger.info("the time is over,we will continuo with at least 2 players.");
				
				// numero di giocatori aggiornati dopo timeout
				playerCount = clientSocketContainers.size();
			}else{
				logger.warning(e.getMessage());
				System.exit(0);
			}
		} catch (IOException e) {
			logger.info(e.getMessage());
		}
		
		// sending message to all the client for number of players updated
		for(ClientSocketContainer clientSocketContainer : clientSocketContainers){
			clientSocketContainer.getSocketCommunicator().send("We will playing with "+playerCount+" players");
		}
		while(!game.endGame(game)){
			for(int playerNumber = 0; playerNumber < playerCount; playerNumber++){
				try {
					cliGameControllerHelpers.get(playerNumber).play();
				} catch (Exception e) {
					logger.info(e.getMessage());
				}
			}
		}
		logger.info("End the game!");
		for(ClientSocketContainer clientSocketContainer : clientSocketContainers){
			clientSocketContainer.getSocketCommunicator().send("End the game!");
		}
		System.exit(0);
	}


}
