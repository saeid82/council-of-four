package it.polimi.ingsw.cg44.cli;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import it.polimi.ingsw.cg44.server.controller.MainActionController;
import it.polimi.ingsw.cg44.server.controller.QuickActionConroller;
import it.polimi.ingsw.cg44.server.controller.action.IAction;
import it.polimi.ingsw.cg44.server.model.Model;
import it.polimi.ingsw.cg44.server.model.board.RegionType;
import it.polimi.ingsw.cg44.server.model.colors.Colors;
import it.polimi.ingsw.cg44.server.view.AcquirePermitCardView;
import it.polimi.ingsw.cg44.server.view.BuildEmporiumUsingPermitCardView;
import it.polimi.ingsw.cg44.server.view.BuildEmporiumWithKingHelpView;
import it.polimi.ingsw.cg44.server.view.ChangeFaceUpPermitCardView;
import it.polimi.ingsw.cg44.server.view.ElectCouncillorView;
import it.polimi.ingsw.cg44.server.view.EngageAssistantView;
import it.polimi.ingsw.cg44.server.view.SendAssistantToElectCouncillorView;

//--------------------------------------------- THE COUNCILLOR OF FOUR ----------------------------------
/**
 * 
 * @author saeid
 *
 */
public class LocalApp {

	private static Scanner input;

	public static void main(String[] args) throws Exception {

		input = new Scanner(System.in);
		System.out.println("***************************************************************************************************************");
		System.out.println("***************************************************************************************************************");
		System.out.println("***  __________    __      __      _________                                                                ***");
		System.out.println("*** |          |  |  |    |  |    |         |                                                               ***");
		System.out.println("*** |___    ___|  |  |    |  |    |    _____|                                                               ***");
		System.out.println("***     |  |      |  |____|  |    |   |__                                                                   ***");
		System.out.println("***     |  |      |   ____|  |    |    __|                                                                  ***");
		System.out.println("***     |  |      |  |    |  |    |   |_____                                                                ***");
		System.out.println("***     |  |      |  |    |  |    |         |                                                               ***");
		System.out.println("***     |__|      |__|    |__|    |_________|                                                               ***");
		System.out.println("***                                                                                                         ***");
		System.out.println("***   ________      ___________    ___       ___    ___      ___    ________    ___________    ___          ***");
		System.out.println("***  |        |    |           |  |   |     |   |  |   \\    |   |  |        |  |           |  |   |         ***");
		System.out.println("***  |   _____|    |   _____   |  |   |     |   |  |    \\   |   |  |   _____|  |___     ___|  |   |         ***");
		System.out.println("***  |  |          |  |     |  |  |   |     |   |  |     \\  |   |  |  |            |   |      |   |         ***");
		System.out.println("***  |  |          |  |     |  |  |   |     |   |  |      \\ |   |  |  |            |   |      |   |         ***");
		System.out.println("***  |  |_____     |  |_____|  |  |   |_____|   |  |   |\\  \\|   |  |  |_____    ___|   |___   |   |______   ***");
		System.out.println("***  |        |    |           |  |             |  |   | \\      |  |        |  |           |  |          |  ***");
		System.out.println("***  |________|    |___________|  |_____________|  |___|  \\_____|  |________|  |___________|  |__________|  ***");
		System.out.println("***                                                                                                         ***");
		System.out.println("***                                                                                                         ***");
		System.out.println("***   ___________    ___________        __________    ____________    ___        ___    __________          ***");
		System.out.println("***  |           |  |           |      |          |  |            |  |   |      |   |  |    ___   |         ***");
		System.out.println("***  |   _____   |  |   ________|      |   _______|  |   ______   |  |   |      |   |  |   |   |  |         ***");
		System.out.println("***  |  |     |  |  |  |_____          |  |_____     |  |      |  |  |   |      |   |  |   |___|  |         ***");
		System.out.println("***  |  |     |  |  |   _____|         |   _____|    |  |      |  |  |   |      |   |  |        __|         ***");
		System.out.println("***  |  |_____|  |  |  |               |  |          |  |______|  |  |   |______|   |  |   |\\   \\           ***");
		System.out.println("***  |           |  |  |               |  |          |            |  |              |  |   | \\   \\          ***");
		System.out.println("***  |___________|  |__|               |__|          |____________|  |______________|  |___|  \\___\\         ***");
		System.out.println("***                                                                                                         ***");
		System.out.println("***************************************************************************************************************");
		System.out.println("***************************************************************************************************************");
		System.out.println("\n------------------------------ MAIN ACTION ------------------------------");
		System.out.println(
				"\nMainAction 1: (Elect Councillor)\n\tChoose one available councillors at the side of the board,"
						+ "\n\tInserts them in the choosen council balcony. receive 4 coins");
		System.out.println(
				"\nMainAction 2:(Acquire permit card)\n\tChoose and satisfies the council of a region by discarding between 1 and 4 politics cards,"
						+ "\n\tcorresponding to the colors of the councillors present in the council."
						+ "\n\tPays a sum of money, depending on the number of councillors satisfied."
						+ "\n\t-1 card played = 10 coins\n\t-2 cards played = 7 coins\n\t-3 cards played = 4 coins\n\t-4 cards played = 0 coins\n\t-Each Joker card played = 1 additional coin"
						+ "\n\tChoose one of the two permit card face up in front of the council they have satisfied.then takes it and place it face up in front of them."
						+ "\n\tobtain the bonuses indicated at the bottom of the card.replace the card with the first card of the corresponding deck.");
		System.out.println(
				"\nMainAction 3:(Building emporium using permit card)\n\tChoose on of the permit card face up in front of them,they place an emporium on the, "
						+ "\n\tcorresponding space in the city whose initial is indicated on the card."
						+ "\n\tthen turn the used card face down and can't use it anymore to build another emporium.");
		System.out.println(
				"\nMainAction 4: (Building emporium with king help)\n\tSatisfy the King's council, Move the King to the city of their choice, The King must use uninterrupted roads to make"
						+ "\n\tthe journey. The player pays 2 coins for each road traveled. the King may also be left in the same city, in this case the player pays no money."
						+ "\n\tbuild an emporium in the space where the King is located at the end of his journey.");
		System.out.println("\n\n------------------------------ Quick ACTION ------------------------------");
		System.out.println("\nQuickAction 1: (Engage assistant)\n\tPays three coins, take an assistant from the pool,");
		System.out.println(
				"\nQuickAction 2: (Change faceUp permit card)\n\tReturn an assistant to the pool, then take the 2 permit card face up in a region, "
						+ "\n\treturn them to the bottom of the deck and draws 2 new ones from the top of the deck.");
		System.out.println(
				"\nQuickAction 3: (Send assistant to elect councillor)\n\tReturns an assistant to the pool, takes a councillor and inserts them in a balcony.");
		System.out.println(
				"\nQuickAction 4: (Perform additional Main action)\n\tReturn 3 assistants to the pool and perform another main action.");

		System.out.println("\n. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . ."
				+ "\n\t\t\tInsert the number of players:\n. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .");
		boolean condition = true;
		int playerNumber = 0;
		while (condition) {
			try {
				playerNumber = input.nextInt();
				condition = false;
			} catch (InputMismatchException e) {
				System.out.println("Try again! the inserted number is not correct!");
				input.nextLine();
			}
		}
		
		Model game = new Model("src/main/resources/map.txt", playerNumber);
		System.out.println("\nThere are " + playerNumber + " players, The game will start with first player!\n");

		while (game.endGame(game) == false) {
			for (int i = 0; i < game.getPlayers().size(); i++) {
				System.out.println(
						"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
				System.out.println("\n\t\t\t\t\tPLAYER " + game.getPlayers().get(i).getId());
				System.out.println(
						"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
				System.out.println("Your situaztion is:\n" + game.getPlayers().get(i));
				System.out.println("\n------------------------------ MAIN ACTION ------------------------------");
				System.out.println(
						"\nYou can do one MAIN action from the four available action, Select an actions number:\n");
				System.out.println("\t1-Acquire permit card\n\t2-Build emporium using permit card"
						+ "\n\t3-Build emporium with king help\n\t4-Elect councillors\n");
				int mainActionNumber = selectActionNumber();
				switch (mainActionNumber) {// switch per main action
				case 1:
					acquirePermitCard(game, i);
					break;
				case 2:
					buildEmporiumUsingPermitCard(game, i);
					break;
				case 3:
					buildEmporiumWithKingHelp(game, i);
					break;
				case 4:
					electCouncillor(game, i);
					break;
				default:
				}// swith finish --- main action

				System.out.println("Now your situation is:\n" + game.getPlayers().get(i));

				System.out.println("\n------------------------------ Quick ACTION ------------------------------");

				System.out.println(
						"You can do one QUICK action from the four available action!\nDo you want to perform a Quick action? [y/n]");
				// controllo input del utente che sia giusto
				String answer = playerAnswer();

				if (answer.toUpperCase().equals("Y")) {

					System.out.println(
							"Select a Quick actions number:\n\n\t1-Change face up permit card\n\t2-Engage assistant"
									+ "\n\t3-Perform additional main action\n\t4-Send assistant to elect councillor\n");
					int quickActionNumber = selectActionNumber();
					switch (quickActionNumber) {
					case 1:
						changeFaceUpPermitCard(game, i);
						break;
					case 2:
						engageAssistant(game, i);
						break;
					case 3:
						performAdditionalMainAction(game, i);
						break;
					case 4:
						sendAssistantToElectCouncillor(game, i);
						break;
					default:
						// throw new IllegalArgumentException("the number
						// inserted is not correct!");
					}// switch quickaction finished
				} else {
					System.out.println("The player number " + game.getPlayers().get(i).getId() + " has terminated!");
				}

				System.out.println("The player number " + game.getPlayers().get(i).getId() + " has terminated!");
				System.out.println("**********************************************************************************");
				//System.out.println("\nNow is the turn of the player " + game.getPlayers().get(i + 1).getId());

			} // for finish
		} // while finish
		System.out
				.println("\n,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,, End the game! ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,\n");
	}// main method finish

	
	
	// -------------------------------- the methods for main actions ------------------------------------------

	
	
	/**
	 * the method for main action: acquirePermitCard
	 * 
	 * @param game
	 * @param playerNumber
	 */
	public static void acquirePermitCard(Model game, int playerNumber) {
		System.out.println("You have to satisfay the councillor.\n");
		System.out.println("The councillors in the balcony of HILL region are:\n"
				+ game.getBoard().getRegions(RegionType.HILL).getBalcony().getBench());
		System.out.println("The councillors in the balcony of COAST region are:\n"
				+ game.getBoard().getRegions(RegionType.COAST).getBalcony().getBench());
		System.out.println("The councillors in the balcony of MOUNTAIN region are:\n"
				+ game.getBoard().getRegions(RegionType.MOUNTAIN).getBalcony().getBench());

		String nameRegion = selectNameRegion();
		RegionType regionType = selectRegion(nameRegion);
		switch (regionType) {
		case HILL:
			hillAcquirePermirCard(game, playerNumber);
			break;
		case COAST:
			coastAcquirePermitCard(game, playerNumber);
			break;
		case MOUNTAIN:
			mountainAcquirePermitCard(game, playerNumber);
			break;
		default:
		}

	}

	/**
	 * the method used for AcquirePErmiCard in case of HILL region selected by
	 * player
	 * 
	 * @param game
	 * @param playerNumber
	 */
	public static void hillAcquirePermirCard(Model game, int playerNumber) {
		int permitcardselected = selectPermitcard(game, RegionType.HILL);

		if (permitcardselected == 0 || permitcardselected == 1) {
			AcquirePermitCardView acquirePermitCardView = new AcquirePermitCardView(game.getPlayers().get(playerNumber),
					game.getBoard().getRegions(RegionType.HILL), game.getBoard(),
					game.getPlayers().get(playerNumber).getPoliticCards(), permitcardselected);
			MainActionController mainActionController = new MainActionController(game.getPlayers());
			IAction action = acquirePermitCardView.createAction(mainActionController);
			action.run();
		} else {
			throw new IllegalArgumentException();
		}
	}

	/**
	 * the method used for AcquirePErmiCard in case of COAST region selected by
	 * player
	 * 
	 * @param game
	 * @param playerNumber
	 */
	public static void coastAcquirePermitCard(Model game, int playerNumber) {

		int permitcardselected = selectPermitcard(game, RegionType.COAST);
		if (permitcardselected == 0 || permitcardselected == 1) {
			AcquirePermitCardView acquirePermitCardView = new AcquirePermitCardView(game.getPlayers().get(playerNumber),
					game.getBoard().getRegions(RegionType.COAST), game.getBoard(),
					game.getPlayers().get(playerNumber).getPoliticCards(), permitcardselected);
			MainActionController mainActionController = new MainActionController(game.getPlayers());
			IAction action = acquirePermitCardView.createAction(mainActionController);
			action.run();
		} else {
			throw new IllegalArgumentException();
		}
	}

	/**
	 * the method used for AcquirePErmiCard in case of MOUNTAIN region selected
	 * by player
	 * 
	 * @param game
	 * @param playerNumber
	 */
	public static void mountainAcquirePermitCard(Model game, int playerNumber) {
		int permitcardselected = selectPermitcard(game, RegionType.MOUNTAIN);
		if (permitcardselected == 0 || permitcardselected == 1) {
			AcquirePermitCardView acquirePermitCardView = new AcquirePermitCardView(game.getPlayers().get(playerNumber),
					game.getBoard().getRegions(RegionType.MOUNTAIN), game.getBoard(),
					game.getPlayers().get(playerNumber).getPoliticCards(), permitcardselected);
			MainActionController mainActionController = new MainActionController(game.getPlayers());
			IAction action = acquirePermitCardView.createAction(mainActionController);
			action.run();
		} else {
			throw new IllegalArgumentException();
		}
	}

	/**
	 * The method for main action BuildEmporiumUsingPermitCard
	 * 
	 * @param game
	 * @param playerNumber
	 * @throws Exception
	 */
	public static void buildEmporiumUsingPermitCard(Model game, int playerNumber) throws Exception {
		int permCardNum = selectPermitcardFromPly(game, playerNumber); // the
																		// permitcard
																		// selected
		// permitcard può avere qualche city e non solo uno, quindi....
		for (int i = 0; i < game.getPlayers().get(playerNumber).getPermitCards().get(permCardNum).getCities()
				.size(); i++) {
			MainActionController mainActionController = new MainActionController(game.getPlayers());
			BuildEmporiumUsingPermitCardView buildEmporiumUsingPermitCardView = new BuildEmporiumUsingPermitCardView(
					game.getPlayers().get(playerNumber),
					game.getPlayers().get(playerNumber).getPermitCards().get(permCardNum).getCities().get(i),
					game.getBoard(), game.getPlayers().get(playerNumber).getPermitCards().get(permCardNum));
			IAction action = buildEmporiumUsingPermitCardView.createAction(mainActionController);
			action.run();
		}

	}

	/**
	 * The method for main action BuildEmporiumWithKingHelp
	 * 
	 * @param game
	 * @param playerNumber
	 */
	public static void buildEmporiumWithKingHelp(Model game, int playerNumber) {
		System.out.println("Yu must satisfy the King's council! and select one city to moving the King.");
		System.out
				.println("Attention!! you must have enough coins to moving King, for each road you have to pay 2 coins."
						+ "\nThe King may also be left in the same city.");
		System.out.println("Currently the KING is located within the city:\n\nKING ---> "
				+ game.getBoard().getKing().getCity() + "\n\nSelect the city index:\n");

		for (int i = 0; i < game.getBoard().getCities().size(); i++) {
			System.out.println(i + "-" + game.getBoard().getCities().get(i));
		}

		input = new Scanner(System.in);
		int selectedCity = input.nextInt();
		BuildEmporiumWithKingHelpView buildEmporiumWithKingHelpView = new BuildEmporiumWithKingHelpView(
				game.getPlayers().get(playerNumber), game.getBoard(), game.getBoard().getCities().get(selectedCity));
		MainActionController mainActionController = new MainActionController(game.getPlayers());
		IAction action = buildEmporiumWithKingHelpView.createAction(mainActionController);
		action.run();
	}

	/**
	 * The method for main action ElectCouncillor
	 * 
	 * @param game
	 * @param playerNumber
	 */
	public static void electCouncillor(Model game, int playerNumber) {
		System.out.println(
				"\nYou must choose one of their available councillors at the side of the board and inserts them in the council balcony");
		System.out.println("Select the Councillor from reserve:\n\n" + game.getBoard().getCouncillorReserve());

		Colors color = selectColor();
		String regionName = selectNameRegion();
		RegionType regionType = selectRegion(regionName);
		ElectCouncillorView electCouncillorView = new ElectCouncillorView(game.getPlayers().get(playerNumber),
				game.getBoard().getRegions(regionType), game.getBoard(), color);
		MainActionController mainActionController = new MainActionController(game.getPlayers());
		IAction action = electCouncillorView.createAction(mainActionController);
		action.run();
		System.out.println("You have received 4 coins!!");
	}

	// ------------------------------------the methods for quick actions ---------------------------------------------------

	/**
	 * The method for quick action changeFaceUpPermitCard
	 * 
	 * @param game
	 * @param playerNumber
	 */
	public static void changeFaceUpPermitCard(Model game, int playerNumber) {
		System.out.println(
				"You will return an assistant to the pool, then you will take the 2 permit card face up in a region, "
						+ "return them to the bottom of the deck and draws 2 new ones from the top of the deck,So for do it:");
		String regionName = selectNameRegion();
		RegionType regionType = selectRegion(regionName);
		ChangeFaceUpPermitCardView changeFaceUpPermitCardView = new ChangeFaceUpPermitCardView(
				game.getPlayers().get(playerNumber), game.getBoard().getRegions(regionType), game.getBoard());
		QuickActionConroller quickActionController = new QuickActionConroller(game.getPlayers());
		IAction action = changeFaceUpPermitCardView.createAction(quickActionController);
		action.run();
	}

	/**
	 * The method for quick action engageAssistant
	 * 
	 * @param game
	 * @param playerNumber
	 * @throws Exception 
	 */
	public static void engageAssistant(Model game, int playerNumber) throws Exception {
		System.out.println("You have to pays 3 coins to take an assistant from the pool");
		System.out.println("Currently you have "
				+ game.getPlayers().get(playerNumber).getCoinsTrack().getCurrentCoinsNumber() + " coins.");
		if (game.getPlayers().get(playerNumber).getCoinsTrack().getCurrentCoinsNumber() > 3) {
			EngageAssistantView engageAssistantView = new EngageAssistantView(game.getPlayers().get(playerNumber),
					game.getBoard());
			QuickActionConroller quickActionConroller = new QuickActionConroller(game.getPlayers());
			IAction action = engageAssistantView.createAction(quickActionConroller);
			action.run();
			System.out.println("\n--------------------------------- Action done --------------------------------------------\n");
			System.out.println("Now you have "
					+ game.getPlayers().get(playerNumber).getCoinsTrack().getCurrentCoinsNumber() + " coins.");
			System.out.println("Number of your assistants: "
					+ game.getPlayers().get(playerNumber).getAssistants().size());
		} else {
			System.out.println("Select another QUICK action from the 3 available action!");

			System.out.println("Select a Quick actions number:\n\n\t1-Change face up permit card\n\t2-Perform Additional MainAction"
					+ "\n\t3-Send assistant to elect councillor\n");
			int quickActionNumber = selectActionNumber();
			switch (quickActionNumber) {
			case 1:
				changeFaceUpPermitCard(game, playerNumber);
				break;
			case 2:
				performAdditionalMainAction(game, playerNumber);
				break;
			case 3:
				sendAssistantToElectCouncillor(game, playerNumber);
				break;
			default:
				// throw new IllegalArgumentException("the number inserted is
				// not correct!");
			}// switch quickaction finished
		}
	}

	/**
	 * The method for quick action performAdditionalMainAction
	 * 
	 * @param game
	 * @param playerNumber
	 * @throws Exception
	 */
	public static void performAdditionalMainAction(Model game, int playerNumber) throws Exception {
		System.out.println("\nyou need at least 3 assistants to execute this action.");
		System.out.println(
				"\nCurrently you have " + game.getPlayers().get(playerNumber).getAssistants().size() + " Assistants.");
		if (game.getPlayers().get(playerNumber).getAssistants().size() > 3) {

			System.out.println(
					"\nYou can do one MAIN action from the four available action, Select an actions number:\n");
			System.out.println("\t1-Acquire permit card\n\t2-Build emporium using permit card"
					+ "\n\t3-Build emporium with king help\n\t4-Elect councillors\n");
			int mainActionNumber = selectActionNumber();
			switch (mainActionNumber) {// switch per main action
			case 1:
				acquirePermitCard(game, playerNumber);
				break;
			case 2:
				buildEmporiumUsingPermitCard(game, playerNumber);
				break;
			case 3:
				buildEmporiumWithKingHelp(game, playerNumber);
				break;
			case 4:
				electCouncillor(game, playerNumber);
				break;
			default:
			}// swith finish --- main action

		} else {
			System.out.println("Select another QUICK action from the 3 available action!");

			System.out.println("Select a Quick actions number:\n\n\t1-Change face up permit card\n\t2-Engage assistant"
					+ "\n\t3-Send assistant to elect councillor\n");
			int quickActionNumber = selectActionNumber();
			switch (quickActionNumber) {
			case 1:
				changeFaceUpPermitCard(game, playerNumber);
				break;
			case 2:
				engageAssistant(game, playerNumber);
				break;
			case 3:
				sendAssistantToElectCouncillor(game, playerNumber);
				break;
			default:
				// throw new IllegalArgumentException("the number inserted is
				// not correct!");
			}// switch quickaction finished
		}

	}

	/**
	 * The method for quick action sendAssistantToElectCouncillor
	 * 
	 * @param game
	 * @param playerNumber
	 * @throws Exception 
	 */
	public static void sendAssistantToElectCouncillor(Model game, int playerNumber) throws Exception {
		System.out.println(
				"You must return one assistant to the pool, takes a councillor and inserts them in a balcony.");
		System.out.println(
				"\nCurrently you have " + game.getPlayers().get(playerNumber).getAssistants().size() + " Assistants.");
		if (game.getPlayers().get(playerNumber).getAssistants().size() > 1) {
			System.out.println("Select the Councillor from reserve:\n" + game.getBoard().getCouncillorReserve());
			Colors color = selectColor();
			String regioName = selectNameRegion();
			RegionType regionType = selectRegion(regioName);
			SendAssistantToElectCouncillorView sendAssistantToElectCouncillorView = new SendAssistantToElectCouncillorView(
					game.getPlayers().get(playerNumber), game.getBoard().getRegions(regionType), game.getBoard(), color);
			QuickActionConroller quickActionConroller = new QuickActionConroller(game.getPlayers());
			IAction action = sendAssistantToElectCouncillorView.createAction(quickActionConroller);
			action.run();
		} else {
			System.out.println("Select another QUICK action from the 3 available action!");
			System.out.println("Select a Quick actions number:\n\n\t1-Change face up permit card\n\t2-Engage assistant"
					+ "\n\t3-Perform additional MainAction\n");
			int quickActionNumber = selectActionNumber();
			switch (quickActionNumber) {
			case 1:
				changeFaceUpPermitCard(game, playerNumber);
				break;
			case 2:
				engageAssistant(game, playerNumber);
				break;
			case 3:
				performAdditionalMainAction(game, playerNumber);
				break;
			default:
				// throw new IllegalArgumentException("the number inserted is
				// not correct!");
			}// switch quickaction finished
		}
	}

	/**
	 * Metodo usato al interno del metodo selectRegion per prendere in ingresso
	 * dal utente come String il nome della regione
	 * 
	 * @return
	 */
	public static String selectNameRegion() {
		String regionTypeStr = null;
		boolean condition = true;
		System.out.println("\nSelect a region:\n\n\t1-HILL\n\t2-COAST\n\t3-MOUNTAIN\n");
		while (condition) {
			try {
				regionTypeStr = input.next().toUpperCase();
				if (regionTypeStr.equals("HILL") || regionTypeStr.equals("COAST") || regionTypeStr.equals("MOUNTAIN")) {
					condition = false;
					return regionTypeStr;
				}
				System.out.println("Try again! 'Incorrect input'\n");
			} catch (InputMismatchException e) {
				input.nextLine().toUpperCase();
			}
		}
		return regionTypeStr;
	}

	/**
	 * the method for asking to client to select region
	 * 
	 * @return RegionType
	 */
	public static RegionType selectRegion(String regionName) {
		RegionType regionType = null;
		switch (regionName) {
		case "HILL":
			regionType = RegionType.HILL;
			break;
		case "COAST":
			regionType = RegionType.COAST;
			break;
		case "MOUNTAIN":
			regionType = RegionType.MOUNTAIN;
			break;
		default:
			// System.out.println("The region type is not correct!");
		}
		return regionType;
	}

	/**
	 * the method for select the color
	 * 
	 * @param colorString
	 * @return
	 */
	public static Colors selectColor() {
		boolean condition = true;
		String colorSelected = null;
		Colors color = null;
		while (condition) {
			try {
				colorSelected = input.next().toUpperCase();
				if (colorSelected.equals("BLUE") || colorSelected.equals("BLACK") || colorSelected.equals("WHITE")
						|| colorSelected.equals("PINK") || colorSelected.equals("PURPLE")
						|| colorSelected.equals("ORANGE")) {
					condition = false;
				}
				System.out.println("Try again! 'Incorrect input'\n");
			} catch (InputMismatchException e) {
				input.nextLine();
			}
		}
		switch (colorSelected) {
		case "BLUE":
			color = Colors.BLUE;
			break;
		case "BLACK":
			color = Colors.BLACK;
			break;
		case "WHITE":
			color = Colors.WHITE;
			break;
		case "PINK":
			color = Colors.PINK;
			break;
		case "PURPLE":
			color = Colors.PURPLE;
			break;
		case "ORANGE":
			color = Colors.ORANGE;
		default:
			System.out.println("The color is not correct!");
		}
		return color;
	}

	/**
	 * the method for asking client to select one permitCard face up
	 * 
	 * @param game
	 * @param regionType
	 * @return
	 */
	public static int selectPermitcard(Model game, RegionType regionType) {
		int choose = 0;
		boolean condition = true;
		System.out.println("\nSelect the one of the two faceUp permitcard:('0' or '1')");
		System.out.println("\n.............................................................................\n0 --> "
				+ game.getBoard().getRegions(regionType).getDeckOfPermitCard().getFsceUpPermitCard().get(0));
		System.out.println("\n.............................................................................\n1 --> "
				+ game.getBoard().getRegions(regionType).getDeckOfPermitCard().getFsceUpPermitCard().get(1) + "\n"
				+ "..............................................................................");
		while (condition) {
			try {
				choose = input.nextInt();
				if (choose == 0 || choose == 1) {
					condition = false;
					return choose;
				}
				System.out.println("Try again! 'Incorrect input'\tinsert '0' or '1'\n");
			} catch (InputMismatchException e) {
				input.nextLine();
			}
		}
		return choose;
	}

	/**
	 * the method for asking client to select one Main Action
	 * 
	 * @return int selectedAction
	 */
	public static int selectActionNumber() {
		int mainActionNumber = 0;
		boolean condition = true;
		while (condition) {
			try {
				mainActionNumber = input.nextInt();
				if (0 < mainActionNumber && mainActionNumber < 5) {
					condition = false;
					return mainActionNumber;
				}
				System.out.println("Try again. (" + "Incorrect input: an integer is required: 1 or 2 0r 3 or 4)\n");
			} catch (InputMismatchException e) {

				input.nextLine();
			}
		}
		return mainActionNumber;
	}

	/**
	 * the method for asking player to select one of the permit card face up in
	 * front of them
	 * 
	 * @param game
	 * @param playerNumber
	 * @return int
	 * @throws Exception
	 */
	public static int selectPermitcardFromPly(Model game, int playerNumber) throws Exception {
		List<Integer> idx = new ArrayList<>();
		System.out.println("\nSelect one of the permit card face up in front of you:\n");
		if (!game.getPlayers().get(playerNumber).getPermitCards().isEmpty()) {
			// stampo le permitcard presente per player per decidere
			for (int i = 0; i < game.getPlayers().get(playerNumber).getPermitCards().size(); i++) {
				System.out.println(i + "-" + game.getPlayers().get(playerNumber).getPermitCards());
				idx.add(i);
			}
		} else {
			System.out.println("error: You do not have any permitcard!\n");
			System.out.println("Choose another action:\n");
			System.out.println("\t1-Acquire permit card\n\t"
					+ "\n\t3-Build emporium with king help\n\t4-Elect councillors\n");
			System.out.println(
					"\nYou can do one MAIN action from the 3 available action, Select an actions number:\n");
			System.out.println("\t1-Acquire permit card\n\t2-Build emporium using permit card"
					+ "\n\t3-Elect councillors\n");
			int mainActionNumber = selectActionNumber();
			switch (mainActionNumber) {// switch per main action
			case 1:
				acquirePermitCard(game, playerNumber);
				break;
			case 2:
				buildEmporiumWithKingHelp(game, playerNumber);
				break;
			case 3:
				electCouncillor(game, playerNumber);
				break;
			default:
			}// swith finish --- main action
		}
		int choose = 0;
		boolean condition = true;
		while (condition) {
			try {
				choose = input.nextInt();
				for (int j : idx) {
					if (choose == idx.get(j)) {
						condition = false;
						return choose;
					}
				}

			} catch (InputMismatchException e) {
				input.nextLine();
			}
		}
		return choose;

	}

	/**
	 * the method for asking client to doing a quick action
	 * 
	 * @return String yes or no
	 */
	public static String playerAnswer() {
		boolean condition = true;
		String answer = null;
		while (condition) {
			try {
				answer = input.next().toUpperCase();
				if (answer.equals("Y") || answer.equals("N")) {
					condition = false;
					return answer;
				}
				System.out.println("Try again. (" + "Incorrect input: an character is required: 'y' or 'n')\n");
			} catch (InputMismatchException e) {
				input.nextLine();
			}
		}
		return answer;
	}

}// class finish
